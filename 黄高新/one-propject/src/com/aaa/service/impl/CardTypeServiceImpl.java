package com.aaa.service.impl;

import com.aaa.dao.CardTypeDao;
import com.aaa.dao.impl.CardTypeDaoImpl;
import com.aaa.entity.CardType;
import com.aaa.service.CardTypeService;

import java.util.List;

public class CardTypeServiceImpl implements CardTypeService {
    private CardTypeDao cardTypeDao = new CardTypeDaoImpl();


    @Override
    public List<CardType> getAllCardTypeInfo() {
        return cardTypeDao.getAllCardTypeInfo();
    }
}
