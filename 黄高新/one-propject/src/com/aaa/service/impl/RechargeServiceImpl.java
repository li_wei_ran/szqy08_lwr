package com.aaa.service.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.RechargeDao;
import com.aaa.dao.impl.RechargeDaoImpl;
import com.aaa.entity.Card;
import com.aaa.entity.RechargeRecord;
import com.aaa.entity.RechargeRule;
import com.aaa.service.RechargeService;

import java.util.Date;
import java.util.List;

public class RechargeServiceImpl implements RechargeService {
    RechargeDao rechargeDao = new RechargeDaoImpl();

    BaseDao baseDao = new BaseDao();
    @Override
    public int rechargeRecode(Integer cardId, double recharge, Integer rechargeRule, Integer staffId, double amount) {
        String sql = "select * from card  where cardId = ?";
        Object[] params ={cardId};
        List<Card> cardList = baseDao.query(sql,params, Card.class);
        if(rechargeRule != 0) {
            String sql2 = "select * from rechargerule where status = 1 ";
            sql2 += " and id = ?";
            Object[] objects = {rechargeRule};
            List<RechargeRule> rechargeRuleList = baseDao.query(sql2, objects, RechargeRule.class);
            double oldAmount= cardList.get(0).getAmount();
            double newAmount = rechargeRuleList.get(0).getCoefficient() * rechargeRuleList.get(0).getStartMoney() + oldAmount + recharge;
            RechargeRecord rechargeRecord = new RechargeRecord();
            rechargeRecord.setCardId(cardId);
            rechargeRecord.setRechargeAmount(recharge);
            rechargeRecord.setRuleId(rechargeRule);
            rechargeRecord.setBeforeAmount(oldAmount);
            rechargeRecord.setAfterAmount(newAmount);
            rechargeRecord.setStaffId(staffId);
            rechargeRecord.setCreatedTime(new Date());
            return rechargeDao.rechargeRecord(rechargeRecord);
        }
        return 0;
    }
}
