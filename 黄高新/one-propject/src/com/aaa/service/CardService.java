package com.aaa.service;

import com.aaa.entity.Card;
import com.aaa.util.BusinessException;

import java.util.List;
import java.util.Map;

public interface CardService {

    /*
     * 获取最后一个会员卡id
     * */
    String getLastCardId();

    /*
     *
     * 用来实现添加会员的同时  并创建会员卡
     * @return
     */

    int addCard(Card card);
    /**
     * 分页查询所有会员卡信息
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     * @throws Exception
     */
    Map<String,Object> getAllCard(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception;

    /**
     * 查询所有会员卡信息条数
     *
     * @param searchId
     * @param searchName
     * @return
     */
    int getAllCardInfoCount(String searchId, String searchName) throws Exception;

    /**
     * 禁用会员卡
     *
     * @param cardId
     * @param status
     * @return
     */
    int BanOrAllowedCardByCardId(Integer cardId, Integer status);

    /**
     * 会员消费记录
     *
     * @param
     * @param
     * @return
     */
    /**
     * 获取所有卡的消费信息条数
     *
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     * @throws Exception
     */

    Map<String, Object> getAllRechargeRecord(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception;
    int deleteCardByCardId(int cardId);//删除
    List<Card> getCardById(Integer cardId);
    int rechargeCard(Integer cardId, double amount, Integer rechargeRule) throws BusinessException;

}
