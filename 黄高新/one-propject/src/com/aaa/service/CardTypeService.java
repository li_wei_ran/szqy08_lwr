package com.aaa.service;

import com.aaa.entity.CardType;

import java.util.List;

public interface CardTypeService {
    /**
     * 获取所有的卡类型
     * @return
     */
    List<CardType> getAllCardTypeInfo();
}
