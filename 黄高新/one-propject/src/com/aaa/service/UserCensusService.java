package com.aaa.service;

import com.aaa.entity.UserCensus;

import java.util.List;
import java.util.Map;

public interface UserCensusService {

    /**
     * 增加用
     * @return
     */
    boolean addUser(UserCensus userCensus, double amount);

    List<Map<String,Object>> getDataByNearYear();

}
