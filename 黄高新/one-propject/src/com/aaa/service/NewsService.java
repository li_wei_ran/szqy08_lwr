package com.aaa.service;

import com.aaa.entity.News;

import java.util.List;
import java.util.Map;

public interface NewsService {
    int addNews(News News);

    int updateNewsById(News News);

    int updateNewsStatusById( Integer id,Integer status);

    Map<String,Object> findAllAndSearchNews(Integer pageNum, Integer pageSize, String searchTitle, String searchstaffName,
                                                  String createdTime, String endTime);

    int findAllAndSearchNewsCount( String searchTitle, String searchstaffName,
                                   String createdTime,String endTime);
}
