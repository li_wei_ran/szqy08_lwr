package com.aaa.controller.Staff;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Staff;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetStaffInformationServlet")
public class GetStaffInformationServlet extends HttpServlet {
    private StaffService staffService;

    @Override
    public void init() throws ServletException {
        staffService = new StaffServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try{
            //获取staffID
            int staffId = Integer.valueOf(req.getParameter("staffId"));

            //通过staffID调用staffService.getStaffByStaffId(staffId)方法获取对象（数据库里需要得到的一条记录）
            Staff staff = staffService.getStaffByStaffId(staffId);

            //把staff里的东西放到resp那个对象里传到后天前端ajax解析
            responseDto.setData(staff);
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("请求成功");
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("请求失败");
        }
        //将Java对象转换为json字符串传到前端
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
