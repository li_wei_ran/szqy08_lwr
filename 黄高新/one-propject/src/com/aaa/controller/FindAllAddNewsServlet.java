package com.aaa.controller.news;

import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.impl.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/FindAllAddNewsServlet")
public class FindAllAddNewsServlet extends HttpServlet {
    private NewsService NewsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        ResponseDto responseDto = new ResponseDto();
        try {
            String searchStaffName = req.getParameter("searchStaffName");
            String searchCreatedTime = req.getParameter("searchCreatedTime");
            String searchEndTime = req.getParameter("searchEndTime");
            String searchTitle = req.getParameter("searchTitle");
            Integer pageSize = Integer.parseInt(req.getParameter("pageSize"));
            Integer pageNum = Integer.parseInt(req.getParameter("pageNumber"));

            Map<String,Object> map = NewsService.findAllAndSearchNews(pageNum,pageSize,searchTitle,searchStaffName,searchCreatedTime,searchEndTime);
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setData(map);
            responseDto.setMessage("操作成功");
        }catch (Exception e){
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("操作失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init() throws ServletException {
        NewsService = new NewsServiceImpl();
    }
}
