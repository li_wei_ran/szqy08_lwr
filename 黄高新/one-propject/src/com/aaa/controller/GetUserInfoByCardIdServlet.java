package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.UserService;
import com.aaa.service.impl.UserServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/GetUserInfoByCardIdServlet")
public class GetUserInfoByCardIdServlet extends HttpServlet {
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        Integer uid= IntegerUtils.ToInteger(req.getParameter("cardId"));
        List<String> list = userService.findUserByCardId(uid);
        ResponseDto responseDto = new ResponseDto();
        if (list != null) {
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setData(list);
            responseDto.setMessage("操作成功");
        } else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("操作失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        userService = new UserServiceImpl();
    }
}
