package com.aaa.controller.role;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddRoleServlet")
public class AddRoleServlet extends HttpServlet {
    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        roleService = new RoleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();

        try {
            //通过ajax,获取roleList.js里data 传过来的数据
            String roleName = req.getParameter("addRoleName");
            String description = req.getParameter("addDescription");
            String status = req.getParameter("addStatus");

            //调用roleService里的方法
            roleService.updateOrAddRole(0,roleName,description, Integer.valueOf(status));
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("添加成功");
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("添加失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
