package com.aaa.controller;

import com.aaa.entity.Order;
import com.aaa.entity.ResponseDto;
import com.aaa.service.OrderService;
import com.aaa.service.impl.OrderServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/GetAllOrderServlet")
public class GetAllOrderServlet extends HttpServlet {
    OrderService orderService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;application/json");
        Integer pageNumber = IntegerUtils.ToInteger(req.getParameter("pageNumber"));
        Integer pageSize = IntegerUtils.ToInteger(req.getParameter("pageSize"));
        Integer orderId = IntegerUtils.ToInteger(req.getParameter("orderId"));
        Integer cardId = IntegerUtils.ToInteger(req.getParameter("cardId"));

        List<Order> list = orderService.findAllOrder(pageNumber,pageSize,orderId,cardId);
        int count=orderService.getAllOrderInfoCount(orderId,cardId);
        ResponseDto responseDto = new ResponseDto();
        if (list != null){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setCount(count);
            responseDto.setData(list);
            responseDto.setMessage("操作成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("操作失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        orderService = new OrderServiceImpl();
    }
}
