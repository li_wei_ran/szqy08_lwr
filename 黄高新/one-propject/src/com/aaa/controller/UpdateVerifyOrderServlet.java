package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.OrderService;
import com.aaa.service.impl.OrderServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateVerifyOrderServlet")
public class UpdateVerifyOrderServlet extends HttpServlet {

    OrderService orderService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        Integer orderId = IntegerUtils.ToInteger(req.getParameter("orderId"));
        Integer status = IntegerUtils.ToInteger(req.getParameter("status"));
        int num = orderService.updateVerifyOrderByOrderId(orderId,status);

        ResponseDto responseDto = new ResponseDto();
        if (num != 0) {
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("更新成功");
        } else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("请求失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        orderService = new OrderServiceImpl();
    }
}
