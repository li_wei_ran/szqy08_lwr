package com.aaa.controller.news;

import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.impl.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateNewsStatusServlet")
public class UpdateNewsStatusServlet extends HttpServlet {
    private NewsService NewsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String status = req.getParameter("status");
        String id = req.getParameter("id");
        int num = NewsService.updateNewsStatusById(Integer.parseInt(id),Integer.parseInt(status));

        ResponseDto responseDto = new ResponseDto();
        if (num > 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("更新成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("更新失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doGet(req,resp);
    }

    @Override
    public void init() throws ServletException {
        NewsService = new NewsServiceImpl();
    }
}
