package com.aaa.controller.news;

import com.aaa.entity.News;
import com.aaa.entity.News;
import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.impl.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateNewsServlet")
public class UpdateNewsServlet extends HttpServlet {
    private NewsService newsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("ssssssssssss");


        String content = req.getParameter("content");
        String staffId = req.getParameter("staffId");
        String createdTime = req.getParameter("createdTime");
        String endTime = req.getParameter("endTime");
        String status = req.getParameter("status");
        String title = req.getParameter("title");
        String id= req.getParameter("id");

        News news = new News();
        news.setId(Integer.parseInt(id));
        news.setContent(content);
        news.setStaffId(Integer.parseInt(staffId));
        news.setTitle(title);
        news.setCreatedTime(createdTime);
        news.setEndTime(endTime);
        news.setStatus(Integer.parseInt(status));

        System.out.println(news);
        int num = newsService.updateNewsById(news);
        ResponseDto responseDto = new ResponseDto();
        if (num > 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("更新成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("更新失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init() throws ServletException {
        newsService = new NewsServiceImpl();
    }
}
