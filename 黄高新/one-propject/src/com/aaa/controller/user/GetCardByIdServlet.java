package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.UserService;
import com.aaa.service.impl.UserServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/GetCardByIdServlet")
public class GetCardByIdServlet extends HttpServlet {
    private UserService userService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer cardId = IntegerUtils.ToInteger(req.getParameter("searchCardId"));
        try {
            Map<String, Object> cardInfoById = userService.getCardInfoById(cardId);
            for (String s : cardInfoById.keySet()) {
                System.out.println(cardInfoById.get(s));
            }
            if (cardInfoById.size() > 0 ){
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setData(cardInfoById);
                responseDto.setMessage("请求成功");
                resp.getWriter().print(new Gson().toJson(responseDto));
            }else {
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setData(1);
                responseDto.setMessage("未找到该账号");
                resp.getWriter().print(new Gson().toJson(responseDto));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
