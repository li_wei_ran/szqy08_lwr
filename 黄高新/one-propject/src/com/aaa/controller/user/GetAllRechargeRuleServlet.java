package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Rule;
import com.aaa.service.RechargeRuleService;
import com.aaa.service.impl.RechargeRuleServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/GetAllRechargeRuleServlet")
public class GetAllRechargeRuleServlet extends HttpServlet {
    private RechargeRuleService rechargeRuleService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
       rechargeRuleService = new RechargeRuleServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Rule> rechargeRule = rechargeRuleService.getRechargeRule();
            if (rechargeRule != null) {
                responseDto.setData(rechargeRule);
                responseDto.setMessage("请求成功");
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                resp.getWriter().print(new Gson().toJson(responseDto));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
