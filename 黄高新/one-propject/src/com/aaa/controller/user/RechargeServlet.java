package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.RechargeService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.service.impl.RechargeServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/RechargeServlet")
public class RechargeServlet extends HttpServlet {
    private CardService cardService;
    private RechargeService rechargeService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
        rechargeService = new RechargeServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer cardId = Integer.parseInt(req.getParameter("cardId"));
        double recharge = Double.parseDouble(req.getParameter("recharge"));
        Integer rechargeRule = Integer.parseInt(req.getParameter("rechargeRule"));
        double amount = Double.parseDouble(req.getParameter("amount"));
        Integer staffId = Integer.parseInt(req.getParameter("staffId"));

        try {
                    int len = cardService.rechargeCard(cardId,recharge,rechargeRule);
                    int len1 = rechargeService.rechargeRecode(cardId,recharge,rechargeRule,staffId,amount);

            System.out.println(len);
            System.out.println(len1);
                    if (len > 0 && len1 > 0) {
                        responseDto.setMessage("请求成功");
                        responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                        resp.getWriter().print(new Gson().toJson(responseDto));
                    }
        } catch (Exception e) {
                e.printStackTrace();
        }





    }
}
