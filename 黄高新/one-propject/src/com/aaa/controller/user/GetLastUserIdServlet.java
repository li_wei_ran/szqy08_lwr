package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.UserService;
import com.aaa.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetLastUserIdServlet")
public class GetLastUserIdServlet extends HttpServlet {
    private UserService userService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String lastUserId = userService.getLastUserId();

    }
}
