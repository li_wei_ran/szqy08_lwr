package com.aaa.controller;

import com.aaa.entity.Goods;
import com.aaa.entity.ResponseDto;
import com.aaa.service.GoodsService;
import com.aaa.service.GoodsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/initGoodsIdServlet")
public class InitGoodsIdServlet extends HttpServlet {
    GoodsService goodsService = null;

    @Override
    public void init() throws ServletException {
        goodsService = new GoodsServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            List<Goods> initGoodsId = goodsService.InitGoodsId();

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("初始化商品编号成功");
            responseDto.setData(initGoodsId);
            resp.getWriter().print(new Gson().toJson(responseDto));

        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("初始化商品编号失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
