package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Staff;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/AddStaffServlet")
public class AddStaffServlet extends HttpServlet {
    StaffService staffService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String staffName = req.getParameter("staffName");
        String phone = req.getParameter("phone");
        String idCard = req.getParameter("idCard");
        String address = req.getParameter("address");
        String status = req.getParameter("status");
        String roleId = req.getParameter("roleId");
        String momo = req.getParameter("momo");
        System.out.println(roleId);
        Staff staff = new Staff();
        staff.setAddress(address);
        staff.setIdCard(idCard);
        staff.setMomo(momo);
        staff.setRoleId(Integer.parseInt(roleId));
        staff.setPhone(phone);
        staff.setStatus(Integer.parseInt(status));
        staff.setStaffId(staffService.getMaxStaffId().get(0).getStaffId()+1);
        staff.setStaffName(staffName);
        staff.setPassword("123123");


        Date date=new Date();
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
        String time=formatter.format(date);
        System.out.println(time);
        staff.setCreatedTime(time);

        int num = staffService.addStaffByStaffId(staff);

        ResponseDto responseDto = new ResponseDto();
        if (num != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("添加成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("添加失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        staffService = new StaffServiceImpl();
    }
}
