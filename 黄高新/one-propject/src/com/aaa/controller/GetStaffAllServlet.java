package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetStaffAllServlet")
public class GetStaffAllServlet extends HttpServlet {
    StaffService staffService;
    @Override
    public void init() throws ServletException {
       staffService = new StaffServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //返回参数
        ResponseDto responseDto = new ResponseDto();
        try {

            Integer pageNumber = IntegerUtils.ToInteger(request.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(request.getParameter("pageSize"));
            String searchStaffId = request.getParameter("searchStaffId");
            String searchName = request.getParameter("searchName");
            responseDto.setData(staffService.getAllStaffInfo(pageNumber, pageSize, searchStaffId, searchName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");

        }
        response.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
