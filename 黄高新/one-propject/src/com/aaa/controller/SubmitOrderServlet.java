package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Staff;
import com.aaa.service.OrderService;
import com.aaa.service.StaffService;
import com.aaa.service.impl.OrderServiceImpl;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/SubmitOrderServlet")
public class SubmitOrderServlet extends HttpServlet {
    OrderService orderService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String orderinfoArr = req.getParameter("orderinfoArr");
        String orderObj = req.getParameter("orderObj");

        int affectRow=orderService.submitOrder(orderinfoArr,orderObj);
        ResponseDto responseDto = new ResponseDto();
        if (affectRow != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("提交成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("提交失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        orderService = new OrderServiceImpl();
    }
}
