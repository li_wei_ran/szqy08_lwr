package com.aaa.controller;


import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/DeleteServlet")
public class deleteServlet extends HttpServlet {
    CardService cardService;


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String cardId = request.getParameter("cardId");
        int num = cardService.deleteCardByCardId(Integer.parseInt(cardId));
        ResponseDto responseDto = new ResponseDto();
        if (num !=0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("删除成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("删除失败");
        }
        response.getWriter().write(new Gson().toJson(responseDto));

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    public void init() throws ServletException {
        cardService=new CardServiceImpl();
    }

}
