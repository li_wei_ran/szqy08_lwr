package com.aaa.dao;

import com.aaa.entity.RechargeRecord;

public interface RechargeDao {

    /**
     * 值时添加充值信息
     * @return
     */
    int rechargeRecord(RechargeRecord rechargeRecord);
}
