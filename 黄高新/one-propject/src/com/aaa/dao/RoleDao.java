package com.aaa.dao;

import com.aaa.entity.Role;
import com.aaa.entity.TreeMenu;

import java.util.List;

public interface RoleDao {

    /**
     * 获取所有角色
     * @return
     */
    List<Role> getAllRole();

    /**
     * 按照搜索 分页获取角色
     * @param pageNumber
     * @param pageSize
     * @param searchName
     * @return
     */
    List<Role> getAllRoleInfo(int pageNumber, int pageSize, String searchName);

    /**
     * 获取所有符合条件的角色的 数量
     * @param searchName
     * @return
     */
    int getAllRoleInfoCount(String searchName);

    /**
     * 增加角色
     * @param roleName
     * @param description
     * @param status
     * @return
     */
    int addRole(String roleName, String description, int status);


    List<TreeMenu>  getMenuList(int roleId);

    int updateMenuList(int roleId, int[] resouceces);

    //删除角色
    int deleteRole(Integer id);

    //修改角色
    int updateRole(Integer id,String roleName,String description,Integer status);

    //删除resource_role中的角色
    int deleteRole1(Integer id);

    //新增resource_role中的角色
    int addRole1(Integer id,Integer resource_id);

}
