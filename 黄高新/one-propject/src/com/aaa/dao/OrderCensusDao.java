package com.aaa.dao;
import com.aaa.entity.OrderCensus;

import java.util.List;
import java.util.Map;


public interface OrderCensusDao {
    int addOrder(OrderCensus orderCensus);
    List<Map<String, Object>> getDataByNearYear();
}
