package com.aaa.dao;

import com.aaa.entity.Category;

import java.util.List;

public interface CategoryDao {
    /**
     * 商品列表对应商品类型的动态下拉框
     * @return
     */
    List<Category> getCategoryList();


    /**
     * 分页查询所有的商品信息
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     */
    List<Category> getAllCategoryInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    /**
     * 查询关键字段所有商品信息的总条数
     * @param searchId
     * @param searchName
     * @return
     */
    int getAllCategoryInfoCount(String searchId,String searchName);

    /**
     * 修改商品类型
     * @param category
     * @return
     */
    int updateCategory(Category category);

    /**
     * 改变商品状态
     * @param category
     * @return
     */
    int changeStatus(Category category);

    /**
     * 添加商品类型
     */
    int addCategor(Category category);
}
