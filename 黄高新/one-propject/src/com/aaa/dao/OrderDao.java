package com.aaa.dao;

import com.aaa.entity.Order;
import com.aaa.entity.OrderInfo;

import java.util.List;

public interface OrderDao {
    /**
     * 修改订单信息
     * @param order
     * @return
     */
    int updateOrderByOrderId(Order order);

    /**
     *更新订单的状态
     * @param orderId
     * @param status
     * @return
     */
    int updateVerifyOrderByOrderId(Integer orderId, Integer status);

    int getAllOrderInfoCount(Integer orderId, Integer cardId);

    /**
     * 查询订单
     * @param pageNumber
     * @param pageSize
     * @param orderId
     * @param cardId
     * @return
     */
    List<Order> findAllOrder(Integer pageNumber, Integer pageSize, Integer orderId, Integer cardId);

    /**
     * 根据订单id查询订单
     * @param orderId
     * @return
     */
    List<Order> findOrderByOrderId(int orderId);

    /**
     * 更加会员id查询订单
     * @param cardId
     * @return
     */
    List<Order> findOrderByCardId(int cardId);


    /**
     * 提交订单更新order表
     * @param order
     * @return
     */
    public int insertIntoOrder(Order order);

    /**
     * 提交订单更新orderinfo表
     * @param orderInfo
     * @return
     */
    public int insertIntoOrderinfo(OrderInfo orderInfo);

    /**
     * 减去用户提交订单后的余额
     * @param cardId
     * @param money
     * @return
     */
    public int minusCardAmount(int cardId, double money);

    /**
     * 添加订单记录
     * @param order
     * @return
     */
    public int addOrder(Order order);

}
