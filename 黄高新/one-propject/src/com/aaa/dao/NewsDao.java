package com.aaa.dao;

import com.aaa.entity.News;

import java.util.List;
import java.util.Map;

public interface NewsDao {

    int addNews(News news);

    int updateNewsById(News news);

    int updateNewsStatusById( Integer id,Integer status);

    List<News> findAllAndSearchNews(Integer pageNum, Integer pageSize,  String searchTitle, String searchstaffName,
                                                  String createdTime,String endTime);

    int findAllAndSearchNewsCount( String searchTitle, String searchstaffName,
                                   String createdTime,String endTime);


}
