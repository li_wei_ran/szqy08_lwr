package com.aaa.dao;

import com.aaa.entity.Order;
import com.aaa.entity.OrderInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class OrderDaoImpl implements OrderDao{
    BaseDao baseDao=BaseDao.getInstance();

    @Override
    public int updateOrderByOrderId(Order order) {
        String sql = "UPDATE `order` SET `cardId` = ?,cardType = ?,`price` = ?,`pay` = ?,`credit` = ?,`status` = ?,`momo` = ? WHERE `orderId` = ?";
        Object[] paramsArr = {order.getCardId(), order.getCardType(), order.getPrice(), order.getPay(), order.getCredit(), order.getStatus(), order.getMomo(), order.getOrderId()};
        return baseDao.executeUpdate(sql, paramsArr);
    }

    @Override
    public int updateVerifyOrderByOrderId(Integer orderId, Integer status) {
        int statusForUpdate = 0;
        if (status == 0) {
            statusForUpdate = 1;
        }
        String sql = "UPDATE `order` SET `status` = ? WHERE `orderId` = ?";
        Object[] paramsArr = {statusForUpdate, orderId};
        return baseDao.executeUpdate(sql, paramsArr);
    }

    @Override
    public int getAllOrderInfoCount(Integer orderId, Integer cardId) {
        String sql = "select count(1) len from `order` where 1 = 1";
        if (orderId > 0) {
            sql += " and orderId = " + orderId;
        }
        if (cardId > 0) {
            sql += " and cardId like '" + "%" + cardId + "%" + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        } else {
            return 0;

        }
    }

    @Override
    public List<Order> findAllOrder(Integer pageNumber, Integer pageSize, Integer orderId, Integer cardId) {
        pageNumber = (pageNumber - 1) * pageSize;
        String sql = "select * from `order` where 1=1";
        if (orderId > 0) {
            sql += " and orderId = " + orderId;
        }
        if (cardId > 0) {
            sql += " and cardId like '" + "%" + cardId + "%" + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<Order> orderList = baseDao.query(sql, params, Order.class);
        return orderList;
    }

    @Override
    public List<Order> findOrderByOrderId(int orderId) {
        String sql = "select * from staff where staffId = ?";
        Object[] arr = {orderId};
        return baseDao.query(sql, arr, Order.class);
    }

    @Override
    public List<Order> findOrderByCardId(int cardId) {
        String sql = "select * from staff where staffName = ?";
        Object[] arr = {cardId};
        return baseDao.query(sql, arr, Order.class);
    }

    @Override
    public int insertIntoOrder(Order order) {
        String sql = "INSERT INTO `order` (`orderId`, `cardId`, `cardType`, `price`, `pay`, `credit`, `status`, `momo`,`createdTime`) " +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        Object[] arr = {order.getOrderId(), order.getCardId(), order.getCardType(), order.getPrice(), order.getPay(),
                order.getCredit(), order.getStatus(), order.getMomo(), order.getCreatedTime()};
        return baseDao.executeUpdate(sql, arr);
    }

    @Override
    public int insertIntoOrderinfo(OrderInfo orderInfo) {
        String sql = "INSERT INTO `orderinfo` (`orderId`, `goodsId`, `goodsNumber`, `goodsCredit`) VALUES (?,?,?,?)";
        Object[] arr = {orderInfo.getOrderId(), orderInfo.getGoodsId(), orderInfo.getGoodsNumber(), 1};
        return baseDao.executeUpdate(sql, arr);
    }

    @Override
    public int minusCardAmount(int cardId, double money) {
        String sql = "UPDATE `card` SET `amount`=`amount`-? WHERE `cardId`=?";
        Object[] arr = {money,cardId};
        return baseDao.executeUpdate(sql, arr);
    }

    @Override
    public int addOrder(Order order) {
        String sql = "INSERT INTO `orderinfo` (`orderId`, `goodsId`, `goodsNumber`, `goodsCredit`) VALUES (?,?,?,?)";
        Object[] arr = {order};
        return baseDao.executeUpdate(sql, arr);
    }
}
