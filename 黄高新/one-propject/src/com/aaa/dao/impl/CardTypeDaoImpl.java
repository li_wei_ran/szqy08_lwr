package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.CardTypeDao;
import com.aaa.entity.CardType;

import java.util.List;

public class CardTypeDaoImpl implements CardTypeDao {
    private BaseDao baseDao = new BaseDao();

    @Override
    public List<CardType> getAllCardTypeInfo() {
        String sql = "select * from cardtype where status = 1";
        List<CardType> cardTypeList = baseDao.query(sql, null, CardType.class);
        return cardTypeList;
    }
}
