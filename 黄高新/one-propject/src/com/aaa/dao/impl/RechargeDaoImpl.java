package com.aaa.dao.impl;


import com.aaa.dao.BaseDao;
import com.aaa.dao.RechargeDao;
import com.aaa.entity.RechargeRecord;


public class RechargeDaoImpl implements RechargeDao {
    private BaseDao baseDao = new BaseDao();
    @Override
    public int rechargeRecord(RechargeRecord rechargeRecord) {
        String  sql = "insert into  rechargerecord(cardId,rechargeAmount,afterAmount,beforeAmount,ruleId,createdTime,staffId,momo) values (?,?,?,?,?,?,?,?) ";
        Object[] objects = {rechargeRecord.getCardId(),rechargeRecord.getRechargeAmount()
                ,rechargeRecord.getAfterAmount(),rechargeRecord.getBeforeAmount(),
                rechargeRecord.getRuleId(),rechargeRecord.getCreatedTime(),
                rechargeRecord.getStaffId(),rechargeRecord.getMomo()};
        int  len = baseDao.executeUpdate(sql,objects);
        return len;
    }
}
