package com.aaa.dao.impl;

import com.aaa.dao.NewsDao;
import com.aaa.entity.News;
import com.aaa.dao.BaseDao;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class NewsDaoImpl implements NewsDao {
    BaseDao baseDao = BaseDao.getInstance();

    @Override
    public int addNews(News news) {
        String sql = "insert into news(title,content,status,staffId,createdTime,endTime) values(?,?,?,?,?,?)";
        Object [] params = {news.getTitle(),news.getContent(),news.getStatus(),news.getStaffId(),news.getCreatedTime(),
        news.getEndTime()};
        return baseDao.executeUpdate(sql,params)  ;
    }

    @Override
    public int updateNewsById(News news) {
        String sql = "update news set title = ?,content = ?,status = ?,staffId = ?,createdTime = ? ,endTime=? " +
                "where id = ? ";
        Object [] params = {news.getTitle(),news.getContent(),news.getStatus(),news.getStaffId(),news.getCreatedTime(),
                news.getEndTime(),news.getId()};
        return baseDao.executeUpdate(sql,params) ;
    }

    @Override
    public int updateNewsStatusById(Integer id, Integer status) {
        String sql = "update news set status = ? where id = ?";
        Object[] params = {status,id};
        return baseDao.executeUpdate(sql,params);
    }

    @Override
    public List<News> findAllAndSearchNews(Integer pageNum, Integer pageSize, String searchTitle, String searchStaffName,
                                                        String createdTime,String endTime) {
        String sql = "select n.*,s.staffName from news n,staff s where n.staffId = s.staffId ";
        if (StringUtils.isNotBlank(searchTitle)){
            searchTitle = "%" + searchTitle + "%";
            sql += "and title like '"+ searchTitle + "'";
        }
        if (StringUtils.isNotBlank(searchStaffName)) {
            searchStaffName = "%" + searchStaffName + "%";
            sql += " and s.staffName like '" + searchStaffName + "'";
        }
        if (StringUtils.isNotBlank(createdTime)) {
            System.out.println(createdTime);
            sql += " and n.createdTime like '" + createdTime + "'" ;
        }
        if (StringUtils.isNotBlank(endTime)) {
            sql += " and n.endTime like '" + endTime + "'" ;
        }
        sql = sql + " limit ?,?";

        Object[] params = {pageNum,pageSize};
        return baseDao.query(sql,params,News.class);
    }

    @Override
    public int findAllAndSearchNewsCount( String searchTitle, String searchStaffName,
                                          String createdTime,String endTime) {
        String sql = "select count(1) as len from news n,staff s where n.staffId = s.staffId ";
        if (StringUtils.isNotBlank(searchTitle)){
            searchTitle = "%" + searchTitle + "%";
            sql += "and searchTitle like '"+ searchTitle + "'";
        }
        if (StringUtils.isNotBlank(searchStaffName)) {
            searchStaffName = "%" + searchStaffName + "%";
            sql += " and s.staffName like '" + searchStaffName + "'";
        }
        if (StringUtils.isNotBlank(createdTime)) {
            sql += " and n.createdTime like '" + createdTime + "'" ;
        }
        if (StringUtils.isNotBlank(endTime)) {
            sql += " and n.endTime like '" + endTime + "'" ;
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }
}
