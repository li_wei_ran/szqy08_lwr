package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.OrderCensusDao;
import com.aaa.entity.OrderCensus;

import java.util.List;
import java.util.Map;

public class OrderCensusDaoImpl implements OrderCensusDao {
    BaseDao baseDao= BaseDao.getInstance();

    @Override
    public int addOrder(OrderCensus orderCensus) {
        String sql = "insert into order (orderId,cardId,cardType,price,pay,credit,status momo,createdTime)"+
                "values (?,?,?,?,?,?,?,?,?,?)";

        Object[] params = {orderCensus.getCardId(),orderCensus.getCardType(),orderCensus.getCreadit(),orderCensus.getCreatedTime(),orderCensus.getPay(),orderCensus.getPrice(),orderCensus.getCardId(),orderCensus.getMomo()};

        return baseDao.executeInsert(sql,params);
    }

    @Override
    public List<Map<String, Object>> getDataByNearYear() {
        String sql = "select COUNT(*) amount,SUBSTRING(createdTime FROM 6 FOR 2) month from `order` GROUP BY SUBSTRING(createdTime FROM 6 FOR 2)";
        return baseDao.query(sql,null);
    }
}
