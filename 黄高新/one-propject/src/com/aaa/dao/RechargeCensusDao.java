package com.aaa.dao;

import com.aaa.entity.RechargeCensus;

import java.util.List;
import java.util.Map;

public interface RechargeCensusDao {

    int addRecharge(RechargeCensus rechargeCensus);
    List<Map<String, Object>> getDataByNearYear();
}
