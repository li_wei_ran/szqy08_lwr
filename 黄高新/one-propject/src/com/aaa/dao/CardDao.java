package com.aaa.dao;

import com.aaa.entity.Card;
import com.aaa.entity.RechargeRecord;

import java.util.List;

public interface CardDao {
    /*
     *获取最后一个会员的id
     * 用来实现会员id自增
     * @return
     */
    String getLastCardId();
    /*
     *
     * 用来实现添加会员的同时  并创建会员卡
     * @return
     */

    int addCard(Card card);

    /**
     * 分页查询所有会员卡信息
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     */
    List<Card> getAllCard(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    /**
     * 查询所有会员卡信息条数
     *
     * @param searchId
     * @param searchName
     * @return
     */
    int getAllCardInfoCount(String searchId, String searchName);

    int BanOrAllowedCardByCardId(Integer cardId, Integer Status);
    int deleteCardByCardId(int cardId);

    List<RechargeRecord> getAllRechargeRecord(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    int getAllRechargeRecordCount(String searchId, String searchName);

    List<Card> getCardById(Integer cardId);
    int rechargeCard(Integer cardId, double amount, Integer rechargeRule);

}
