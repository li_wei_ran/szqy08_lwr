var goodsListUrl = "/GetAllGoodsServlet";
var CategoryUrl = "/GetAllGoodsCategoryServlet";
var unitUrl = "/GetAllGoodsUnitServlet";
var updateGoodsUrl = "/UpdateGoodsServlet";
var updateStatusUrl = "/ChangeStatusServlet";

$(function () {
    // 初始化 分页列表
    goodsManage.initList();
    // 初始化商品类型下拉
    goodsCateGory.initList();
    // 初始化商品单位下拉
    goodsUnit.initList();
})
var goodsManage = {};
var goodsCateGory = {};
var goodsUnit = {};

goodsManage.initList = function () {
    $("#goodsList").bootstrapTable({
        url: goodsListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: goodsManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [5,10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function ( e,row, element) {  // 设置行点击事件***************************
            $(".success").removeClass("success");
            $(row).addClass("success");
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result.status==1) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "商品id",
            visible: false
        }, {
            field: 'goodsId',
            title: "商品编号"
        }, {
            field: 'name',
            title: "商品名称"
        }, {
            field: 'code',
            title: "商品数量",
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return '<button class="btn-xs btn-info" border="0">上架</button>';
                    case 2 :
                        return '<button class="btn-xs btn-danger" border="0">下架</button>';
                }
            }
        }, {
            field: 'categoryName',
            title: "商品类型"
        }, {
            field: 'unitName',
            title: "单位"
        }, {
            field: 'price',
            title: "价格"
        }, {
            field: 'operation',//--------------------
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return goodsManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
goodsManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchGoodsId": $("#searchGoodsId").val(),
        "searchGoodsName": $("#searchGoodsName").val()
    }
}
/**
 * 搜索
 */
goodsManage.search = function () {
    //bootstrapTable 刷新
    $("#goodsList").bootstrapTable('refresh');
}
/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
goodsManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateGoods">修改</button>');
    returnButton.push('<button class="btn btn-danger changeStatus">上架/下架</button>');
    return returnButton.join('');
}
/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 商品 对应的 更新按钮的updateStaff
    'click .updateGoods': function (e, value, row, index) {
        //row 这一行的数据
        // 初始化更新的模态框
        //1.初始模态框内的数据
        $("#goodsId").val(row.goodsId);
        $("#goodsName").val(row.name);
        $("#category option[value='1']").attr("selected","selected");
        $("#unit option[value='1']").attr("selected","selected");
        $("#code").val(row.code);
        $("#price").val(row.price);
        //2.显示  在模态框的确定按钮添加 事件  使用ajax 将用户修改的数据 提交到后台
        $("#myModal").modal('show');


    },
    // 改变商品状态
    'click .changeStatus': function (e, value, row, index) {
        goodsManage.change(row);
    }
}
/**
 * 初始化商品单位
 */
goodsUnit.initList = function(){

    $.ajax({
        url: unitUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0 ) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].name + "</option>");
                    $("#unit").append(opt);
                }
            }
        }
    })
}
/**
 * 初始化商品类型
 */
goodsCateGory.initList = function(){

    $.ajax({
        url: CategoryUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0 ) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].name + "</option>");
                    $("#category").append(opt);
                }
            }
        }
    })
}


/**
 * 修改商品信息
 */
function updateGoods(){
    $.ajax({
        url: updateGoodsUrl,// 自己完成后台
        type: 'post',
        data: {
            "goodsId": $("#goodsId").val(),
            "goodsName": $("#goodsName").val(),
            "category":$("#category").val(),
            "unit":$("#unit").val(),
            "code": $("#code").val(),
            "price": $("#price").val(),
        },
        dataType: 'json',
        success: function (result) {
            if (result.goodsId>0) {
                toastr['success']("操作成功");
                $("#myModal").modal('hide');
                goodsManage.search();
            } else {
                toastr['error']("操作失败");
            }
        }
    })
}


/**
 * 改变商品状态
 * @param row
 */
goodsManage.change = function (row) {
    Modal.confirm({
        msg: "是否改变商品状态?"  // 配置 确认窗口 ，也必须在html 设置窗口布局
    }).on(function (e) {
        if (e) {
            $.ajax({
                url: updateStatusUrl,
                type: 'post',
                data: {
                    "id":row.id,
                    "status": row.status
                },
                dataType: 'json',
                success: function (result) {
                    if (result.id > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                    goodsManage.search();
                },error:function (result) {
                    toastr['error']("切换失败");
                }
            })

        }
    })
}

/**
* 表单提交验证
* */
function aaa (){
    var bootstrapValidator = $("#goodsForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
    }else {
    }
}
$("#goodsForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        goodsName: {
            validators: {
                notEmpty: {
                    message: "商品名称不能为空"
                }
            }
        },
        category:{
            validators: {
                notEmpty: {
                    message: "商品类型不能为空"
                }
            }
        },
        unit: {
            validators: {
                notEmpty: {
                    message: "商品单位不能为空"
                }
            }
        },
        code: {
            validators: {
                notEmpty: {
                    message: "商品数量不能为空"
                }
            }
        },
        price: {
            validators: {
                notEmpty: {
                    message: "商品价格不能为空"
                }
            }
        },
    }
});