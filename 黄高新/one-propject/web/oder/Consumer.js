var goodsListUrl = "/ConsumerGoodsListServlet";
var userInfoUrl = "/GetUserInfoByCardIdServlet";


// 商品管理类
var goodsManager = {};

//订单类
var oderManager = {};

// 用户
var userManager = {};
/**
 * 查询用户信息(消费收银的读卡功能)
 */
userManager.searchUser = function () {
    $.ajax({
        url: userInfoUrl,
        type: 'get',
        data: {
            'cardId': $('#serchCardId').val()
        },
        dataType: 'json',
        success: function (result) {
            var data = result.data[0];
            if (result.status == 1) {
                $('#userId').html(data.userId);
                $('#userName').html(data.userName);
                $('#cardId').html(data.cardId);
                $('#amount').html(data.amount);
                $('#cardLevel').html(data.name);
                $('#credit').html(data.credit);
            }
        }
    })
}

/**
 * 初始化
 */
$(function () {
    goodsManager.initList();
})


/**
 * 搜索
 */
goodsManager.search = function () {
    //bootstrapTable 刷新
    $("#goodsList").bootstrapTable('refresh');
}

goodsManager.initList = function () {
    $("#goodsList").bootstrapTable({
        url: goodsListUrl, //请求路径
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: goodsManager.queryParams, //传递参数(*)
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function (e, row, element) {
            $(".success").removeClass("success");
            $(row).addClass("success");
        },
        responseHandler: function (result) {
            if (result != null) {
                return {
                    'total': result.data.count, //总条数
                    'rows': result.data.list //所有的数据
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },

        //列表显示
        columns: [{
            field: 'goodsId',
            title: "商品编号"
        }, {
            field: 'name',
            title: "商品名称"
        },
            //     {
            //     field: 'code',
            //     title: "商品余量"
            // },
            {
                field: 'categoryName',
                title: "商品类型",
            }, {
                field: 'unitName',
                title: "单位",
            }, {
                field: 'price',
                title: "价格"
            }, {
                field: 'operation',
                events: buttonOperateEvent,
                title: '操作',
                formatter: function (value, row, index) {
                    return goodsManager.buttonOption(value, row, index);
                }
            }
        ]
    });
}

/**
 * 传递参数
 * @param params
 * @returns {{pageNumber: *, searchGoodsId: *, searchGoodsName: *, pageSize: *}}
 */
goodsManager.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber,
        "pageSize": params.pageSize,
        "searchGoodsId": $("#searchGoodsId").val(),
        "searchGoodsName": $("#searchGoodsName").val()
    }
}

/**
 * 添加按钮源
 *
 */
window.buttonOperateEvent = {
    'click .buyGoods': function (e, value, row, index) {
        oderManager.add(row);
    }
}

/**
 * 添加操作按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
goodsManager.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button type="button" class="btn btn-primary buyGoods" ">购买</button>');
    return returnButton.join('');
}

oderManager.add = function (row) {
    console.log(row);
    alert('购买');

    var tr = $("<tr></tr>");
    var td = $("<td>" + row.goodsId + "</td>");
    var td1 = $("<td>" + row.name + "</td>");
    var td2 = $("<td>" + 1 + "</td>");
    var td3 = $("<td>" + row.price + "</td>");
    var td4 = $("<td><button class='btn btn-danger' onclick='del(this)' >删除</button></td>");
    tr.append(td);
    tr.append(td1);
    tr.append(td2);
    tr.append(td3);
    tr.append(td4);
    $("#bugGoodsTb").append(tr);

    oderManager.initConsumer();
}

function del(obj) {
    $(obj).parent().parent().remove();

    // 从新计算金额
    oderManager.initConsumer();
}

oderManager.initConsumer = function () {

    var sum = 0;
    // 商品数量
    var count = 0;
    $("#buyGoods tbody tr").each(function () {
        var td = $(this).find("td");
        sum += Number(td.eq(3).text());
        count = count + 1;
    })
    $("#orderPrice").text(sum);
    $("#orderNum").text(count);

    var timestamp = new Date().getTime();
    timestamp += "";
    localStorage.initOrderId = timestamp.substring(6);
    $("#orderId").text(timestamp.substring(6));


    //初始化消费时间
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    $("#orderTime").text(year + "-" + (month + 1) + "-" + day + " " + hour + ":" + min + ":" + sec);

}

/**
 * 提交订单
 */
function submitOrder() {

    //1.获取订单数据

    //循环获取表格的订单数据
    var tab = document.getElementById("buyGoods");
    var rows = tab.rows;
    // console.log(rows.length);//获取表格的行数
    var orderinfoArr = new Array();

    for (var i = 1; i < rows.length; i++) { //遍历表格的行
        var obj = new Object();
        obj.orderId = localStorage.initOrderId;
        for (var j = 0; j < rows[i].cells.length - 2; j++) {  //遍历每行的列
            if (j + 1 == 2) {
                eval("obj.goodsId='" + rows[i].cells[j - 1].innerHTML + "'");
            } else if (j + 1 == 3) {
                eval("obj.goodsNumber='" + rows[i].cells[j].innerHTML + "'");
            }
            //console.log("第" + i + "行，第" + (1 + j) + "列的值是:" + rows[i].cells[j].innerHTML);
        }
        orderinfoArr[i - 1] = obj;
    }
    console.log(orderinfoArr);


    //2.获取订单信息

    //get data for insert order
    //判断用户等级（cardType）
    var cardType = 0;
    var cardLevel = $('#cardLevel').html();
    if (cardLevel == '普通会员') {
        cardType = 1;
    } else if (cardLevel == '青铜会员') {
        cardType = 2;
    } else if (cardLevel == '白银会员') {
        cardType = 3;
    } else if (cardLevel == '青铜会员') {
        cardType = 4;
    } else if (cardLevel == '黄金会员') {
        cardType = 5;
    } else {
        alert('请读卡');
        return
    }
    //封装order需要的数据
    var orderObj = new Object();
    orderObj.orderId = localStorage.initOrderId;
    orderObj.cardId = $('#cardId').html();
    orderObj.cardType = cardType;
    orderObj.price = $('#orderPrice').html();
    orderObj.pay = $('#orderPrice').html();
    orderObj.credit = 1;
    orderObj.status = 0;
    orderObj.createdTime = $('#orderTime').html();


    //提交
    $.ajax({
        url: "/SubmitOrderServlet",
        type: 'post',
        data: {
            'orderinfoArr': JSON.stringify(orderinfoArr),
            'orderObj': JSON.stringify(orderObj)
        },
        dataType: 'json',
        success: function (result) {
            if (result.status==1)
            alert("感谢您的选择，我们已经收到您的付款");
        }
    })
}


