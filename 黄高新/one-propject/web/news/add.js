/*
* 表单提交验证
* */
var urlUAA="";

var AddUrl="/AddNewsServlet";
var UpdateUrl="/UpdateNewsServlet";

$(function () {
    //  在ajax启动    初始模态框
    // initRole();
})

// 设置模态框 监听事件
$("#myModal").on('hide.bs.modal', function () {
})


function addnews() {

    $("#myModalLabel").text("添加新闻公告");
    // 调用显示模态框
    $("#myModal").modal('show');

    $("#newsForm")[0].reset();
    $("#staffName").val(window.localStorage.loginName);
    urlUAA= AddUrl;



}



function updatenews(row) {

    $("#myModalLabel").text("修改新闻公告");
    $("#staffName").val(window.localStorage.loginName);
    $("#title").val(row.title);
    $("#content").val(row.content);
    $("#id").val(row.id);
    $("#createdTime").val(row.createdTime);
    $("#endTime").val(row.endTime);
    $("#status").val(row.status);


    urlUAA= UpdateUrl;
    // 调用显示模态框
    $("#myModal").modal('show');

}


function aaa (){
    // 当点击提交校验输入框

    var bootstrapValidator = $("#newsForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        toastr['success']("校验成功");
        $.ajax({
            url:urlUAA,
            type:'post',
            // data:$("#newsForm").serialize(),
            data:{

                title: $("#title").val(),
                content:$("#content").val(),
                 id:$("#id").val(),
                createdTime:$("#createdTime").val(),
                endTime:$("#endTime").val(),
                status:$("#status").val(),
                staffId:window.localStorage.staffId
            },
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("操作成功");
                    $("#myModal").modal('hide');
                    $("#newsList").bootstrapTable('refresh');
                } else {
                    toastr['error']("操作失败");
                }
            }
        })
    }else {
        toastr['error']("校验失败");



        // $("#oderForm")[0].reset();

    }
}

/**
 * 关闭模态框
 */
$("#myModal").on('hide.bs.modal', function () {
    //移除上次的校验配置
    $("#newsForm").data('bootstrapValidator').resetForm();
    $("#newsForm")[0].reset();
})

/*
* 初始化表单验证
* */
$("#newsForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        title: {
            validators: {
                notEmpty: {
                    message: "标题不能为空"
                }
            }
        },
        content : {
            validators: {
                notEmpty: {
                    message: "新闻公告内容不能为空"
                }
            }
        },
        status: {
            validators: {
                notEmpty: {
                    message: "职位状态不能为空"
                }
            }
        },
        createdTime: {
        validators: {
            notEmpty: {
                message: "开始日期不能为空"
            }
        }
    },
        endTime: {
            validators: {
                notEmpty: {
                    message: "结束日期不能为空"
                }
            }
        },
        staffName: {
            validators: {
                notEmpty: {
                    message: "发布人不能为空"
                }
            }
        }
    }

});
