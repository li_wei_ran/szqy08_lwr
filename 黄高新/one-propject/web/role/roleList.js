/**
 * 权限增删改查
 * @param row
 */

$(function () {
    // 初始化分页列表
    roleManage.initList();

});
var roleManage = {};

roleManage.initList = function () {
    //给初始化分页列表赋予函数
    $("#roleList").bootstrapTable({
        url: '/GetAllRoleServlet', //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: roleManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function (e, row, element) {  // 设置行点击事件***************************
            //alert('监控行点击事件：'+row.id)
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result != null) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "职位id",
            visible: false
        }, {
            field: 'roleName',
            title: "职位名称"
        }, {
            field: 'description',
            title: "职位描述"
        }, {
            field: 'status',
            title: "职位状态",
            formatter: function (value) {
                switch (value) {
                    case 0 :
                        return "禁用";
                    case 1 :
                        return "启用";
                }
            }
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return roleManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
roleManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数

        //根据职位名称搜索职位信息
        "searchRoleName": $("#searchRoleName").val()
    }
}

/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 员工 对应的 更新按钮的updateStaff
    'click .updateRole': function (e, value, row, index) {
        //row 这一行的数据

        //1.初始模态框内的数据
        $("#id").val(row.id);
        $("#roleName").val(row.roleName);
        $("#description").val(row.description);
        $("#status").val(row.status);

        //2.显示模态框   确定按钮添加事件 使用ajax数据提交到后台
        $("#updateModal").modal('show');
    },
    // 删除员工按钮的delStaff
    'click .delRole': function (e, value, row, index) {
        roleManage.del(row);
    },

    //权限分配按钮
    'click .select': function (e, value, row, index) {
        roleManage.updateResource(row);
    }
};
/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
roleManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateRole">修改</button>');
    returnButton.push('<button class="btn btn-danger delRole">删除</button>');
    returnButton.push('<button class="btn btn-success select">权限分配</button>');
    return returnButton.join('');
}

/**
 * 刷新列表
 */
roleManage.search = function () {
    //bootstrapTable 刷新
    $("#roleList").bootstrapTable('refresh');
}

roleManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days / 365);
}

/**
 * 删除事件
 * @param row
 */
roleManage.del = function (row) {
    /**
     * 一般情况下删除要加confirm
     */
    Modal.confirm({
        msg: "确认当前操作"
    }).on(function (e) {
        if (e) {
            $.ajax({
                url: '/DeleteRoleServlet',
                type: 'post',
                data: {
                    "id": row.id,
                },
                dataType: 'json',
                success: function (result) {
                    if (result.status > 0) {
                        toastr['success']("删除成功");
                    } else {
                        toastr['error']("删除失败");
                    }
                    roleManage.search();
                }
            })
        }
    })
};

/**
 * 点击修改职位的按钮的点击事件
 */
function updateRole() {

    $.ajax({
        url:'/UpdateRoleServlet',
        type:'post',
        data: {
            "id":$("#id").val(),
            "roleName": $("#roleName").val(),
            "description": $("#description").val(),
            "status": $("#status").val(),
        },
        dataType:'json',
        success:function (result) {
            if (result.status > 0) {
                toastr['success']("更新成功");
                $("#updateModal").modal('hide');
            } else {
                toastr['error']("更新失败");
            }
            roleManage.search();
        }

    });

}

/**
 * 新增职位按钮的点击事件
 */
function addModal() {

    // 调用显示模态框
    $("#addModal").modal('show');
}

/**
 * 新增职位模态框确认按钮的点击事件
 */
function addRole() {

    $.ajax({
        url:'/AddRoleServlet',
        type:'post',
        data: {
            "addRoleName": $("#addRoleName").val(),
            "addDescription": $("#addDescription").val(),
            "addStatus": $("#addStatus").val(),
        },
        dataType:'json',
        success:function (result) {
            if (result.status > 0) {
                toastr['success']("增加成功");
                $("#addModal").modal('hide');
            } else {
                toastr['error']("增加失败");
            }
            roleManage.search();
        }
    });
}


/**
 * 权限树的模态框
 * @param row
 */

//给权限分配按钮点击事件赋予函数
roleManage.updateResource = function (row) {
    // 权限分配的模态框显示
    $("#myModaltree").modal('show');

    //获取角色id
    roleId = row.id;

    // 假设查询角色1的对应资源
    //roleId = 1;

    $.ajax({
            url:"/GetRoleListByIdServlet",
            type:"post",
            data:{
                "roleId":roleId
            },
            dataType:'json',
            success:function (result) {

                //  初始化 权限树
                $('#tree1').treeview({
                    levels: 1,
                    expandIcon: 'glyphicon glyphicon-chevron-right',
                    collapseIcon: 'glyphicon glyphicon-chevron-down',
                    selectedBackColor: false,
                    selectedColor: '#337AB7',
                    showCheckbox: true,
                    multiSelect: true,

                    data:result.data,// 得到List<TreeMenu> treeMenuList
                    onNodeChecked: function (event, node) { //设置选中节点
                        var selectNodes = getChildNodeIdArr(node); //获取所有子节点
                        if (selectNodes) { //子节点不为空，则选中所有子节点
                            $('#tree1').treeview('checkNode', [selectNodes, {
                                silent: true
                            }]);
                        }
                        $("#tree1").treeview("getNode", node.parentId);
                        setParentNodeCheck(node);
                    },
                    onNodeUnchecked: function (event, node) {//取消选中节点
                        var selectNodes = getChildNodeIdArr(node); //获取所有子节点
                        if (selectNodes) { //子节点不为空，则取消选中所有子节点
                            $('#tree1').treeview('uncheckNode', [selectNodes, {
                                silent: true
                            }]);
                        }
                    }
                })
            }
        }
    )
};



/*
 * 设置父节点
 * @param {Object} node
 */
function setParentNodeCheck(node) {
    var parentNode = $("#tree1").treeview("getNode", node.parentId);
    if (parentNode.nodes) {
        var checkedCount = 0;
        for (x in parentNode.nodes) {
            if (parentNode.nodes[x].state.checked) {
                checkedCount ++;
            } else {
                break;
            }
        }
        if (checkedCount === parentNode.nodes.length) {
            $("#tree1").treeview("checkNode", parentNode.nodeId);
            setParentNodeCheck(parentNode);
        }
    }
}

/*
 * @author
 * @date 2019/11/22
 * 获取所有的字节点
**/
function getChildNodeIdArr(node) {
    var ts = [];
    if (node.nodes) {
        for (x in node.nodes) {
            ts.push(node.nodes[x].nodeId);
            if (node.nodes[x].nodes) {
                var getNodeDieDai = getChildNodeIdArr(node.nodes[x]);
                for (j in getNodeDieDai) {
                    ts.push(getNodeDieDai[j]);
                }
            }
        }
    } else {
        ts.push(node.nodeId);
    }
    return ts;
}

/**
 *
 * 给修改权限的确认按钮设置函数
 */
roleManage.setTreeList = function () {

    //获取所有已经勾选的节点
    var getNodes = $("#tree1").treeview('getChecked');

    var getNodeList = new Array();

    //遍历得到的已经勾选的id
    for (var i = 0;i<getNodes.length;i++){
        getNodeList[i] = getNodes[i].nodeid;
    }

    //提示：1.首先将 数据转换以,分割的字符串，后台解析
    //      2. 修改业务 resource_role
    //          首先删除所有当前角色的所有资源
    //          重新设置对应角色资源
    $.ajax({
            url:"/UpdateResourceServlet", //连接后台服务器接收已经修改的角色权限
            type:"post",
            data:{
                //把角色ID对象的修改的权限传给后端
                'roleId':roleId,
                //1.首先将数据转换为以,分割的字符串，后台解析
                'nodeList':JSON.stringify(getNodeList)
            },
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("权限分配成功");
                    $("#myModaltree").modal('hide');
                } else {
                    toastr['error']("权限分配失败");
                }
                roleManage.search();
            }
        }
    )
}