package com.aaa.dao;

import com.aaa.entity.Order;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class OrderDaoImpl implements OrderDao{
    BaseDao baseDao=BaseDao.getInstance();
    @Override
    public int updateOrder(Order order) {
        String sql="update order set cardId=?,cardType=?,price=?,pay=?,credit=?,status=?,momo=? where orderId=?";
        Object[] objects={order.getCardId(),order.getCardType(),order.getPrice(),order.getPay(),order.getCredit(),order.getStatus(),order.getMomo(),order.getOrderId()};
        return baseDao.executeUpdate(sql,objects);
    }

    @Override
    public int checkStatus(Order order) {
        String sql="update order set status=? where orderId=?";
        Object[] objects={order.getStatus(),order.getOrderId()};
        return  baseDao.executeUpdate(sql,objects);
    }

    @Override
    public List<Map<String, Object>> getLevel() {
        String sql="select * from cardType";
        return baseDao.query(sql,null);
    }

    @Override
    public List<Order> getOrderList(Integer pageNumber, Integer pageSize, String searchOrderId, String searchCardId) {
        String sql="select orderId,cardId,cardType,price,pay,credit,status,momo,createdTime from order";
        if (StringUtils.isNotBlank(searchOrderId)) {
            sql += " and orderId = '" + searchOrderId + "'";
        }
        if (StringUtils.isNotBlank(searchCardId)) {
            sql += " and cardId = '" + searchCardId + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<Order> order = baseDao.query(sql,params,Order.class);
        return order;
    }

    @Override
    public int getOrderListCount(String searchOrderId, String searchCardId) {
        String sql="select count(1) len from order where 1=1";
        if (StringUtils.isNotBlank(searchOrderId)) {
            sql += " and orderId = '" + searchOrderId + "'";
        }
        if (StringUtils.isNotBlank(searchCardId)) {
            sql += " and cardId = '" + searchCardId + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }
}
