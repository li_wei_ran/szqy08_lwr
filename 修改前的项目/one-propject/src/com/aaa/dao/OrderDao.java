package com.aaa.dao;

import com.aaa.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao {
    int updateOrder(Order order);
    int checkStatus(Order order);
    List<Map<String,Object>> getLevel();
    List<Order> getOrderList(Integer pageNumber, Integer pageSize, String searchOrderId, String searchCardId);
    int getOrderListCount(String searchOrderId, String searchCardId);
}
