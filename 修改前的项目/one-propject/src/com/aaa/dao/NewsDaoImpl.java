package com.aaa.dao;

import com.aaa.dao.BaseDao;
import com.aaa.dao.NewsDao;
import com.aaa.entity.News;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class NewsDaoImpl implements NewsDao {
    private BaseDao baseDao=BaseDao.getInstance();

    @Override
    public int updateNewsByStaffId(News news) {
       String sql="update news set title=?,status=?,endTime=?,content=? where staffId=?";
       Object[] objects={news.getTitle(),news.getStatus(),news.getEndTime(),news.getContent(),news.getStaffId()};
       return baseDao.executeUpdate(sql,objects);
    }

    @Override
    public int changeStatus(Integer id,Integer status) {
        String sql = "update news set status=? where id=?";
        Object[] params = {status,id};
        return  baseDao.executeUpdate(sql,params);
    }

    @Override
    public int addNewsByStaffId(News news) {
        String sql="insert into news(title,status,staffId,createTime,endTime,content)values(?,?,?,?,?,?) ";
        Object[] objects={news.getTitle(),news.getStaffId(),news.getStatus(),news.getCreatedTime(),news.getEndTime(),news.getContent()};
        return baseDao.executeUpdate(sql,objects);
    }


    @Override
    public List<News> getAllNewsInfo(Integer pageNumber, Integer pageSize, String searchTitle, String searchName) {
        String sql="select n.*,s.staffName staffName from news n,staff s where n.staffId=s.staffId";
        if (StringUtils.isNotBlank(searchTitle)) {
            sql += " and n.title like '% " + searchTitle+ "%'";
        }
        if (StringUtils.isNotBlank(searchName)) {
            sql += " and s.staffName like '%" + searchName + "%'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<News> news = baseDao.query(sql, params, News.class);
        return news;
    }


    @Override
    public List<News> getMaxId() {
        String sql = "select max(id) as id from news ";
        return baseDao.query(sql,null,News.class);
    }

    @Override
    public int getAllNewsInfoCount(String searchTitle, String searchName) {
        String sql = "select count(1) len from news where 1 = 1";
        if (StringUtils.isNotBlank(searchTitle)) {
            searchTitle = "%" + searchTitle + "%";
            sql += " and title like' " + searchTitle + "'";
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and staffName like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }

}
