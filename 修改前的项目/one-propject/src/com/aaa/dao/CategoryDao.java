package com.aaa.dao;

import com.aaa.entity.Category;

import java.util.List;

public interface CategoryDao {
    List<Category> getCategoryList();
}
