package com.aaa.dao;

import com.aaa.entity.Category;
import com.aaa.entity.Unit;

import java.util.List;

public class CategoryDaoImpl implements CategoryDao {
    private BaseDao baseDao = BaseDao.getInstance();

    @Override
    public List<Category> getCategoryList() {
        String sql = "select id,name from category where status!=0";
        List<Category> categoryList = baseDao.query(sql,null,Category.class);
        return categoryList;
    }
}
