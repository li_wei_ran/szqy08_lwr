package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.StaffDao;
import com.aaa.entity.Staff;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class StaffDaoImpl implements StaffDao {

    private BaseDao baseDao = BaseDao.getInstance();


    @Override
    public Staff login(String staffId, String password) {
        String sql = "select * from staff where staffId = ? and password = ?  and status = 1";

        //接受用户写入的ID，密码
        Object[] params = {staffId,password};
        List<Staff> staffList = baseDao.query(sql,params, Staff.class);

        if (staffList!=null && !staffList.isEmpty()){
            return staffList.get(0);
        }

        return null;
    }

    @Override
    public int updateStaffByStaffId(Staff staff) {
        String sql = "UPDATE staff SET `staffName` = ?," +
                "phone = ?," +
                "`idCard` = ?,`address` = ?," +
                "`status` = ?," +
                "`roleId` = ?,`momo` = ?" +
                " WHERE `staffId` = ?";
        Object[] arr = { staff.getStaffName(), staff.getPhone()
                , staff.getIdCard(), staff.getAddress(), staff.getStatus(),
                staff.getRoleId(), staff.getMomo(), staff.getStaffId()};

        return baseDao.executeUpdate(sql, arr);
    }

    @Override
    public int addStaffByStaffId(Staff staff) {
        String sql = "insert into staff(staffId,password,phone,idCard,address,createdTime,status,roleId," +
                "momo,staffName)" +
                " values(?,?,?,?,?,?,?,?,?,?)";
        Object[] arr = {staff.getStaffId(),staff.getPassword(), staff.getPhone()
                , staff.getIdCard(), staff.getAddress(), staff.getCreatedTime(), staff.getStatus(),
                staff.getRoleId(), staff.getMomo(),staff.getStaffName()};
        return baseDao.executeUpdate(sql, arr);
    }

    @Override
    public int deleteStaffByStaffId(int staffId) {
        String sql = "delete from staff where staffId = ?";
        Object [] arr = {staffId};
        return baseDao.executeUpdate(sql,arr) ;
    }

    @Override
    public List<Staff> getAllStaffInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        String sql = "select s.*,r.roleName roleName from staff s,role r where s.roleId = r.id ";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and staffId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and staffName like '" + searchName + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<Staff> staff = baseDao.query(sql, params, Staff.class);
        return staff;
    }

    @Override
    public List<Staff> findStaffByStaffId(int staffId) {
        String sql = "select * from staff where staffId = ?";
        Object[] arr = {staffId};
        return baseDao.query(sql,arr,Staff.class);
    }

    @Override
    public List<Staff> findStaffByStaffName(String staffName) {
        String sql = "select * from staff where staffName = ?";
        Object[] arr = {staffName};
        return baseDao.query(sql,arr,Staff.class);
    }

    @Override
    public List<Map<String, Object>> getRole() {
        String sql = "select * from role";
        return baseDao.query(sql,null);
    }

    @Override
    public List<Staff> getMaxStaffId() {
        String sql = "select max(staffId) as staffId from staff ";
        return baseDao.query(sql,null,Staff.class);
    }

    @Override
    public int getAllStaffInfoCount(String searchId, String searchName) {
        String sql = "select count(1) len from staff where 1 = 1";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and staffId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and staffName like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }



    @Override
    public Staff getStaffInformation(int staffId) {
        //创建staff对象
        Staff staff = new Staff();

        //SQL语句
        String sql = "select * from staff where staffId = ?";

        //SQL语句里？所代表的的参数
        Object[] params = {staffId};

        //用Staff类型的List接受查询到的数据
        List<Staff> staffList = baseDao.query(sql,params, Staff.class);

        //将staffList数组里的唯一一条数据赋值给staff对象
        staff = staffList.get(0);
        return staff;
    }

    @Override
    public int updateStaffPassword(int staffId, String password) {
        String sql = "update staff set password = ? where staffId = ?";
        Object[] params = {password,staffId};
        return baseDao.executeUpdate(sql,params);
    }
}
