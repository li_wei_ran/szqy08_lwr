package com.aaa.dao;

import com.aaa.entity.Staff;

import java.util.List;
import java.util.Map;

public interface StaffDao {

    /**
     * 登录
     */
    public Staff login(String staffId, String password);


    int updateStaffByStaffId(Staff staff);

    int addStaffByStaffId(Staff staff);

    int deleteStaffByStaffId(int staffId);
    List<Staff> getAllStaffInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    //查询员工的一条记录
    Staff getStaffInformation(int staffId);

    //修改员工密码，存入数据库
    int updateStaffPassword(int staffId,String password);

    List<Staff> findStaffByStaffId(int staffId);

    List<Staff> findStaffByStaffName(String staffName);

    List<Map<String,Object>> getRole();

    List<Staff> getMaxStaffId();
    /**
     * 查询所有员工信息的总条数
     */
    int getAllStaffInfoCount(String searchId,String searchName);


}
