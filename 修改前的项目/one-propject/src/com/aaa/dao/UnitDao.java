package com.aaa.dao;

import com.aaa.entity.Unit;

import java.util.List;

public interface UnitDao {
    /**
     * 动态查询状态下拉列表
     * @return
     */
    List<Unit> getUnitList();
}
