package com.aaa.dao;

import com.aaa.entity.Unit;

import java.util.List;

public class UnitDaoImpl implements UnitDao {

    private BaseDao baseDao = BaseDao.getInstance();

    @Override
    public List<Unit> getUnitList() {
        String sql = "select id,name from unit where status!=0";
        List<Unit> units = baseDao.query(sql,null,Unit.class);
        return units;
    }
}
