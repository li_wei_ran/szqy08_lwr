package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.OrderService;
import com.aaa.service.OrderServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/GetOrderLevelServlet")
public class GetOrderLevelServlet extends HttpServlet {
    OrderService orderService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Map<String,Object>> levelName = orderService.getLevel();

        ResponseDto responseDto = new ResponseDto();
        if (levelName.size() != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setData(levelName);
            responseDto.setMessage("查询成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("查询失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        orderService=new OrderServiceImpl();
    }
}
