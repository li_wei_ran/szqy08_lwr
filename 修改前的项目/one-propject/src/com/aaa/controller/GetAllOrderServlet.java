package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.OrderService;
import com.aaa.service.OrderServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetAllOrderServlet")
public class GetAllOrderServlet extends HttpServlet {
    OrderService orderService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {

            Integer pageNumber = IntegerUtils.ToInteger(req.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(req.getParameter("pageSize"));
            String searchOrderId = req.getParameter("searchOrderId");
            String searchCardId = req.getParameter("searchCardId");
            responseDto.setData(orderService.getAllOrder(pageNumber, pageSize, searchOrderId, searchCardId));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");

        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        orderService=new OrderServiceImpl();
    }
}
