package com.aaa.controller;

import com.aaa.entity.News;
import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ChangeNewsStatusServlet")
public class ChangeNewsStatusServlet extends HttpServlet {
    NewsService newsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id=req.getParameter("id");
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        String staffName=req.getParameter("staffName");
        String status =req.getParameter("status");
        String createdTime=req.getParameter("createdTime");
        String endTime=req.getParameter("endTime");
        String staffId=req.getParameter("staffId");

        News news=new News();
        news.setId(Integer.parseInt(id));
        news.setTitle(title);
        news.setContent(content);
        news.setStatus(Integer.parseInt(status));
        news.setStaffName(staffName);
        news.setStaffId(Integer.parseInt(staffId));
        news.setEndTime(endTime);

        int num=newsService.changeStatus(Integer.parseInt(status),Integer.parseInt(id));
        ResponseDto responseDto = new ResponseDto();
        if (num != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("更新成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("更新失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        newsService =new NewsServiceImpl();
    }
}
