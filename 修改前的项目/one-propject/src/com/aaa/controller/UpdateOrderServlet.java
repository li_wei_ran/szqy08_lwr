package com.aaa.controller;

import com.aaa.entity.Order;
import com.aaa.entity.ResponseDto;
import com.aaa.service.OrderService;
import com.aaa.service.OrderServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateOrderServlet")
public class UpdateOrderServlet extends HttpServlet {
    OrderService orderService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId=req.getParameter("orderId");
        String cardId=req.getParameter("cardId");
        String cardType=req.getParameter("cardType");
        String price=req.getParameter("price");
        String pay=req.getParameter("pay");
        String credit=req.getParameter("credit");
        String status=req.getParameter("status");
        String momo=req.getParameter("momo");
        String createdTime=req.getParameter("createdTime");

        Order order=new Order();
        order.setCardId(Integer.parseInt(cardId));
        order.setPrice(Integer.parseInt(price));
        order.setPay(Integer.parseInt(pay));
        order.setCardType(Integer.parseInt(cardType));
        order.setCredit(Integer.parseInt(credit));
        order.setMomo(momo);
        order.setStatus(Integer.parseInt(status));
        order.setCreatedTime(createdTime);

        int num=orderService.updateOrder(order);

        ResponseDto responseDto = new ResponseDto();
        if (num != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("更新成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("请求失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init() throws ServletException {
        orderService=new OrderServiceImpl();
    }
}
