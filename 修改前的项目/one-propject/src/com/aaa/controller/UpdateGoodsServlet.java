package com.aaa.controller;

import com.aaa.entity.Goods;
import com.aaa.service.GoodsService;
import com.aaa.service.GoodsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateGoodsServlet")
public class UpdateGoodsServlet extends HttpServlet {
    GoodsService goodsService = null;
    @Override
    public void init() throws ServletException {
        goodsService = new GoodsServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String goodsId = req.getParameter("goodsId");
        String goodsName = req.getParameter("goodsName");
        String code = req.getParameter("code");
        String price = req.getParameter("price");
        String category = req.getParameter("category");
        String unit = req.getParameter("unit");

        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setName(goodsName);
        goods.setCode(code);
        goods.setPrice(Double.valueOf(price));
        goods.setCategoryId(Integer.valueOf(category));
        goods.setUnitId(Integer.valueOf(unit));

        goodsService.UpdateGoods(goods);

        resp.getWriter().print(new Gson().toJson(goods));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
