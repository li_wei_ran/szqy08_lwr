package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.NewsServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetAllNewsServlet")
public class GetAllNewsServlet extends HttpServlet {
    NewsService newsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            Integer pageNumber= IntegerUtils.ToInteger(req.getParameter("pageNumber"));
            Integer pageSize=IntegerUtils.ToInteger(req.getParameter("pageSize"));
            String searchTitle = req.getParameter("searchTitle");
            String searchName = req.getParameter("searchName");
            responseDto.setData(newsService.getAllNews(pageNumber, pageSize, searchTitle, searchName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        }catch (Exception e){

        }
        resp.getWriter().print(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        newsService=new NewsServiceImpl();
    }
}
