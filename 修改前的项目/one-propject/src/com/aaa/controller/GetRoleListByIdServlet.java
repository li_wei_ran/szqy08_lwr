package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.TreeMenu;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/GetRoleListByIdServlet")
public class GetRoleListByIdServlet extends HttpServlet {

    RoleService roleService = new RoleServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int roleId = IntegerUtils.ToInteger(req.getParameter("roleId"));

        // 根据一个角色  查出出来对应的数据结构   list<TreeMenu>  父节点结合，每一一个人父节点都一个子节点集合
        List<TreeMenu> treeMenuList =  roleService.getMenuList(roleId);

        ResponseDto responseDto = new ResponseDto();
        responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        responseDto.setMessage("success");
        responseDto.setData(treeMenuList);

        resp.getWriter().print(new Gson().toJson(responseDto));


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
