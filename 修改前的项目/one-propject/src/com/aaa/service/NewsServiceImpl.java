package com.aaa.service;

import com.aaa.dao.NewsDao;
import com.aaa.dao.NewsDaoImpl;
import com.aaa.entity.News;
import com.aaa.service.NewsService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsServiceImpl implements NewsService {
    private NewsDao newsDao=new NewsDaoImpl();
    @Override
    public int updateNewsByStaffId(News news) {
        return newsDao.updateNewsByStaffId(news);
    }

    @Override
    public int addNewsByStaffId(News news) {
        return newsDao.addNewsByStaffId(news);
    }

    @Override
    public List<News> getMaxId() {
        return newsDao.getMaxId();
    }

    @Override
    public Map<String, Object> getAllNews(Integer pageNumber, Integer pageSize, String searchTitle, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
        List<News> list = newsDao.getAllNewsInfo(pageNumber, pageSize, searchTitle, searchName);

        // 总页数
        int count = newsDao.getAllNewsInfoCount(searchTitle, searchName);

        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }

    @Override
    public int changeStatus(int status,int id) {
        if (status==0){
            status=1;
        }else {
            status=0;
        }
        return newsDao.changeStatus(status,id);
    }
}
