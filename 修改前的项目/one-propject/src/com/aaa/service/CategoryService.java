package com.aaa.service;

import com.aaa.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> GetCategoryList();
}
