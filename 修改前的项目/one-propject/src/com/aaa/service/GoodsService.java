package com.aaa.service;

import com.aaa.entity.Goods;
import com.aaa.entity.Staff;

import java.util.Map;

public interface GoodsService {
    /**
     * 修改商品信息
     * @param goods
     * @return
     */
    boolean UpdateGoods(Goods goods);

    /**
     * 改变商品状态
     * @param goods
     * @return
     */
    boolean ChangeStatus(Goods goods);
    /**
     * 分页查询所有商品的信息
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Map<String,Object> getAllGoodsInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName)throws Exception;

}
