package com.aaa.service;

import com.aaa.dao.CategoryDao;
import com.aaa.dao.CategoryDaoImpl;
import com.aaa.entity.Category;


import java.util.List;

public class CategoryServiceImpl implements CategoryService {
    private CategoryDao categoryDao = new CategoryDaoImpl();
    @Override
    public List<Category> GetCategoryList() {
        return categoryDao.getCategoryList();
    }
}
