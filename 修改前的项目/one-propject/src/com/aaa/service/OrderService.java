package com.aaa.service;

import com.aaa.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {
    int updateOrder(Order order);
    int checkOrder(Order order);
    List<Map<String,Object>> getLevel();
    Map<String,Object> getAllOrder(Integer pageNumber, Integer pageSize, String searchOrderId, String searchCardId)throws Exception;

}
