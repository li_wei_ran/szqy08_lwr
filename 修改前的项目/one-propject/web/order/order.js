var orderListUrl = "/GetAllOrderServlet";
var cardLevelUrl = "/GetOrderLevelServlet";
var urlUAA="";
$(function () {
    orderManage.initList();
    cardLevelManager.initList();
})

var orderManage = {};
var cardLevelManager = {};

/**
 * 在js 加载时初始化
 */
cardLevelManager.initList = function(){
    $.ajax({
        url: cardLevelUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status == 1) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].name + "</option>");
                    $("#level").append(opt);
                }
            }
        }
    })

}

orderManage.initList = function () {
    $("#orderList").bootstrapTable({
        url: orderListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: orderManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function ( row,e, element) {  // 设置行点击事件***************************
            alert('监控行点击事件：'+row.orderId)
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result.status==1) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'orderId',
            title: "订单编号",
        }, {
            field: 'cardId',
            title: "会员卡号"
        }, {
            field: 'cardType',
            title: "会员等级"
        }, {
            field: 'price',
            title: "应付金额"
        }, {
            field: 'pay',
            title: "实付金额"
        }, {
            field: 'credit',
            title: "商品积分"
        }, {
            field: 'status',
            title: "订单状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return "已核验";
                    case 2 :
                        return "未核验";
                }
            }
        }, {
            field: 'momo',
            title: "备注"
        },{
            field: 'createdTime',
            title: "创建时间"
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return orderManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 */
orderManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchOrderId": $("#searchOrderId").val(),
        "searchCardId": $("#searchCardId").val()
    }
}

/**
 * 按钮的点击事件
 * */
window.buttonOperateEvent = {
    // 更新
    'click .updateOrder': function (e, value, row, index) {
        updateOrder(row);
    },
    // 审核
    'click .check': function (e, value, row, index) {
        check(row);
    }
}
/**
 * 为每一行添加按钮
 */
orderManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateOrder">修改</button>');
    returnButton.push('<button class="btn btn-danger check">审核</button>');
    return returnButton.join('');
}

/**
 * 刷新列表
 */
orderManage.search = function () {
    //bootstrapTable 刷新
    $("#OrderList").bootstrapTable('refresh');
}

orderManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days/365);
}



function updateOrder(row) {
    $("#myModalLabel").text("修改订单");
    $("#orderId").val(row.orderId);
    $("#cardId").val(row.cardId);
    $("#level").val(row.level);
    $("#price").val(row.price);
    $("#pay").val(row.pay);
    $("#credit").val(row.credit);
    $("#momo").val(row.momo);
    $("#status").val(row.status);
    $("#createdTime").val(row.createdTime);
    urlUAA= "/UpdateOrderServlet";
    // 调用显示模态框
    $("#myModal").modal('show');

}
function confirm(){
    // 当点击提交校验输入框
    var bootstrapValidator = $("#orderForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        alert("校验成功");
        $.ajax({
            url:urlUAA,
            type:'post',
            data:$("#orderForm").serialize(),
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("操作成功");
                    $("#myModal").modal('hide');
                    $("#orderList").bootstrapTable('refresh');
                } else {
                    toastr['error']("操作失败");
                }
            }
        })
    }else {
        alert("校验失败");
    }
}

/**
 * 关闭模态框
 */
$("#myModal").on('hide.bs.modal', function () {
    //移除上次的校验配置
    $("#orderForm").data('bootstrapValidator').resetForm();
    $("#orderForm")[0].reset();
})

/*
* 初始化表单验证
* */
$("#orderForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        cardId: {
            validators: {
                notEmpty: {
                    message: "请输入会员卡号"
                }
            }
        },
        price: {
            validators: {
                notEmpty: {
                    message: "请输入应付金额"
                }
            }
        },
        pay: {
            validators: {
                notEmpty: {
                    message: "请输入实付金额"
                }
            }
        },
        credit: {
            validators: {
                notEmpty: {
                    message: "请输入商品积分"
                }
            }
        }
    }
});
