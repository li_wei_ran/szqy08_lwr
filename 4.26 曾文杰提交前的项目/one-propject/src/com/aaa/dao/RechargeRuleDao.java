package com.aaa.dao;

import com.aaa.entity.RechargeRule;

import java.util.List;

public interface RechargeRuleDao {

    List<RechargeRule> getAllRechargeRule();

    //分页查询所有充值规则
    List<RechargeRule> getAllRechargeRuleInfo(Integer pageNumber, Integer pageSize, String name, String status);
    //根据名字或者状态查询充值规则
    int getAllRechargeRuleInfoCount(String name, String status);

    //新增充值规则
    int addRechargeRule(RechargeRule rechargeRule);

    //更新充值规则（根据id）
    int updateRechargeRule(RechargeRule rechargeRule);


    int deleteRechargeRule(int id);

    //更新的时候如果是禁用状态，在切换状态的时候要更新启用时间
    //如果是启用状态，在切换的时候要更新禁用时间
    int updateStatus(RechargeRule rechargeRule);


}
