package com.aaa.dao;

import com.aaa.entity.Goods;

import java.util.List;

public interface GoodsDao {

    /**
     * 修改商品信息
     * @param goods
     * @return
     */
    int updateGoods(Goods goods);

    /**
     * 改变商品状态
     * @param goods
     * @return
     */
    int changeStatus(Goods goods);

    /**
     * 分页查询所有的商品信息
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     */
    List<Goods> getAllGoodsInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    /**
     * 查询关键字段所有商品信息的总条数
     * @param searchId
     * @param searchName
     * @return
     */
    int getAllGoodsInfoCount(String searchId,String searchName);


    /**
     * 新增商品信息
     * @param goods
     * @return
     */
    int addGoods(Goods goods);


    /**
     * 初始化商品编号
     */
    List<Goods> initGoodsId();
}
