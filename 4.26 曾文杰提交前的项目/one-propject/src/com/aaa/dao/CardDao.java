package com.aaa.dao;

import javax.smartcardio.Card;
import java.util.List;

public interface CardDao {
    int updateCardByCardId(Card card);

    int addCardByCardId(Card card);

    /**
     * 分页查询所有会员信息
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     */
    List<Card> getAllCardInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    int getAllCardInfoCount(String searchId,String searchName);
}
