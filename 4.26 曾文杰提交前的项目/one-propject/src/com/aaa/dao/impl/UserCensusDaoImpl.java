package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.UserCensusDao;
import com.aaa.entity.UserCensus;

import java.util.List;
import java.util.Map;

public class UserCensusDaoImpl implements UserCensusDao {

    BaseDao baseDao = BaseDao.getInstance();

    @Override
    public int addUser(UserCensus userCensus) {

        String sql = "insert into user (cardId,userName,status,phone,idCard,sex,address,createdTime,momo,area)"+
                "values (?,?,?,?,?,?,?,?,?,?)";

        Object[] params = {userCensus.getCardId(), userCensus.getUserName(), userCensus.getStatus(), userCensus.getPhone(), userCensus.getIdCard(), userCensus.getSex()
                , userCensus.getAddress(), userCensus.getCreatedTime(), userCensus.getMomo(), userCensus.getArea()};

        return baseDao.executeInsert(sql,params);
    }

    @Override
    public List<Map<String, Object>> getDataByNearYear() {
        String sql = "select COUNT(*) amount,SUBSTRING(createdTime FROM 6 FOR 2) month from `user` GROUP BY SUBSTRING(createdTime FROM 6 FOR 2)";
        return baseDao.query(sql,null);
    }


}
