package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.CardDao;

import javax.smartcardio.Card;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class CardDaoImpl implements CardDao {
    private BaseDao baseDao = BaseDao.getInstance();
    private PreparedStatement pstm;
    private Connection conn;
    @Override
    public int updateCardByCardId(Card card) {
        int i = 0;
        String sql = "update card set userName= ?,phone=?," +
                "status=?,idCard=?,cardId=?,address=? where  id = ? ";
        i =  baseDao.executeUpdate(sql,new Object[]{});
        return i;
    }

    @Override
    public int addCardByCardId(Card card) {
        return 0;
    }

    @Override
    public List<Card> getAllCardInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        return null;
    }

    @Override
    public int getAllCardInfoCount(String searchId, String searchName) {
        return 0;
    }
}
