package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.RechargeRuleDao;
import com.aaa.entity.RechargeRule;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class RechargeRuleDaoImpl implements RechargeRuleDao {

    private BaseDao baseDao = BaseDao.getInstance();

    @Override
    public List<RechargeRule> getAllRechargeRule() {
        String sql = "select * from rechargerule where status =1 and endTime >= now()";
        List query = baseDao.query(sql, null, RechargeRule.class);
        return query;
    }

    //分页查询所有
    @Override
    public List<RechargeRule> getAllRechargeRuleInfo(Integer pageNumber, Integer pageSize, String name, String status) {
        //System.out.println(name);
        String sql = "select * from rechargerule   ";
        if (StringUtils.isNotBlank(status)) {
            sql += " where status != " + status;
        }
        if (StringUtils.isNotBlank(name)) {
            String name1;
            name1 = "%" + name + "%";
            sql += " and name like '"+ name1 +"'";

        }
        sql += " limit ?,?";
        //System.out.println(sql);
        Object[] params = {pageNumber, pageSize};
        List<RechargeRule> rechargeRule  = baseDao.query(sql, params, RechargeRule.class);
        return rechargeRule;
    }
    @Override
    public int getAllRechargeRuleInfoCount(String name, String status) {

        String sql = "select count(1) len from rechargerule ";
        if (StringUtils.isNotBlank(status)) {
            sql += " where status != " + status;
        }
        if (StringUtils.isNotBlank(name)) {
            String name1;
            name1 = "%" + name + "%";
            sql += " and name like '" +name1+ "'";
        }
        //System.out.println(sql);
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }

    //新增充值规则
    @Override
    public int addRechargeRule(RechargeRule rechargeRule) {
        String sql = "insert into rechargerule (name,coefficient,createdTime,endTime,status,startMoney)values(?,?,?,?,?,?)";
        Object[] params = {rechargeRule.getName(),rechargeRule.getCoefficient(),rechargeRule.getCreatedTime(),rechargeRule.getEndTime(),rechargeRule.getStatus(),rechargeRule.getStartMoney()};
        int len = baseDao.executeInsert(sql,params);
        return  len;
    }

    //更新充值规则
    @Override
    public int updateRechargeRule(RechargeRule rechargeRule) {
        String sql = "update rechargerule set name=?,coefficient=?,createdTime=?,endTime=?,status=?,startMoney=? where id = ?";
        Object[] params = {rechargeRule.getName(),rechargeRule.getCoefficient(),rechargeRule.getCreatedTime(),rechargeRule.getEndTime(),rechargeRule.getStatus(),rechargeRule.getStartMoney(),rechargeRule.getId()};
        int len = baseDao.executeUpdate(sql,params);
        return len;
    }

    //删除充值规则
    @Override
    public int deleteRechargeRule(int id) {
        return 0;
    }

    //调换禁用启用状态
    @Override
    public int updateStatus(RechargeRule rechargeRule) {//1是启用状态，0是禁用状态
        int len;
        if(rechargeRule.getStatus()==1){//说明当前是启用状态，要切换到禁用状态
            String sql="update rechargerule set status=0,endTime=? where id=?";
            Object[] params = {rechargeRule.getCreatedTime(),rechargeRule.getId()};
            len = baseDao.executeUpdate(sql,params);
        }else{//说明当前状态是禁用状态，要切换到启用状态
            String sql="update rechargerule set status=1,createdTime=? where id=?";
            Object[] params = {rechargeRule.getCreatedTime(),rechargeRule.getId()};
            len = baseDao.executeUpdate(sql,params);
        }
        return len;
    }
}
