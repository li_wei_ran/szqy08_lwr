package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.RechargeCensusDao;
import com.aaa.entity.RechargeCensus;

import java.util.List;
import java.util.Map;

public class RechargeCensusDaoImpl implements RechargeCensusDao {
    BaseDao baseDao= BaseDao.getInstance();
    @Override
    public int addRecharge(RechargeCensus rechargeCensus) {
        String sql = "insert into rechargerecord (rechargeAmount,createdTime)"+
                "values (?,?)";

        Object[] params = {rechargeCensus.getCreatedTime(),rechargeCensus.getRechargeAmount()};

        return baseDao.executeInsert(sql,params);
    }

    @Override
    public List<Map<String, Object>> getDataByNearYear() {
        String sql = "select COUNT(*) amount,SUBSTRING(createdTime FROM 6 FOR 2) month from `rechargerecord` GROUP BY SUBSTRING(createdTime FROM 6 FOR 2)";
        return baseDao.query(sql,null);
    }
}
