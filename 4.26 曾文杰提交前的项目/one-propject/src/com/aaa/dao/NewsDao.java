package com.aaa.dao;

import com.aaa.entity.News;

import java.util.List;

public interface NewsDao {
    int updateNewsByStaffId(News news);
    int changeStatus(Integer id,Integer status);
    int addNewsByStaffId(News news);
    List<News> getAllNewsInfo(Integer pageNumber, Integer pageSize, String searchTitle, String searchName);

    List<News> getMaxId();

    int getAllNewsInfoCount(String searchTitle,String searchName);

}
