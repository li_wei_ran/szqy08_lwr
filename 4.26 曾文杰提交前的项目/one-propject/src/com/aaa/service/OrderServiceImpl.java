package com.aaa.service;


import com.aaa.dao.OrderDao;
import com.aaa.dao.OrderDaoImpl;
import com.aaa.entity.Order;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderServiceImpl implements OrderService {
    OrderDao orderDao=new OrderDaoImpl();

    @Override
    public int updateOrder(Order order) {
        return orderDao.updateOrder(order);
    }

    @Override
    public int checkOrder(Order order) {
        return orderDao.checkStatus(order);
    }

    @Override
    public List<Map<String, Object>> getLevel() {
        return orderDao.getLevel();
    }

    @Override
    public Map<String, Object> getAllOrder(Integer pageNumber, Integer pageSize, String searchOrderId, String searchCardId) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
        List<Order> list = orderDao.getOrderList(pageNumber, pageSize, searchOrderId, searchCardId);

        // 总页数
        int count = orderDao.getOrderListCount(searchOrderId, searchCardId);

        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }
}
