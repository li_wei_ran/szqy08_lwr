package com.aaa.service.impl;

import com.aaa.dao.CardDao;
import com.aaa.dao.impl.CardDaoImpl;
import com.aaa.entity.Card;
import com.aaa.service.CardService;
import com.aaa.util.BusinessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardServiceImpl implements CardService {
    private CardDao cardDao = new CardDaoImpl();
    private PreparedStatement pstm;
    private Connection conn;

    @Override
    public int updateCardByCardId(Card card) {
        return 0;
    }

    @Override
    public int addCardByCardId(Card card) {
        return 0;
    }

    @Override
    public Map<String, Object> getAllCardInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
       List<javax.smartcardio.Card> list = cardDao.getAllCardInfo(pageNumber,pageSize,searchStuno,searchName);

        // 总页数
        int count = cardDao.getAllCardInfoCount(searchStuno, searchName);

        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }
}
