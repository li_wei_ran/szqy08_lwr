package com.aaa.service;

import com.aaa.entity.Unit;

import java.util.List;

public interface UnitService {
    List<Unit> getUnitList();
}
