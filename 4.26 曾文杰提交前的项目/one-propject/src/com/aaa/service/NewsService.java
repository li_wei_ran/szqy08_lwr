package com.aaa.service;

import com.aaa.entity.News;

import java.util.List;
import java.util.Map;

public interface NewsService {
    int updateNewsByStaffId(News news);
    int addNewsByStaffId(News news);

    List<News> getMaxId();

    Map<String,Object> getAllNews(Integer pageNumber, Integer pageSize, String searchTitle, String searchName)throws Exception;

    int changeStatus(int id,int status);
}
