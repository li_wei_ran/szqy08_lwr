package com.aaa.service.impl;

import com.aaa.dao.OrderCensusDao;
import com.aaa.dao.impl.OrderCensusDaoImpl;
import com.aaa.entity.OrderCensus;
import com.aaa.service.OrderCensusService;

import java.util.List;
import java.util.Map;

public class OrderCensusServiceImpl implements OrderCensusService {
    private OrderCensusDao orderCensusDao=new OrderCensusDaoImpl();
    @Override
    public boolean addOrder(OrderCensus orderCensus, double amount) {
        return false;
    }

    @Override
    public List<Map<String, Object>> getDataByNearYear() {
        return orderCensusDao.getDataByNearYear();
    }
}
