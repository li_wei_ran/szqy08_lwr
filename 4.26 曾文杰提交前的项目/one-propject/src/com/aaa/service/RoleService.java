package com.aaa.service;


import com.aaa.entity.Role;
import com.aaa.entity.TreeMenu;
import com.aaa.util.BusinessException;

import java.util.List;
import java.util.Map;

public interface RoleService {

    List<Role> getAllRole();

    /**
     * 搜索 分页获取所有角色
     * @param pageNumber
     * @param pageSize
     * @param searchName
     * @return
     * @throws BusinessException
     */
    Map<String,Object> getAllRoleInfo(int pageNumber, int pageSize, String searchName) throws BusinessException;


    //更新，新增角色
    int updateOrAddRole(int id, String roleName, String description, int status);

    List<TreeMenu> getMenuList(int roleId);

    /**
     * 更新角色权限
     * @param roleId
     * @param resouceces
     * @return
     */
    int updateMenuList(int roleId, int[] resouceces);


    //删除角色
    int deleteRole(Integer id);

    //删除resource_role中的角色
    int deleteRole1(Integer id);

    //增加resource_role中的
    int addRole1(Integer id,Integer resource_id);

}
