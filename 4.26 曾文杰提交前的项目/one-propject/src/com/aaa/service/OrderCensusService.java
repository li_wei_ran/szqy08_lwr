package com.aaa.service;

import com.aaa.entity.OrderCensus;

import java.util.List;
import java.util.Map;

public interface OrderCensusService {
    /**
     * 增加用
     * @return
     */
    boolean addOrder(OrderCensus orderCensus, double amount);

    List<Map<String,Object>> getDataByNearYear();

}
