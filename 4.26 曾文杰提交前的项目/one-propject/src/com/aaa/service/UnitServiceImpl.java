package com.aaa.service;

import com.aaa.dao.UnitDao;
import com.aaa.dao.UnitDaoImpl;
import com.aaa.entity.Unit;

import java.util.List;

public class UnitServiceImpl implements UnitService {
    private UnitDao unitDao = new UnitDaoImpl();
    @Override
    public List<Unit> getUnitList() {
        return unitDao.getUnitList();
    }
}
