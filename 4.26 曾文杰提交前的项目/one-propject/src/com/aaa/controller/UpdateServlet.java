package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Staff;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
    StaffService staffService;
    @Override
    public void init() throws ServletException {
        staffService = new StaffServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String id = request.getParameter("id");
        String staffId = request.getParameter("staffId");
        String staffName = request.getParameter("staffName");
        String phone = request.getParameter("phone");
        String idCard = request.getParameter("idCard");
        String status = request.getParameter("status");
        String roleId = request.getParameter("roleId");
        String momo = request.getParameter("remake");
        String address = request.getParameter("address");
        String password = request.getParameter("password");
        String createdate = request.getParameter("createdate");
        Staff staff = new Staff();
        ResponseDto responseDto = new ResponseDto();
        try{

            if (staffId!=null){
                staff.setId(Integer.valueOf(id));
                staff.setStaffId(Integer.valueOf(staffId));
                staff.setStaffName(staffName);
                staff.setPhone(phone);
                staff.setIdCard(idCard);
                staff.setStatus(Integer.valueOf(status));
                staff.setRoleId((roleId==null || "".equals(roleId))?null:Integer.valueOf(roleId));
//                staff.setRoleId(Integer.valueOf(roleId));
                staff.setMomo(momo);
                staff.setAddress(address);
                staff.setPassword(password);
                staff.setCreatedTime(createdate);
//                try {
                    System.out.println("---"+staff);
                    staffService.updateStaffByStaffId(staff);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setMessage("success!!");
            }else {
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setMessage("fail!!不存在该用户");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        response.getWriter().print(new Gson().toJson(responseDto));

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
