package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.CategoryService;
import com.aaa.service.CategoryServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetAllCategoryServlet")
public class GetAllCategoryServlet extends HttpServlet {
    CategoryService categoryService = null;
    @Override
    public void init() throws ServletException {
        categoryService = new CategoryServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //返回参数
        ResponseDto responseDto = new ResponseDto();
        try {

            Integer pageNumber = IntegerUtils.ToInteger(request.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(request.getParameter("pageSize"));
            String searchCategoryId = request.getParameter("searchCategoryId");
            String searchCategoryName = request.getParameter("searchCategoryName");
            responseDto.setData(categoryService.getAllCategoryInfo(pageNumber, pageSize, searchCategoryId, searchCategoryName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");

        }
        response.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
