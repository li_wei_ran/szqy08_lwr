package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.UserCensusService;
import com.aaa.service.impl.UserCensusServiceImpl;
import com.aaa.util.DateUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/UserCensusServlet")
public class UserCensusServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserCensusService userCensusService =new UserCensusServiceImpl();

        int[] months= DateUtils.getMonthByNearYear();
        List<Map<String, Object>> mapList = userCensusService.getDataByNearYear();
        double[] values = new double[12];
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < mapList.size(); j++) {
                if (months[i] == Integer.parseInt(mapList.get(j).get("month") + "")) {
                    values[i] = Double.parseDouble(mapList.get(j).get("amount") + "");
                }
            }
        }
        //返回参数
        ResponseDto responseDto = new ResponseDto();
        Map<String,Object> map = new HashMap<>();
        map.put("months",months);
        map.put("values",values);
        responseDto.setData(map);
        response.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

}
