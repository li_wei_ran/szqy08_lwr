package com.aaa.controller;

import com.aaa.entity.News;
import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/AddNewsServlet")
public class AddNewsServlet extends HttpServlet {
    NewsService newsService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        String status =req.getParameter("status");
        String createdTime=req.getParameter("createdTime");
        String endTime=req.getParameter("endTime");

        News news=new News();
        news.setId(newsService.getMaxId().get(0).getId()+1);
        news.setTitle(title);
        news.setContent(content);
        news.setStatus(Integer.parseInt(status));
        news.setCreatedTime(createdTime);
        news.setEndTime(endTime);

        Date date=new Date();
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
        String time=formatter.format(date);
        news.setCreatedTime(time);

        int num = newsService.addNewsByStaffId(news);

        ResponseDto responseDto = new ResponseDto();
        if (num != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("添加成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("添加失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void init() throws ServletException {
        newsService=new NewsServiceImpl();
    }
}
