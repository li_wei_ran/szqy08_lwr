package com.aaa.controller;

import com.aaa.entity.Card;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetCardUpdateServlet")
public class GetCardUpdateServlet extends HttpServlet {
    CardService cardService;
    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String id = request.getParameter("id");
        String userId = request.getParameter("userId");
        String userName = request.getParameter("userName");
        String phone = request.getParameter("phone");
        String status = request.getParameter("status");
        String idCard = request.getParameter("idCard");
        String birthday = request.getParameter("birthday");
        String sex = request.getParameter("sex");
        String address = request.getParameter("address");
        String area = request.getParameter("area");
        String createTime = request.getParameter("createTime");
        Card card = new Card();
        ResponseDto responseDto = new ResponseDto();
        try{

            if (userId!=null){
                card.setId(Integer.valueOf(id));
                card.setUserId(Integer.valueOf(userId));
                card.setUserName(userName);
                card.setPhone(phone);
                card.setStatus(Integer.valueOf(status));
                card.setIdCard(idCard);
                card.setBirthday(birthday);
                card.setSex(Integer.valueOf(sex));
                card.setAddress(address);
                card.setAddress(address);
                card.setCreatedTime(createTime);
//                try {
                System.out.println("---"+card);
                cardService.updateCardByCardId(card);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setMessage("success!!");
            }else {
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setMessage("fail!!不存在该用户");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        response.getWriter().print(new Gson().toJson(responseDto));

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
