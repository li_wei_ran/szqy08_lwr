package com.aaa.controller;

import com.aaa.entity.RechargeRule;
import com.aaa.entity.ResponseDto;
import com.aaa.service.RechargeRuleService;
import com.aaa.service.impl.RechargeRuleServiceImpl;
import com.aaa.util.DateUtils;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/ChangeRuleStatusServlet")
public class ChangeRuleStatusServlet extends HttpServlet {

    RechargeRuleService rechargeRuleService;
    @Override
    public void init() throws ServletException {
        rechargeRuleService = new RechargeRuleServiceImpl();

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        Integer status = IntegerUtils.ToInteger(req.getParameter("status"));
        Integer id = IntegerUtils.ToInteger(req.getParameter("id"));
        Date  createdTime = DateUtils.toDate(req.getParameter("createdTime"));
        System.out.println(createdTime);
        try{
            RechargeRule rechargeRule = new RechargeRule();
            rechargeRule.setId(id);
            rechargeRule.setStatus(status);
            rechargeRule.setCreatedTime(createdTime);
            int len = rechargeRuleService.updateStatus(rechargeRule);
            if(len!=-1){
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setMessage("更改状态成功");
                responseDto.setData(rechargeRule);
                resp.getWriter().write(new Gson().toJson(responseDto));
            }else{
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setMessage("更新状态失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("更新状态失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}

