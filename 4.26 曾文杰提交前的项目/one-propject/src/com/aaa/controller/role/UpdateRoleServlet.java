package com.aaa.controller.role;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateRoleServlet")
public class UpdateRoleServlet extends HttpServlet {

    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        roleService = new RoleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            //通过ajax,获取roleList.js里data 传过来的数据
            String id = req.getParameter("id");
            String roleName = req.getParameter("roleName");
            String description = req.getParameter("description");
            String status = req.getParameter("status");

            //调用roleService里修改的方法
            roleService.updateOrAddRole(Integer.valueOf(id),roleName,description, Integer.valueOf(status));

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("修改成功");
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("修改失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
