package com.aaa.controller.Staff;


import com.aaa.entity.ResponseDto;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateStaffPasswordServlet")
public class UpdateStaffPasswordServlet extends HttpServlet {
    private StaffService staffService;

    @Override
    public void init() throws ServletException {
        staffService = new StaffServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            //获取ajax传过来的ID和密码
            int staffId = Integer.valueOf(req.getParameter("staffId"));
            String password = req.getParameter("newPassword");

            //根据参数 staffId,password ，调用staffService.updateStaffPassword方法修改密码
            staffService.updateStaffPassword(staffId,password);

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("修改成功");
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("修改失败");
        }
        //将Java对象转换成json字符串传给前端
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
