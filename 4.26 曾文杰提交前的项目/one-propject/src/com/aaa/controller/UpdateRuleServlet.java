package com.aaa.controller;

import com.aaa.entity.RechargeRule;
import com.aaa.entity.ResponseDto;
import com.aaa.service.RechargeRuleService;
import com.aaa.service.impl.RechargeRuleServiceImpl;
import com.aaa.util.DateUtils;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/UpdateRuleServlet")
public class UpdateRuleServlet extends HttpServlet {

    RechargeRuleService rechargeRuleService;
    @Override
    public void init() throws ServletException {
        rechargeRuleService = new RechargeRuleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");
        double coefficient = Double.parseDouble(req.getParameter("coefficient"));
        Integer startMoney = IntegerUtils.ToInteger(req.getParameter("startMoney"));
        Integer status = IntegerUtils.ToInteger(req.getParameter("status"));
        Date createdTime = DateUtils.toDate(req.getParameter("createdTime"));
        Date endTime = DateUtils.toDate(req.getParameter("endTime"));
        Integer id = IntegerUtils.ToInteger(req.getParameter("id"));

        ResponseDto responseDto = new ResponseDto();
        try{
            RechargeRule rechargeRule = new RechargeRule();
            rechargeRule.setName(name);
            rechargeRule.setCoefficient(coefficient);
            rechargeRule.setStartMoney(startMoney);
            rechargeRule.setStatus(status);
            rechargeRule.setCreatedTime(createdTime);
            rechargeRule.setEndTime(endTime);
            rechargeRule.setId(id);
            int len = rechargeRuleService.updateRechargeRule(rechargeRule);
            if (len!=0){
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setMessage("更新成功");
                responseDto.setData(rechargeRule);
                resp.getWriter().write(new Gson().toJson(responseDto));
            }else{
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setMessage("更新失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
