package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RechargeRuleService;
import com.aaa.service.impl.RechargeRuleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetAllRuleServlet")
public class GetAllRuleServlet extends HttpServlet {

    RechargeRuleService rechargeRuleService;
    @Override
    public void init() throws ServletException {
        rechargeRuleService = new RechargeRuleServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            Integer pageNumber = IntegerUtils.ToInteger(req.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(req.getParameter("pageSize"));
            String name = req.getParameter("name1");
            String status = req.getParameter("status");
            responseDto.setData(rechargeRuleService.getAllRechargeRuleInfo(pageNumber, pageSize,name,status));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
