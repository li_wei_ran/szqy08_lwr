package com.aaa.entity;

public class RechargeCensus {
    private double rechargeAmount;
    private String  createdTime;

    public double getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(double rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public String toString() {
        return "RechargeCensus{" +
                "rechargeAmount=" + rechargeAmount +
                ", createdTime='" + createdTime + '\'' +
                '}';
    }
}