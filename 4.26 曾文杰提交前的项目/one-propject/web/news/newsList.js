var newsListUrl = "/GetAllNewsServlet";
$(function () {
    // 初始化分页列表
    newsManage.initList();
})

var newsManage = {};

newsManage.initList = function () {
    $("#newsList").bootstrapTable({
        url: newsListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: newsManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [5, 10, 15, 20], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        data:{"staffId":window.localStorage.getItem("staffId")},
        // onClickRow: function ( row,e, element) {  // 设置行点击事件***************************
        //     alert('监控行点击事件：'+row.staffId)
        // },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result!=null) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "排序"
            //visible: false
        }, {
            field: 'title',
            title: "标题"
        }, {
            field: 'staffName',
            title: "发布人"
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return "禁用";
                    case 2 :
                        return "启用";
                }
            }
        }, {
            field: 'createdTime',
            title: "创建时间"
        }, {
            field: 'endTime',
            title: "结束时间"
        }, {
            field: 'content',
            title: "内容"
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return newsManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 */
newsManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchTitle": $("#searchTitle").val(),
        "searchName": $("#searchName").val()
    }
}

/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 员工 对应的 更新按钮的updateStaff
    'click.updateNews': function (e, value, row, index) {
        newsManage.updateNews(row);
        $("#myModalLabel").text("修改新闻");
        $("#id").val(row.id);
        $("#title").val(row.title);
        $("#endTime").val(row.endTime);
        $("#content").val(row.content);
        $("#status").val(row.status);
        $("#myModal").modal('show');
    },
     'click .changeStatus': function (e, value, row, index) {
         newsManage.change(row);
     }
}
/**
 * 为每一行添加按钮
 */
newsManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateNews">修改</button>');
    returnButton.push('<button class="btn btn-danger changeStatus">禁用/启用</button>');
    return returnButton.join('');
}

/**
 * 刷新列表
 */
newsManage.search = function () {
    //bootstrapTable 刷新
    $("#newsList").bootstrapTable('refresh');
}

newsManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days/365);
}


/**
 * 更新员工信息
 */
function updateNews(row) {
    $.ajax({
        url:"/UpdateNewsServlet",
        type:'post',
        data:{
            "id":$("#id").val(),
            "title":$("#title").val(),
            "endTime":$("#endTime").val(),
            "content":$("#content").val(),
            "status":$("#status").val(),
        },
        dataType:'json',
        success:function (result) {
            if (result!=null){
                toastr['success']("更改成功");
                $("#myModal").modal('hide');
                newsManage.search();
            } else {
                toastr['error']("修改失败");
            }
        }
    })
}
newsManage.change=function(row) {
    Modal.confirm({
        msg: "是否启用/禁用新闻?"
    }).on(function (e) {
        if (e) {
            $.ajax({
                url: "/ChangeNewsStatusServlet",
                type: 'post',
                data: {
                    "id":row.id,
                    "status": row.status
                },
                dataType: 'json',
                success: function (result) {
                    if (result.id > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                    newsManage.search();
                },error:function (result) {
                    toastr['error']("切换失败");
                }
            })
        }
    })
}
function confirm(){
    // 当点击提交校验输入框
    var bootstrapValidator = $("#newsForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        alert("校验成功");
        $.ajax({
            url:"/GetAllNewsServlet",
            type:'post',
            data:$("#newsForm").serialize(),
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("操作成功");
                    $("#myModal").modal('hide');
                    $("#newsList").bootstrapTable('refresh');
                } else {
                    toastr['error']("操作失败");
                }
            }
        })
    }else {
        alert("校验失败");
    }
}

/**
 * 关闭模态框
 */
$("#myModal").on('hide.bs.modal', function () {
    //移除上次的校验配置
    $("#newsForm").data('bootstrapValidator').resetForm();
    $("#newsForm")[0].reset();
})

/*
* 初始化表单验证
* */
$("#newsForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        newsTitle: {
            validators: {
                notEmpty: {
                    message: "标题不能为空"
                }
            }
        },
        createdTime: {
            validators: {
                notEmpty: {
                    message: "创建时间不能为空"
                }
            }
        },
        endTime: {
            validators: {
                notEmpty: {
                    message: "结束时间不能为空"
                }
            }
        },
        content: {
            validators: {
                notEmpty: {
                    message: "内容不能为空"
                }
            }
        }
    }
});
function addNews() {
    $("#myModalLabel").text("发布新闻/公告");
    // 调用显示模态框
    $("#myModal").modal('show');
    $("#newsForm")[0].reset();
    $.ajax({
        url: "/AddNewsServlet",// 自己完成后台
        type: 'post',
        data: {
            "id":$("#id").val(),
            "title": $("#title").val(),
            "content":$("#content").val(),
            "status":$("#status").val(),
            "staffName": $("#staffName").val(),
            "createdTime": $("#createdTime").val(),
            "endTime": $("#endTime").val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > -1) {
                toastr['success']("操作成功");
                $("#myModal").modal('hide');
                $("#newsList").bootstrapTable('refresh');
            } else {
                toastr['error']("操作失败");
            }
        }
    })
}