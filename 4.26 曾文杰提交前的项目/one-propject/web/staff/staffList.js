var staffListUrl = "/GetStaffAllServlet";
var initRoleUrl = "/GetRoleServlet";
var urlUAA="";
$(function () {
    // 初始化分页列表
    staffManage.initList();
    // 初始化员工角色下拉
    roleManager.initList();
})

var staffManage = {};
var roleManager = {};

/**
 * 在js 加载时初始化  员工角色表
 */
roleManager.initList = function(){

    // 怎么初始化？
    // ajax 请求 角色表 并填充角色 下拉option

    $.ajax({
        url: initRoleUrl, //这个servlet 自己实现
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status == 1) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].roleName + "</option>");
                    $("#roleId").append(opt);
                }
            }
        }
    })

}

staffManage.initList = function () {
    $("#staffList").bootstrapTable({
        url: staffListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: staffManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function ( row,e, element) {  // 设置行点击事件***************************
            alert('监控行点击事件：'+row.staffId)
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result.status==1) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "商品编号",
            visible: false
        }, {
            field: 'staffId',
            title: "员工编号"
        }, {
            field: 'staffName',
            title: "员工姓名"
        }, {
            field: 'idCard',
            title: "员工年龄",
            formatter: function (value) {    //根据需求 进行第二次配置***********************************
                var res = "-";
                if (value != "" && value != null) {
                    res = value.substr(6, 4) + "/" + value.substr(10, 2) + "/" + value.substr(12, 2);
                    res = staffManage.getNow(res + " 00:00:00");
                }
                return res;
            }
        }, {
            field: 'phone',
            title: "员工手机号"
        }, {
            field: 'idCard',
            title: "员工身份证"
        }, {
            field: 'address',
            title: "员工地址"
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return "在职";
                    case 2 :
                        return "离职";
                    case 3 :
                        return "黑名单";
                }
            }
        }, {
            field: 'roleName',
            title: "角色"
        }, {
            field: 'momo',
            title: "备注"
        }, {
            field: 'createdTime',
            title: "创建时间"
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return staffManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
staffManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchStaffId": $("#searchStaffId").val(),
        "searchName": $("#searchName").val()
    }
}

/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 员工 对应的 更新按钮的updateStaff
    'click .updateStaff': function (e, value, row, index) {
        //row 这一行的数据
        //alert(row.staffName);
        // 初始化更新的模态框  能不能 把我们  修改 员工的模态显示出来
        //1.初始模态框内的数据
        updateStaff(row);
    },
    // 删除员工 更新按钮的delStaff
    'click .delStaff': function (e, value, row, index) {
        staffManage.del(row);
    }
}
/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
staffManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateStaff">修改</button>');
    returnButton.push('<button class="btn btn-danger delStaff">删除</button>');
    return returnButton.join('');
}

/**
 * 刷新列表
 */
staffManage.search = function () {
    //bootstrapTable 刷新
    $("#staffList").bootstrapTable('refresh');
}

staffManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days/365);
}

/**
 * 删除员工
 *1.需要 确认弹框
 *        a.在html 配置弹框布局 直接cp
 *        b. 调用Modal.confirm 配置弹窗信息   "确认当前操作"
 *

 *2 在 function 内调用ajax 请求删除员工
 */
staffManage.del = function (row) {
    /**
     * 1.一般情况下删除要加confirm
     */
    Modal.confirm({
        msg: "确认当前操作"  // 配置 确认窗口 ，也必须在html 设置窗口布局
    }).on(function (e) {
        if (e) {// true 代表确认删除
            // 在弹框确认

            /**
             * 2 调用ajax 请求删除员工
             */
            $.ajax({
                url: "/DeleteStaffServlet",// 自己完成后台
                type: 'post',
                data: {
                    "staffId": row.staffId,
                },
                dataType: 'json',
                success: function (result) {
                    if (result.status > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                }
            })
        }
    })
}

/**
 * 更新员工信息
 */
function updateStaff(row) {

    // 1.获取模态框的数据
    //2.将数据提交到后台

    $("#myModalLabel").text("修改员工");
    $("#staffId").val(row.staffId);
    $("#staffName").val(row.staffName);
    $("#phone").val(row.phone);
    $("#idCard").val(row.idCard);
    $("#momo").val(row.momo);
    $("#address").val(row.address);
    $("#roleId").val(row.roleId);
    $("#status").val(row.status);
    urlUAA= "/UpdateStaffServlet";
    // 调用显示模态框
    $("#myModal").modal('show');

}
function confirm(){
    // 当点击提交校验输入框
    var bootstrapValidator = $("#staffForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        alert("校验成功");
        $.ajax({
            url:urlUAA,
            type:'post',
            data:$("#staffForm").serialize(),
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("操作成功");
                    $("#myModal").modal('hide');
                    $("#staffList").bootstrapTable('refresh');
                } else {
                    toastr['error']("操作失败");
                }
            }
        })
    }else {
        alert("校验失败");
    }
}

/**
 * 关闭模态框
 */
$("#myModal").on('hide.bs.modal', function () {
    //移除上次的校验配置
    $("#staffForm").data('bootstrapValidator').resetForm();
    $("#staffForm")[0].reset();
})

/*
* 初始化表单验证
* */
$("#staffForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        staffName: {
            validators: {
                notEmpty: {
                    message: "员工姓名不能为空"
                }
            }
        },
        phone: {
            validators: {
                notEmpty: {
                    message: "手机号不能为空"
                }
            }
        },
        idCard: {
            validators: {
                notEmpty: {
                    message: "员工身份证不能为空"
                }
            }
        },
        address: {
            validators: {
                notEmpty: {
                    message: "员工地址不能为空"
                }
            }
        },
        status: {
            validators: {
                notEmpty: {
                    message: "员工状态不能为空"
                }
            }
        },
        momo: {
            validators: {
                notEmpty: {
                    message: "员工备注不能为空"
                }
            }
        },
        roleId: {
            validators: {
                notEmpty: {
                    message: "员工角色不能为空"
                }
            }
        }
    }
});
function addStaff() {
    $("#myModalLabel").text("添加员工");
    // 调用显示模态框
    $("#myModal").modal('show');
    $("#staffForm")[0].reset();
    urlUAA= "/AddStaffServlet";
}