var staffListUrl = "/GetAllOrderServlet";
var initRoleUrl = "";
var updateOrderUrl="/UpdateOrderServlet";
var verifyOrderUrl="/UpdateVerifyOrderServlet"

/**
 * jquery  js 初始化得使用调用 $(function(){
 *
 * })
 */
$(function () {
    // 初始化 分页列表
    staffManage.initList();

    // 初始化员工角色下拉
    roleManager.initList();


})
var staffManage = {};

var roleManager = {};

/**
 * 在js 加载时初始化  员工角色表
 */
roleManager.initList = function () {

    // 怎么初始化？
    // ajax 请求 角色表 并填充角色 下拉option

    $.ajax({
        url: initRoleUrl, //这个servlet 自己实现
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status == 1) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].roleName + "</option>");
                    $("#roleId").append(opt);
                }
            }
        }
    })

}

// 初始化   staffList table
/**
 * 1.给他请求路径，并设置请求方式  返回的东西：1.data 数据bean列表（一个员工的集合）
 * 2.给设置请求参数
 *  {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchStaffId": $("#searchStaffId").val(),  // 单个搜索 的id
        "searchName": $("#searchName").val()       // 单个搜索员工的的名字
    }
 3. 设置行点击事件  可以不设置  row 就是  每一行数据 对象
 onClickRow: function (e, row, element) {  // 设置行点击事件***************************
             alert('监控行点击事件：'+row.id)
        },
 4. 设置网络请求到的数据
 result.data.count, //总条数
 result.data.list    List<Staff>   员工集合
 responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result != null) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        }
 5.配置列表显示
 //列表显示
 columns: [{   // 配置显示的列表 ****************************
                field: 'id',
                title: "商品编号",
                visible: false
            }, {
                field: 'staffId',
                title: "员工编号"
            }, {
                field: 'staffName',
                title: "员工姓名"
            },
 // 其中
 events: buttonOperateEvent, // 设置每一行按钮的 点击事件
 */


//将一个方法function  赋值给  staffManage.initList
/**
 * bootstrapTable 自带ajax 网络请求功能 数据处理  表单显示功能
 * 1. url: staffListUrl  发起网络请求  需要数据 1.总共有多少条数据  2.当前页得数据 集合
 * 2.  queryParams: staffManage.queryParams   设置请求的参数（ 1.当前页码  2.页码的大小）
 */
staffManage.initList = function () {
    $("#staffList").bootstrapTable({
        url: staffListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: staffManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function (row, e, element) {  // 设置行点击事件***************************
            //alert('监控行点击事件：' + row)
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log('初始化订单表返回的结果',result);
            if (result.status == 1) {
                return {
                    'total': result.count, //总条数*************************************
                    'rows': result.data //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "编号",
            visible: false
        }, {
            field: 'orderId',
            title: "订单编号"
        }, {
            field: 'cardId',
            title: "会员卡号"
        }, {
            field: 'cardType',
            title: "会员等级"
        }, {
            field: 'price',
            title: "应付金额"
        }, {
            field: 'pay',
            title: "实付金额"
        }, {
            field: 'credit',
            title: "商品积分"
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value) {
                switch (value) {
                    case 0 :
                        return "未核验";
                    case 1 :
                        return "已核验";
                }
            }
        }, {
            field: 'momo',
            title: "备注"
        }, {
            field: 'createdTime',
            title: "创建时间"
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return staffManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
staffManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "orderId": $("#searchOrderId").val(),
        "cardId": $("#searchCardId").val()
    }
}

/**
 * 按钮的点击事件
 *
 */
window.buttonOperateEvent = {
    // 更新按钮的updateOrder
    'click .updateOrder': function (e, value, row, index) {
        //row 这一行的数据
        //alert(row.staffName);
        // 初始化更新的模态框
        //1.初始模态框内的数据
        console.log('更新订单获取点击行数据',row);

        $("#orderId").val(row.orderId);
        $("#cardId").val(row.cardId);
        $("#cardType").val(row.cardType);
        $("#price").val(row.price);
        $("#pay").val(row.pay);
        $("#credit").val(row.credit);
        $("#status").val(row.status);
        $("#momo").val(row.momo);
        //
        //2.显示  在模态框的确定按钮添加 事件  使用ajax 将用户修改的数据 提交到后台
        $("#myModal").modal('show');


    },
    //更新按钮
    'click .verifyOrder': function (e, value, row, index) {
        staffManage.verifyOrder(row);
    }
}
/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
staffManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateOrder">修改</button>');
    returnButton.push('<button class="btn btn-danger verifyOrder">审核</button>');
    return returnButton.join('');
}

/**
 * 刷新列表
 */
staffManage.search = function () {
    //bootstrapTable 刷新
    $("#staffList").bootstrapTable('refresh');
}

staffManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days / 365);
}

/**
 * 删除员工
 *1.需要 确认弹框
 *        a.在html 配置弹框布局 直接cp
 *        b. 调用Modal.confirm 配置弹窗信息   "确认当前操作"
 *

 *2 在 function内调用ajax 请求审核订单
 */
staffManage.verifyOrder = function (row) {
    /**
     * 1.confirm确认
     */
    Modal.confirm({
        msg: "确认当前操作"  // 配置确认窗口 ，也必须在html 设置窗口布局
    }).on(function (e) {
        if (e) {// true 代表确认

            /**
             * 2 调用ajax 请求
             */
            //打印行数据
            console.log(row);
            $.ajax({
                url: verifyOrderUrl,
                type: 'post',
                data: {
                    "status": row.status,
                    "orderId":row.orderId
                },
                dataType: 'json',
                success: function (result) {
                    console.log('审核订单返回的结果',result);
                    if (result.status > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                }
            })

        }
    })
}

/**
 * 更新订单信息
 */
function updateOrder() {

    // 1.获取模态框的数据
    //2.将数据提交到后台

    $.ajax({
        url: updateOrderUrl,// 自己完成后台
        type: 'post',
        data: {
            "orderId": $("#orderId").val(),
            "cardId": $("#cardId").val(),
            "cardType": $("#cardType").val(),
            "price": $("#price").val(),
            "pay": $("#pay").val(),
            "credit": $("#credit").val(),
            "status": $("#status").val(),
            "momo": $("#momo").val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                toastr['success']("操作成功");
                $("#myModal").modal('hide');
            } else {
                toastr['error']("操作失败");
            }
        }
    })

}