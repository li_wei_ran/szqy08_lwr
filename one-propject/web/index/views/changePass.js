
var staffManage = {}

$(function () {

    //初始化员工信息
    staffManage.init();

});

//通过员工ID得到员工的个人信息
staffManage.init = function () {
    $.ajax({
        url: '/GetStaffInformationServlet',
        type: 'post',
        data: {
            //发给后台
            //浏览器缓存得到员工ID
            "staffId": window.localStorage.getItem("staffId"),//1.localStorage长期存在  window.localStorage.("staffId") 2.sessionStorage 临时 关闭就清空
        },
        dataType: 'json',
        success:function (result) {
            if (result.status > 0) {
                var r = result.data;
                console.log(r.createdTime)
                //将后台从数据库获取的一条记录赋值
                $("#staffId").val(r.staffId);
                $("#staffName").val(r.staffName);
                $("#phone").val(r.phone);
                $("#idCard").val(r.idCard);
                $("#address").val(r.address);
                $("#createdTime").val(r.createdTime);
            }
        }
    })
}

function change() {
    // 调用显示模态框
    $("#changePassword").modal('show');

}

function updatePassword() {
    if ($("#newPassword").val() == $("#confirmPassword").val() && $("#newPassword").val().length >=6) {

        $.ajax({
            url:'/UpdateStaffPasswordServlet',
            type:'post',
            data: {
                //将对应员工ID的密码通过ajax提交到后台 UpdateStaffPasswordServlet
                "staffId":window.localStorage.getItem("staffId"),
                "newPassword":$("#newPassword").val()
            },
            dataType:'json',
            success:function (result) {
                if (result.status > 0) {
                    toastr['success']("修改成功");
                    $("#changePassword").modal('hide');
                } else {
                    toastr['error']("修改失败");
                }
            }

        });
    }else {
        alert("两次输入的密码不一致");
    }

}