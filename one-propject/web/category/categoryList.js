var categoryListUrl = "/GetAllCategoryServlet";
var changeStatusUrl = "/ChangeCategoryStatusServlet";
var updateCategoryUrl = "/UpdateCategoryServlet";
var AddCategoryUrl = "/AddCategoryServlet";

$(function () {
    // 初始化 商品类型分页列表
    categoryManage.initList();
    //初始化验证
    formValidator();
})

var categoryManage = {};

function addCategory() {
    //隐藏修改按钮
    if ($("#addCategory").hide()){
        $("#addCategory").show();
        $("#updateCategory").hide();
    }else {
        $("#updateCategory").hide();
    }
    //清空模态框记录
    $("#categoryName").val(null);
    $("#status").find("option:first").prop("selected",true);
    $("#momo").val(null);

    $("#myModal").modal('show');


}
/**
 * 添加商品类别
 */
function addCategoryRealize() {
    // var bootstrapValidator = $("#staffForm").data('bootstrapValidator');
    // bootstrapValidator.validate();
    // if (bootstrapValidator.isValid()) {
        $.ajax({
            url: AddCategoryUrl,// 自己完成后台
            type: 'post',
            data: {
                "categoryName": $("#categoryName").val(),
                "status":$("#status").val(),
                "momo":$("#momo").val()
            },
            dataType: 'json',
            success: function (result) {
                if (result.status > 0) {
                    toastr['success']("操作成功");
                    $("#myModal").modal('hide');
                    categoryManage.search();
                } else {
                    toastr['error']("操作失败");
                }
            }
        })
    // }
}

categoryManage.initList = function () {
    $("#categoryList").bootstrapTable({
        url: categoryListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: categoryManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [5,10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function ( e,row, element) {  // 设置行点击事件***************************
            $(".success").removeClass("success");
            $(row).addClass("success");
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result.status==1) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "类别编号"
        }, {
            field: 'name',
            title: "类别名称"
        }, {
            field: 'momo',
            title: "备注"
        }, {
            field: 'status',
            title: "类别状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return '<button class="btn-xs btn-info" border="0">启用</button>';
                    case 0 :
                        return '<button class="btn-xs btn-danger" border="0">禁用</button>';
                }
            }
        }, {
            field: 'operation',//--------------------
            events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                return categoryManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
categoryManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchCategoryId": $("#searchCategoryId").val(),
        "searchCategoryName": $("#searchCategoryName").val()
    }
}

/**
 * 搜索
 */
categoryManage.search = function () {
    //bootstrapTable 刷新
    $("#categoryList").bootstrapTable('refresh');
}

/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
categoryManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateCategory">修改</button>');
    returnButton.push('<button class="btn btn-danger changeStatus">启用/禁用</button>');
    return returnButton.join('');
}

/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 商品 对应的 更新按钮的updateStaff
    'click .updateCategory': function (e, value, row, index) {
        //row 这一行的数据
        // 初始化更新的模态框
        //1.初始模态框内的数据
        if ($("#updateCategory").hide()){
            $("#updateCategory").show();
            $("#addCategory").hide();
        }else {
            $("#addCategory").hide();
        }
        $("#categoryId").val(row.id);
        $("#categoryName").val(row.name);
        $("#status").val(row.status);
        $("#momo").val(row.momo);
        //2.显示  在模态框的确定按钮添加 事件  使用ajax 将用户修改的数据 提交到后台
        $("#myModal").modal('show');


    },
    // 改变商品状态
    'click .changeStatus': function (e, value, row, index) {
        categoryManage.change(row);
    }
}

/**
 * 改变商品状态
 * @param row
 */
categoryManage.change = function (row) {
    Modal.confirm({
        msg: "是否改变商品类别状态?"  // 配置 确认窗口 ，也必须在html 设置窗口布局
    }).on(function (e) {
        if (e) {
            $.ajax({
                url: changeStatusUrl,
                type: 'post',
                data: {
                    "id":row.id,
                    "status": row.status
                },
                dataType: 'json',
                success: function (result) {
                    if (result.status > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                    categoryManage.search();
                },error:function (result) {
                    toastr['error']("切换失败");
                }
            })

        }
    })
}

/**
 * 修改商品类别信息
 */
function updateCategory() {
    $.ajax({
        url: updateCategoryUrl,// 自己完成后台
        type: 'post',
        data: {
            "categoryId": $("#categoryId").val(),
            "categoryName": $("#categoryName").val(),
            "status":$("#status").val(),
            "momo":$("#momo").val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status>0) {
                toastr['success']("操作成功");
                $("#myModal").modal('hide');
                categoryManage.search();
            } else {
                toastr['error']("操作失败");
            }
        }
    })
}



$("#myModal").on('hide.bs.modal', function () {
    //移除上次的校验配置
    $("#categoryForm").data('bootstrapValidator').destroy();
    $('#categoryForm').data('bootstrapValidator', null);
    formValidator();
})

/**
 * 表单提交验证
 * */
function aaa (){
    var bootstrapValidator = $("#categoryForm").data('bootstrapValidator');
    bootstrapValidator.validate();
}
function formValidator() {
    $("#categoryForm").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            categoryName: {
                validators: {
                    notEmpty: {
                        message: "类别名称不能为空"
                    }
                }
            },
            status: {
                validators: {
                    notEmpty: {
                        message: "货架不能为空"
                    }
                }
            },
            momo: {
                validators: {
                    notEmpty: {
                        message: "备注不能为空"
                    }
                }
            }
        }
    })
};
