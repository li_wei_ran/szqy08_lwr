
var getAllCardTypeInfoUrl = "/GetAllCardTypeServlet";
var addUserUrl = "/AddUserServlet";
var getLastCardIdUrl = "/GetCardIdServlet";

/**
 * 初始化
**/
$(function () {

    var data = {
        "请选择城市" :[],
        "河南省": ["郑州市","开封市","三门峡市","洛阳市","焦作市","新乡市","鹤壁市","安阳市","濮阳市","商丘市","许昌市","漯河市","平顶山市","南阳市","信阳市","周口市","驻马店市","济源市"],
        "湖北省": ["武汉市","十堰市","襄樊市","荆门市","孝感市","黄冈市","鄂州市","黄石市","咸宁市","荆州市","宜昌市","随州市","恩施州"],
        "湖南省": ["长沙市","张家界市","常德市","益阳市","岳阳市","株洲市","湘潭市","衡阳市","郴州市","永州市","邵阳市","怀化市","娄底市","湘西州"],
        "广东省": ["广州市","深圳市","清远市","韶关市","河源市","梅州市","潮州市","汕头市","揭阳市","汕尾市","惠州市","东莞市","珠海市","中山市","江门市","佛山市","肇庆市","云浮市","阳江市","茂名市","湛江市"],
    };
    //2.将json对象遍历，取出所有的key值存在集合obj中
    var obj = Object.keys(data);
    //遍历集合访问所有的key值
    for (var i = 0; i < obj.length; i++) {
        //将取出的key值放入select中
        var opt = $("<option>" + obj[i] + "</option>");
        $("#province").append(opt);
    }
    //为第一个多选框添加单击事件
    $("#province").change(function () {
        //在改变之前将第二个select中的值清空
        $("#city").html("");
        //获取省级下拉列表中选中的键值放入value中
        var value = data[$("#province").val()];
        //将获取到的value值遍历
        for (var i = 0; i < value.length; i++) {
            //创建option标签，将取到的value值放在option中
            var opt = $("<option>" + value[i] + "</option>");
            //将创建的option值放入第二个select中
            $("#city").append(opt);
        }
    });
    var userId = window.localStorage.getItem("staffId");
    var userName = window.localStorage.getItem("loginName");
    $("#staffId").val(userId);
    $("#staffName").val(userName);
    userManage.initCardType();
    userManage.changedLevel();
    getLastCardId();
});
function getLastCardId (){
    $.ajax({
        url:getLastCardIdUrl,
        type:'post',
        dataType:'json',
        success:function(result){
            $("#cardId").val(result.data);
        }
    })
}


var userManage = {};

/**
 * 加载会员类型
**/
userManage.initCardType = function () {
    $.ajax({
        url: getAllCardTypeInfoUrl,
        type: 'post',
        dataType: 'json',
        success: function (result) {
            for (var i = 0; i < result.data.length; i++) {
                var opt = $("<option data-rank='" + result.data[i].rank + "' value='" + result.data[i].id + "'>" + result.data[i].name + "</option>");
                $("#userLevel").append(opt);
            }
        }
    })
}
/**
 * 会员等级积分
**/
var amount = 0;
userManage.changedLevel = function () {
    $("#credit").val(0);
    $("#amount").val(0);
    $("#userLevel").change(function () {
        $("#credit").val($("#userLevel :selected").data("rank"));
        $("#amount").val($("#userLevel :selected").data("rank"));
        amount = $("#userLevel :selected").data("rank");
    });
};

function bbb() {
    window.location.reload()
}
var staffId = window.localStorage.getItem("staffId");
function aaa (){
    // 当点击提交校验输入框
    var bootstrapValidator = $("#addUserForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        alert("校验成功");
        $.ajax({
            url: addUserUrl,
            type: 'post',
            data: {
                //"userId":$("#userId").val(),
                "userName": $("#userName").val(),
                "userPhone": $("#userPhone").val(),
                "userLevel": $("#userLevel").val(),
                "userStatus": $("#userStatus").val(),
                "staffId": staffId,
                "credit": $("#credit").val(),
                "cardId": $("#cardId").val(),
                "amount": $("#amount").val(),
                "shenfen": $("#shenfen").val(),
                "userSex": $("#userSex").val(),
                "address": $("#address").val(),
                "area": $("#province").val() + $("#city").val(),
                "momo": $("#momo").val()
            },
            dataType: 'json',
            success: function (result) {
                toastr['success']("操作成功");
                $("#addUserForm").reset();
            }
        });
    }else {
        alert("校验失败");
    }
}

/**
 * 表单验证
 */
$("#addUserForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'giyphicon glyphicon-ok',
        invalid: 'glyphicon  glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        userName: {
            validators: {
                notEmpty: {
                    message: "会员姓名不能为空"

                }
            }
        },
        userPhone: {
            validators: {
                notEmpty: {
                    message: "会员电话不能为空"
                },
                regexp: {
                    regexp: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9])\d{8}$/,
                    message: "请输入正确的手机号"
                }
            }
        },
        userLevel: {
            validators: {
                notEmpty: {
                    message: "会员姓名不能为空"
                }
            }
        },
        userStatus: {
            validators: {
                notEmpty: {
                    message: "状态不能为空"
                }
            }
        },
        staffName: {
            validators: {
                notEmpty: {
                    message: "办卡人员不能为空"
                }
            }
        },
        credit: {
            validators: {
                notEmpty: {
                    message: "初始积分不能为空"
                }
            }
        },
        amount: {
            validators: {
                notEmpty: {
                    message: "起充金额不能为空"
                }
            }
        },
        shenfen: {
            validators: {
                notEmpty: {
                    message: "身份证号不能为空"
                },
                regexp: {
                    regexp: /^\d{17}[0-9|x]$/,
                    message: "请输入正确的身份证号"
                }
            }
        },
        userSex: {
            validators: {
                notEmpty: {
                    message: "性别不能为空"
                }
            }
        },
        address: {
            validators: {
                notEmpty: {
                    message: "地址不能为空"
                }
            }
        }

    }
});
