package com.aaa.controller;

import com.aaa.entity.Category;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CategoryService;
import com.aaa.service.CategoryServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/ChangeCategoryStatusServlet")
public class ChangeCategoryStatusServlet extends HttpServlet {
    CategoryService categoryService = null;

    @Override
    public void init() throws ServletException {
        categoryService = new CategoryServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        int status = Integer.parseInt(req.getParameter("status"));

        Category category = new Category();
        ResponseDto responseDto = new ResponseDto();
        if (status==1){
            category.setStatus(0);
        }else{
            category.setStatus(1);
        }
        category.setId(Integer.valueOf(id));

        try {
            boolean changeStatus = categoryService.ChangeStatus(category);

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("修改状态成功");
            responseDto.setData(changeStatus);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("修改状态失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
