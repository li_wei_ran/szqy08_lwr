package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 修改权限
 */
@WebServlet("/UpdateResourceServlet")
public class UpdateResourceServlet extends HttpServlet {

    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        roleService = new RoleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();

        try {
            //将对应员工id的权限获取
            String id = req.getParameter("roleId");
            String nodeList = req.getParameter("nodeList"); //"[6,19,7,22]"
            //str＝str.substring(int beginIndex，int endIndex);截取str中从beginIndex开始至endIndex结束时的字符串，并将其赋值给str;
            String nodeList1 = nodeList.substring(1,nodeList.length()-1);

            //String.replace("-", "")方法：第一个参数是你要替换的字符，第二个参数是用于取代旧字符串的新字符
            String[] strings = nodeList1.split(",");//["[6","19","22]"]
            //1.删除resource_role中的
            roleService.deleteRole1(Integer.valueOf(id));

            //遍历将数组里的字符串转换成resource_role中的resource_id
            for (int i=0;i<strings.length;i++){
                //将数组里的每一个元素转换为Integer类型
                Integer resource_id = IntegerUtils.ToInteger(strings[i]);
                //2.在resource_role中新增role_id和resource_id对应的
                roleService.addRole1(Integer.valueOf(id),resource_id);
            }

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("增加成功");

        }catch (Exception e ){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("增加失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
