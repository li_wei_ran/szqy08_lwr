package com.aaa.controller;

import com.aaa.entity.Goods;
import com.aaa.service.GoodsService;
import com.aaa.service.GoodsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ChangeStatusServlet")
public class ChangeStatusServlet extends HttpServlet {
    GoodsService goodsService = null;
    @Override
    public void init() throws ServletException {
        goodsService = new GoodsServiceImpl();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        int status = Integer.parseInt(req.getParameter("status"));

        Goods goods = new Goods();
        if (status==1){
            goods.setStatus(2);
        }else{
            goods.setStatus(1);
        }
        goods.setId(Integer.valueOf(id));

        goodsService.ChangeStatus(goods);

        resp.getWriter().print(new Gson().toJson(goods));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
