package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.User;
import com.aaa.service.UserService;
import com.aaa.service.impl.UserServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {
    private UserService userService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        Integer status = IntegerUtils.ToInteger(req.getParameter("status"));
        String phone = req.getParameter("phone");
        String address = req.getParameter("address");
        Integer userId = IntegerUtils.ToInteger(req.getParameter("userId"));
        String idCard = req.getParameter("idCard");

        User user = new User();
        user.setUserName(userName);
        user.setStatus(status);
        user.setAddress(address);
        user.setPhone(phone);
        user.setUserId(userId);
        user.setIdCard(idCard);

        int len = userService.updateUser(user);
        if (len > 0) {
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        } else {
            responseDto.setMessage("请求失败");
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }
}
