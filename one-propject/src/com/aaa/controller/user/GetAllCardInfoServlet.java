package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetAllCardInfoServlet")
public class GetAllCardInfoServlet extends HttpServlet {
    private CardService cardService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer pageNumber = IntegerUtils.ToInteger(req.getParameter("pageNumber"));
        Integer pageSize = IntegerUtils.ToInteger(req.getParameter("pageSize"));
        String searchId = req.getParameter("searchId");
        String searchName = req.getParameter("searchName");
        try {
            //返回参数
            ResponseDto responseDto = new ResponseDto();
            responseDto.setData(cardService.getAllCard(pageNumber, pageSize, searchId, searchName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
