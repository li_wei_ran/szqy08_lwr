package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/DeleCardlUrlServlet")
public class DeleCardlUrlServlet extends HttpServlet {

    private CardService cardService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer cardId = IntegerUtils.ToInteger(req.getParameter("cardId"));
        Integer status = IntegerUtils.ToInteger(req.getParameter("status"));

        int len = cardService.BanOrAllowedCardByCardId(cardId, status);
        if (len > 0) {
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        } else {
            responseDto.setMessage("请求失败");
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }

}
