package com.aaa.controller.user;

import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/GetCardIdServlet")
public class GetCardIdServlet extends HttpServlet {
    private CardService cardService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String lastCardId = cardService.getLastCardId();
        System.out.println("你好");
        if (lastCardId != null) {
            Integer cardId = IntegerUtils.ToInteger(lastCardId) + 1;
            responseDto.setData(cardId);
            responseDto.setMessage("获取成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        } else {
            responseDto.setMessage("获取失败");
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
        }
        resp.getWriter().print(new Gson().toJson(responseDto));

    }
}
