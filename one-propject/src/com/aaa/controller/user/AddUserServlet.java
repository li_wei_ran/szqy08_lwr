package com.aaa.controller.user;

import com.aaa.entity.Card;
import com.aaa.entity.ResponseDto;
import com.aaa.entity.User;
import com.aaa.service.CardService;
import com.aaa.service.UserService;
import com.aaa.service.impl.CardServiceImpl;
import com.aaa.service.impl.UserServiceImpl;
import com.aaa.util.DateUtils;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {
    private UserService userService;
    private ResponseDto responseDto;
    private CardService cardService;

    @Override
    public void init() throws ServletException {
        userService = new UserServiceImpl();
        responseDto = new ResponseDto();
        cardService = new CardServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer userId = IntegerUtils.ToInteger(userService.getLastUserId()) + 1;
        Integer cardId = IntegerUtils.ToInteger(req.getParameter("cardId"));
        String userName = req.getParameter("userName");
        String phone = req.getParameter("userPhone");
        Integer status = IntegerUtils.ToInteger(req.getParameter("userStatus"));
        Integer staffId = IntegerUtils.ToInteger(req.getParameter("staffId"));
        String createdTime = DateUtils.toFormat(new Date());
        String momo = req.getParameter("momo");
        String address = req.getParameter("address");
        String userSex = req.getParameter("userSex");
        Integer sex = IntegerUtils.ToInteger(userSex);
        String area = req.getParameter("area");
        /*String amount = req.getParameter("amount");*/
        double amount = Double.parseDouble(req.getParameter("amount"));
        /*String credit = req.getParameter("credit");*/
        Integer credit = IntegerUtils.ToInteger(req.getParameter("credit"));
        String idCard = req.getParameter("shenfen");
        Integer level = IntegerUtils.ToInteger(req.getParameter("userLevel"));

        Card card = new Card();
        card.setCardId(cardId);
        card.setUserId(userId);
        card.setAmount(amount);
        card.setCredit(credit);
        card.setStatus(status);
        card.setStaffId(staffId);
        card.setLevelId(level);
        System.out.println(card.toString());

        int len2 = cardService.addCard(card);

        User user = new User();

        user.setUserId(userId);

        user.setCardId(cardId);
        user.setUserName(userName);
        user.setStatus(status);
        user.setPhone(phone);
        user.setIdCard(idCard);
        user.setSex(sex);
        user.setAddress(address);
        user.setCreatedTime(createdTime);
        user.setMomo(momo);
        user.setArea(area);
        user.setStaffId(staffId);
        System.out.println(user.toString());

        int len1 = userService.addUser(user);

        if (len1 > 0 && len2 > 0) {
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        } else {
            responseDto.setMessage("请求失败");
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }
}
