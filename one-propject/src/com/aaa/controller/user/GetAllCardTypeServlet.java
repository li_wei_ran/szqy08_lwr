package com.aaa.controller.user;

import com.aaa.entity.CardType;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CardTypeService;
import com.aaa.service.impl.CardTypeServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/GetAllCardTypeServlet")
public class GetAllCardTypeServlet extends HttpServlet {
    private CardTypeService cardTypeService;
    private ResponseDto responseDto;

    @Override
    public void init() throws ServletException {
        cardTypeService = new CardTypeServiceImpl();
        responseDto = new ResponseDto();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<CardType> allCardTypeInfo = cardTypeService.getAllCardTypeInfo();
        for (CardType cardType : allCardTypeInfo) {
            System.out.println(cardType);
        }
        if (allCardTypeInfo != null) {
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("请求成功");
            responseDto.setData(allCardTypeInfo);
        } else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("请求失败");
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }
}
