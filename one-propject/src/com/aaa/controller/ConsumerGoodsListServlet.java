package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.service.GoodsService;
import com.aaa.service.GoodsServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ConsumerGoodsListServlet")
public class ConsumerGoodsListServlet extends HttpServlet {
    private GoodsService goodsService;
    @Override
    public void init() throws ServletException {
        goodsService = new GoodsServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        ResponseDto responseDto = new ResponseDto();
        try {
            //返回参数
            Integer pageNumber = IntegerUtils.ToInteger(request.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(request.getParameter("pageSize"));
            String searchGoodsId = request.getParameter("searchGoodsId");
            String searchName = request.getParameter("searchName");
            responseDto.setData(goodsService.consumerGoodList(pageNumber, pageSize, searchGoodsId, searchName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");
        }
        response.getWriter().print(new Gson().toJson(responseDto));
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
