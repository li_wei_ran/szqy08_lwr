package com.aaa.controller.news;

import com.aaa.entity.News;
import com.aaa.entity.ResponseDto;
import com.aaa.service.NewsService;
import com.aaa.service.impl.NewsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddNewsServlet")
public class AddNewsServlet extends HttpServlet {
    private NewsService NewsService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = req.getParameter("content");
        String staffId = req.getParameter("staffId");
        String createdTime = req.getParameter("createdTime");
        String endTime = req.getParameter("endTime");
        String status = req.getParameter("status");
        String title = req.getParameter("title");

        News news = new News();
        news.setContent(content);
        news.setStaffId(Integer.parseInt(staffId));
        news.setTitle(title);
        news.setCreatedTime(createdTime);
        news.setEndTime(endTime);
        news.setStatus(Integer.parseInt(status));

        int num = NewsService.addNews(news);
        ResponseDto responseDto = new ResponseDto();
        if (num > 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("添加成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("添加失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    public void init() throws ServletException {
        NewsService = new NewsServiceImpl();
    }
}
