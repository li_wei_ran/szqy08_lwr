package com.aaa.service;

import com.aaa.entity.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * 分页查询所有会员信息
     *
     * @param pageNumber
     * @param pageSize
     * @param searchName
     * @return
     */
    Map<String, Object> getAllUserInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception;

    /**
     * 查询最后一个用户的Id
     */
    String getLastUserId();

    /**
     * 添加用户
     */
    int addUser(User user);
    int deleteCardByUserId(int userId);//删除
    /**
     * 修改会员
     * @param user
     * @return
     */
    int updateUser(User user);

    Map<String,Object> getCardInfoById(Integer cardId)throws Exception;
    /**
     * 通过会员卡号查询会员的余额等信息
     */
    List<String> findUserByCardId(Integer cardId);

}
