package com.aaa.service;

import com.aaa.entity.Staff;

import java.util.List;
import java.util.Map;

public interface StaffService {


    public Staff login(String staffId, String password);

    int updateStaffByStaffId( Staff staff);

    int addStaffByStaffId(Staff staff);

/**
     * 分页查询所有学生的信息
     * @param pageNumber
     * @param pageSize
     * @return
     */
    int deleteStaffByStaffId(int staffId);//删 int deleteStaffByStaffId(int staffId);//删
    List<Staff> findAllStaff(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    //通过staffID获取数据库里的一条记录（staffID对应的所有字段）
    Staff getStaffByStaffId(int staffId);

    //修改员工密码
    int updateStaffPassword(int staffId,String password);

    List<Staff> findStaffByStaffId(int staffId);//id查
    List<Staff> findStaffByStaffName(String staffName);//名字查

    List<Map<String,Object>> getRole();

    List<Staff> getMaxStaffId();
    /**
     * 分页查询所有学生的信息
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Map<String,Object> getAllStaffInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName)throws Exception;


}
