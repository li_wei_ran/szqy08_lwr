package com.aaa.service;

import com.aaa.dao.CategoryDao;
import com.aaa.dao.CategoryDaoImpl;
import com.aaa.entity.Category;
import com.aaa.util.BusinessException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryServiceImpl implements CategoryService {
    private CategoryDao categoryDao = new CategoryDaoImpl();
    @Override
    public List<Category> GetCategoryList() {
        return categoryDao.getCategoryList();
    }

    @Override
    public Map<String, Object> getAllCategoryInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
        List<Category> list = categoryDao.getAllCategoryInfo(pageNumber, pageSize, searchStuno, searchName);

        // 总页数
        int count = categoryDao.getAllCategoryInfoCount(searchStuno, searchName);

        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }

    @Override
    public boolean UpdateCategory(Category category) {
        int num = categoryDao.updateCategory(category);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean ChangeStatus(Category category) {
        int num = categoryDao.changeStatus(category);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean AddCategory(Category category) {
        int num = categoryDao.addCategor(category);
        if (num>0){
            return true;
        }
        return false;
    }
}
