package com.aaa.service;

import com.aaa.dao.RoleDao;
import com.aaa.dao.RoleDaoImpl;
import com.aaa.entity.Role;
import com.aaa.entity.TreeMenu;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoleServiceImpl implements RoleService {

    RoleDao roleDao = new RoleDaoImpl();

    @Override
    public List<Role> getAllRole() {
        return roleDao.getAllRole();
    }

    @Override
    public Map<String, Object> getAllRoleInfo(int pageNumber, int pageSize, String searchName) throws BusinessException {

        if (pageNumber == 0){
            throw new BusinessException("当前页数不能为0");
        }

        if (pageSize == 0){
            throw  new BusinessException("每页条数不能为0");
        }

        Map<String,Object> map = new HashMap<>();

        pageNumber = (pageNumber -1)*pageSize;

        List<Role> roleList =  roleDao.getAllRoleInfo(pageNumber,pageSize,searchName);
        int count = roleDao.getAllRoleInfoCount(searchName);

        map.put("list",roleList);
        map.put("count",count);

        return map;
    }

    @Override
    public int updateOrAddRole(int id, String roleName, String description, int status) {

        int len = 0;

        if (id == 0 ){
            len =  roleDao.addRole(roleName,description,status);
        }else {
            len =  roleDao.updateRole(id,roleName,description,status);
        }

        return len;
    }

    @Override
    public List<TreeMenu> getMenuList(int roleId) {

        return roleDao.getMenuList(roleId);
    }

    @Override
    public int updateMenuList(int roleId, int[] resouceces) {

        return roleDao.updateMenuList(roleId,resouceces);
    }



    @Override
    public int deleteRole(Integer id) {
        return roleDao.deleteRole(id);
    }

    @Override
    public int deleteRole1(Integer id) {
        return roleDao.deleteRole1(id);
    }

    @Override
    public int addRole1(Integer id, Integer resource_id) {
        return roleDao.addRole1(id, resource_id);
    }


}
