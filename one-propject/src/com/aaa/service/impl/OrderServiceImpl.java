package com.aaa.service.impl;

import com.aaa.dao.OrderDao;
import com.aaa.dao.OrderDaoImpl;
import com.aaa.entity.Order;
import com.aaa.entity.OrderInfo;
import com.aaa.service.OrderService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderServiceImpl implements OrderService {
    private static OrderDao orderDao = new OrderDaoImpl();

    /**
     * 提交订单
     *
     * @param orderinfoArr
     * @param orderinfoObj
     * @return
     */
    @Override
    public int submitOrder(String orderinfoArr, String orderinfoObj) {
        //解析前端页面封装好的数据，准备操作数据库
        Gson gson = new Gson();
        //判断执行结果
        int orderAffectNum = 1;
        int orderinfoAffectNum = 1;
        int minusUserAmountAffectNum = 1;
        int affectNum = 0;

        //解析订单明细为Orderinfo实体类
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(orderinfoArr);
        for (int i = 0; i < jsonArray.size(); i++) {
            OrderInfo orderInfo = gson.fromJson(jsonArray.get(i), OrderInfo.class);
            if (orderDao.insertIntoOrderinfo(orderInfo) != 1) {
                orderAffectNum = 0;
            }
        }

        //解析订单为Order实体类,执行操作
        Order order = gson.fromJson(orderinfoObj, Order.class);
        System.out.println(order);
        if (orderDao.insertIntoOrder(order) != 1) {
            orderinfoAffectNum = 0;
        }

        //操作用户余额
        minusUserAmountAffectNum = orderDao.minusCardAmount(order.getCardId(), order.getPrice());

        //判断最终的结果
        if (orderAffectNum == 1 && orderinfoAffectNum == 1 && minusUserAmountAffectNum == 1) {
            affectNum = 1;
        }
        return affectNum;
    }


    @Override
    public int updateOrderByOrderId(Order order) {
        return orderDao.updateOrderByOrderId(order);
    }

    @Override
    public int updateVerifyOrderByOrderId(Integer orderId, Integer status) {
        return orderDao.updateVerifyOrderByOrderId(orderId, status);
    }

    @Override
    public List<Order> findAllOrder(Integer pageNumber, Integer pageSize, Integer orderId, Integer cardId) {
        List<Order> list = orderDao.findAllOrder(pageNumber, pageSize, orderId, cardId);
        //截取日期
        String dateTimeStr = "";
        for (int i = 0; i < list.size(); i++) {
            dateTimeStr = list.get(i).getCreatedTime().substring(0, 19);
            list.get(i).setCreatedTime(dateTimeStr);
        }
        return list;
    }

    @Override
    public List<Order> findOrderByOrderId(Integer orderId) {
        return orderDao.findOrderByOrderId(orderId);
    }

    @Override
    public List<Order> findOrderBySCardId(Integer cartId) {
        return orderDao.findOrderByCardId(cartId);
    }

    @Override
    public int getAllOrderInfoCount(Integer orderId, Integer cardId) {
        // 总页数
        return orderDao.getAllOrderInfoCount(orderId, cardId);
    }


}
