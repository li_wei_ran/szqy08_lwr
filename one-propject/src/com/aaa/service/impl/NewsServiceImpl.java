package com.aaa.service.impl;

import com.aaa.dao.NewsDao;
import com.aaa.dao.impl.NewsDaoImpl;
import com.aaa.entity.News;
import com.aaa.service.NewsService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsServiceImpl implements NewsService {

    private NewsDao newsDao = new NewsDaoImpl();

    @Override
    public int addNews(News news) {
        return newsDao.addNews(news);
    }

    @Override
    public int updateNewsById(News news) {
        return newsDao.updateNewsById(news);
    }

    @Override
    public int updateNewsStatusById(Integer id, Integer status) {
        if (status == 0){
            status = 1;
        }else {
            status = 0;
        }

        return newsDao.updateNewsStatusById(id,status);
    }

    @Override
    public Map<String, Object> findAllAndSearchNews(Integer pageNumber, Integer pageSize, String searchTitle, String searchstaffName, String createdTime, String endTime) {
        try {
            if (pageNumber == null || pageNumber == 0) {
                throw new BusinessException("当前页数不能为空");
            }
            if (pageSize == null || pageSize == 0) {
                throw new BusinessException("每页条数不能为空");
            }
            pageNumber = (pageNumber - 1) * pageSize;
        } catch (Exception e){
            e.printStackTrace();
        }
        List<News> list = newsDao.findAllAndSearchNews(pageNumber,pageSize,searchTitle,searchstaffName,createdTime,endTime);

        int count = findAllAndSearchNewsCount(searchTitle,searchstaffName,createdTime,endTime);
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);

        return map;
    }

    @Override
    public int findAllAndSearchNewsCount(String searchTitle, String searchstaffName, String createdTime, String endTime) {
        return newsDao.findAllAndSearchNewsCount(searchTitle,searchstaffName,createdTime,endTime);
    }
}
