package com.aaa.service.impl;

import com.aaa.dao.CardDao;
import com.aaa.dao.impl.CardDaoImpl;
import com.aaa.entity.Card;
import com.aaa.entity.RechargeRecord;
import com.aaa.service.CardService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardServiceImpl implements CardService {
    private CardDao cardDao = new CardDaoImpl();

    @Override
    public String getLastCardId() {
        return cardDao.getLastCardId();
    }

    @Override
    public int addCard(Card card) {
        return cardDao.addCard(card);
    }

    @Override
    public Map<String, Object> getAllCard(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0){
            throw new BusinessException("页数不能为空");
        }
        if (pageSize == null || pageSize == 0){
            throw new BusinessException("条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        List<Card> list = cardDao.getAllCard(pageNumber,pageSize,searchId,searchName);
        int count = cardDao.getAllCardInfoCount(searchId,searchName);
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return map;
    }

    @Override
    public int getAllCardInfoCount(String searchId, String searchName) throws Exception {
        return 0;
    }

    @Override
    public int BanOrAllowedCardByCardId(Integer cardId, Integer status) {
        return cardDao.BanOrAllowedCardByCardId(cardId, status);
    }

    @Override
    public Map<String, Object> getAllRechargeRecord(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0){
            throw new BusinessException("页数不能为空");
        }
        if (pageSize == null || pageSize == 0){
            throw new BusinessException("条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        List<RechargeRecord> list = cardDao.getAllRechargeRecord(pageNumber,pageSize,searchId,searchName);
        int count = cardDao.getAllRechargeRecordCount(searchId,searchName);
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return map;
    }

    @Override
    public int deleteCardByCardId(int cardId) {
        return cardDao.deleteCardByCardId(cardId);
    }

    @Override
    public List<Card> getCardById(Integer cardId) {
        return cardDao.getCardById(cardId);
    }

    @Override
    public int rechargeCard(Integer cardId, double amount, Integer rechargeRule) throws BusinessException {
        if (cardId == 0){
            throw new BusinessException("ID不能为空");
        }
        if (amount == 0){
            throw new BusinessException("余额不能为空");
        }
        return cardDao.rechargeCard(cardId,amount,rechargeRule);
    }
}
