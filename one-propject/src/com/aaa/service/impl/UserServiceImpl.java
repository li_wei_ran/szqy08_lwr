package com.aaa.service.impl;

import com.aaa.dao.UserDao;
import com.aaa.dao.impl.UserDaoImpl;
import com.aaa.entity.User;
import com.aaa.service.UserService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public Map<String, Object> getAllUserInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        List<User> list = userDao.getAllUserInfo(pageNumber, pageSize, searchId, searchName);
        int count = userDao.getAllUserInfoCount(searchId, searchName);
        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }

    @Override
    public String getLastUserId() {
        return userDao.getLastUserId();
    }

    @Override
    public int addUser(User user) {
        return userDao.addUser(user);
    }

    @Override
    public int deleteCardByUserId(int userId) {
        return userDao.deleteUserById(userId);
    }

    @Override
    public int updateUser(User user) {
        return userDao.updateUser(user);
    }

    @Override
    public Map<String, Object> getCardInfoById(Integer cardId) throws Exception {
        List<Map<String, Object>> list = userDao.getCardInfoById(cardId);
        if (list == null){
            Map<String,Object> maps = new HashMap<>(0);
            return  maps;
        }else {
            Map<String,Object> map = list.get(0);
            return map;
        }
    }

    @Override
    public List<String> findUserByCardId(Integer cardId) {
        return userDao.findUserByCardId(cardId);
    }

}
