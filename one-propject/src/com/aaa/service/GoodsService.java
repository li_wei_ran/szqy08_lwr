package com.aaa.service;

import com.aaa.entity.Goods;
import com.aaa.entity.Staff;
import com.aaa.util.BusinessException;

import java.util.List;
import java.util.Map;

public interface GoodsService {
    /**
     * 修改商品信息
     * @param goods
     * @return
     */
    boolean UpdateGoods(Goods goods);

    /**
     * 改变商品状态
     * @param goods
     * @return
     */
    boolean ChangeStatus(Goods goods);
    /**
     * 分页查询所有商品的信息
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Map<String,Object> getAllGoodsInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName)throws Exception;

    /**
     * 新增商品信息
     * @param goods
     * @return
     */
    boolean AddGoods(Goods goods);

    /**
     * 初始化商品编号
     * @return
     */
    List<Goods> InitGoodsId();
    int addGoods(Goods goods);

    Map<String,Object> getConsumerGoodList(Integer pageNumber, Integer pageSize, String searchId, String searchName)throws Exception;


    int consumerGoods(Integer goodsId, Integer code) throws  Exception;


    /**
     * 商品销售里的商品列表
     * @param pageNumber
     * @param pageSize
     * @param searchId
     * @param searchName
     * @return
     * @throws Exception
     */
    Map<String,Object> consumerGoodList(Integer pageNumber, Integer pageSize, String searchId, String searchName) throws BusinessException;
}
