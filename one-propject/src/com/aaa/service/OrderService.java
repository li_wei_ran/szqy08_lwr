package com.aaa.service;

import com.aaa.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {


    int  submitOrder(String orderinfoArr, String orderinfoObj);

    int updateOrderByOrderId(Order order);

    int  updateVerifyOrderByOrderId(Integer orderId, Integer status);

    List<Order> findAllOrder(Integer pageNumber, Integer pageSize, Integer orderId, Integer cardId);

    List<Order> findOrderByOrderId(Integer orderId);

    List<Order> findOrderBySCardId(Integer cardId);

    int getAllOrderInfoCount(Integer orderId, Integer cardId);


}
