package com.aaa.service;

/**
 * @Author: Ltl
 * @Date: 2019/12/3 0003 14:47
 */
public interface RechargeService {

    /**
     *
     */
    int rechargeRecode(Integer cardId, double recharge, Integer rechargeRule, Integer staffId, double amount);
}
