package com.aaa.filter;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@WebFilter("/*")
public class LogFilter implements Filter {
    //Logger最大的好处是异步线程，不阻塞当前的线程，System.out.println 线上不能使用
    Logger logger = Logger.getLogger(LogFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("LogFilter初始化");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        //获取uri
        String url = request.getRequestURI();//.getReuqestURI 有String类、和StringBuffer类 这里选择String
//        System.out.println("请求内容"+url);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(url+"?");
        //获取Map
        Map<String,String[]> map = request.getParameterMap();
        //遍历Map,获取所有请求参数的键值对
        for (String key:map.keySet()){
            String[] strings = map.get(key);
            for (String value:strings){
//                System.out.println("key:"+key+" value:"+value);
                stringBuffer.append(key+"="+value+"&");
            }
        }
        logger.info("请求内容:"+stringBuffer.toString());
        //不进行任何鉴权过滤，只获取参数，不用转换HttpServletRequest
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        logger.info("LogFilter调用destroy()");
    }
}
