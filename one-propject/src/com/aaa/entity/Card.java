package com.aaa.entity;

public class Card {
    /**
     * id
     */
    private Integer id;
    /**
     * cardid
     */
    private Integer cardId;
    /**
     * userid
     */
    private Integer userId;
    /**
     * idCard
     * 身份证号
     */
    private String idCard;
    /**
     * amount
     */
    private Integer staffId;
    private Double amount;
    /**
     * 卡积分
     */
    private Integer credit;
    /**
     * 状态(0:正常 1:挂失)
     */
    private Integer status;

    private Integer levelId;

    private String levelName;

    /**
     * momo
     */
    private  String  momo;
    private  String userName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLevelId() {
        return levelId;
    }

    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getMomo() {
        return momo;
    }

    public void setMomo(String momo) {
        this.momo = momo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }



    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", cardId=" + cardId +
                ", userId=" + userId +
                ", idCard='" + idCard + '\'' +
                ", staffId=" + staffId +
                ", amount=" + amount +
                ", credit=" + credit +
                ", status=" + status +
                ", levelId=" + levelId +
                ", levelName='" + levelName + '\'' +
                ", momo='" + momo + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
