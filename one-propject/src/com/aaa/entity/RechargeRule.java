package com.aaa.entity;

import com.aaa.util.DateUtils;

import java.util.Date;

public class RechargeRule {

    private int id;

    private String name;

    private double coefficient;

    private String createdTime;

    private String endTime;

    private int status;

    private int startMoney;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        //往这个类传入进来的必须是Date类型的数据，然后把它格式化转为String类型存储
        //从这个类的对象中获取的是String类型的数据 ，可以直接在返回的json中使用，不用强转格式
        this.createdTime = DateUtils.toFormat(createdTime);
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toFormat(endTime);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStartMoney() {
        return startMoney;
    }

    public void setStartMoney(int startMoney) {
        this.startMoney = startMoney;
    }
}
