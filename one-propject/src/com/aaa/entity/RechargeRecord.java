package com.aaa.entity;

import com.aaa.util.DateUtils;

import java.util.Date;

public class RechargeRecord {
    /**
     * id
     */
    private Integer id;

    /**
     * cardId
     */
    private Integer cardId;

    /**
     * 充值金额
     */
    private Double rechargeAmount;

    /**
     * 充值后卡余额
     */
    private Double afterAmount;

    /**
     * 充值前卡余额
     */
    private Double beforeAmount;

    /**
     * 充值规则
     */
    private Integer ruleId;

    /**
     * 充值时间
     */
    private String createdTime;

    /**
     * 操作员
     */
    private Integer staffId;

    /**
     * 备注
     */
    private String momo;

    private String userName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Double getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(Double rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public Double getAfterAmount() {
        return afterAmount;
    }

    public void setAfterAmount(Double afterAmount) {
        this.afterAmount = afterAmount;
    }

    public Double getBeforeAmount() {
        return beforeAmount;
    }

    public void setBeforeAmount(Double beforeAmount) {
        this.beforeAmount = beforeAmount;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = DateUtils.toFormat(createdTime);
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getMomo() {
        return momo;
    }

    public void setMomo(String momo) {
        this.momo = momo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "RechargeRecord{" +
                "id=" + id +
                ", cardId=" + cardId +
                ", rechargeAmount=" + rechargeAmount +
                ", afterAmount=" + afterAmount +
                ", beforeAmount=" + beforeAmount +
                ", ruleId=" + ruleId +
                ", createdTime=" + createdTime +
                ", staffId=" + staffId +
                ", momo='" + momo + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
