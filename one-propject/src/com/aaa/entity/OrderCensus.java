package com.aaa.entity;

public class OrderCensus {
    private int id;
    private int orderId;
    private  int cardId;
    private  int cardType;
    private  double price;
    private  double pay;
    private int credit;
    private int status;
    private String momo;
    private String  createdTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    public int getCreadit() {
        return credit;
    }

    public void setCreadit(int credit) {
        this.credit = credit;
    }

    public int getStatuse() {
        return status;
    }

    public void setStatuse(int status) {
        this.status = status;
    }

    public String getMomo() {
        return momo;
    }

    public void setMomo(String momo) {
        this.momo = momo;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public String toString() {
        return "OrderCensus{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", cardId=" + cardId +
                ", cardType=" + cardType +
                ", price=" + price +
                ", pay=" + pay +
                ", credit=" + credit +
                ", status=" + status +
                ", momo='" + momo + '\'' +
                ", createdTime='" + createdTime + '\'' +
                '}';
    }
}
