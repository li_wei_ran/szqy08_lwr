package com.aaa.entity;

import com.aaa.util.DateUtils;

import java.util.Date;

public class UserCensus {
    private Integer userId;
    private String  createdTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public String toString() {
        return "UserCensus{" +
                "userId=" + userId +
                ", createdTime='" + createdTime + '\'' +
                '}';
    }
}