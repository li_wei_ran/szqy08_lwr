package com.aaa.entity;

public class OrderInfo {
    /**
     * id
     */
    private Integer id;

    /**
     * 订单编号
     */
    private Integer orderId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品数量
     */
    private Integer goodsNumber;

    /**
     * 单个商品积分
     */
    private Integer goodsCredit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getGoodsCredit() {
        return goodsCredit;
    }

    public void setGoodsCredit(Integer goodsCredit) {
        this.goodsCredit = goodsCredit;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", goodsId=" + goodsId +
                ", goodsNumber=" + goodsNumber +
                ", goodsCredit=" + goodsCredit +
                '}';
    }
}
