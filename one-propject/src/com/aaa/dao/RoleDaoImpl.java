package com.aaa.dao;

import com.aaa.entity.Role;
import com.aaa.entity.TreeMenu;
import com.aaa.util.IntegerUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RoleDaoImpl implements RoleDao {

    BaseDao baseDao = BaseDao.getInstance();

    @Override
    public List<Role> getAllRole() {

        String sql = "select * from role where status = 1";
        List<Role> roleList = baseDao.query(sql,null,Role.class);
        return roleList;
    }

    @Override
    public List<Role> getAllRoleInfo(int pageNumber, int pageSize, String searchName) {

        String sql = "select  * from role";

        //通过搜索框里输入的字来模糊查询roleName
        if (StringUtils.isNotBlank(searchName)){
            sql += " where roleName like '%"+ searchName.trim() +"%'";
        }

        sql += " limit ?,?";

        Object[] params = {pageNumber,pageSize};
        List<Role> roleList = baseDao.query(sql,params,Role.class);

        return roleList;
    }

    @Override
    public int getAllRoleInfoCount( String searchName) {
        String sql = "select  count(1) a from role";

        if (!StringUtils.isEmpty(searchName)){
            sql += " where roleName = '"+ searchName.trim() +"'";
        }

        List<Map<String,Object>> list = baseDao.query(sql,null);

        if (list!=null  && list.size() >0){
                return IntegerUtils.ToInteger(list.get(0).get("a")+"");
        }
        return 0;
    }

    @Override
    public int addRole(String roleName, String description, int status) {

        String sql = "insert into role(roleName,description,status) values (?,?,?)";
        Object[] params = {roleName,description,status};
        return  baseDao.executeUpdate(sql,params);
    }


    @Override  // 根据 roleId 查询得到一个模态数据
    public List<TreeMenu> getMenuList(int roleId) {

        // 1.找到角色 已经拥有的权限
        String sqlRole = "SELECT resource_id rId from resource_role where role_id =  ?";

        Object[] paramRole = {roleId};

        List<Map<String,Object>> idList = baseDao.query(sqlRole,paramRole);

        Set<Integer> ids = new HashSet<>();

        for (Map<String,Object> map:idList){
             long id  = (long) map.get("rId");
             ids.add((int)id);
        }

        // 2.查到所有父类的列表
        String sql = "select rs.id nodeid,rs.resource_name text,rs.icon icon,rs.pid pid,rs.sort sort \n" +
                "               from resource rs \n" +
                "                where rs.pid = 0 and rs.status = 1 order by rs.id asc;";


        List<TreeMenu> listParent =   baseDao.query(sql,null, TreeMenu.class);

        // 3.遍历每一个父列表
        for (TreeMenu treeMenu:listParent ){
               // 标记父类是否勾选
               if (ids.contains(treeMenu.getNodeid())){
                   treeMenu.setState("selected");
               }
                //4.查到每一父亲的孩子列表
                String sql1 = "select rs.id nodeid,rs.resource_name text,rs.icon icon,rs.pid pid,rs.sort sort \n" +
                        "               from resource rs \n" +
                        "                where rs.pid = ? and rs.status = 1 order by rs.id asc;";

                Object[] objects = {treeMenu.getNodeid()};
                List<TreeMenu> listNodes =   baseDao.query(sql1,objects, TreeMenu.class);
                // 标记子类是否勾选
                for (TreeMenu node:listNodes){
                    if (ids.contains(node.getNodeid())){
                        node.setState("selected");
                    }
                }
                // 设置子节点
                treeMenu.setTreeMenuList(listNodes);

        }

        return listParent;
    }

    @Override
    public int updateMenuList(int roleId, int[] resouceces) {
        //必要时，开启事务

        // 先删除
        String delSql = "delete from resource_role where role_id = ?";
        Object[] delParams = {roleId};
        baseDao.executeUpdate(delSql,delParams);


        // 在插入
        int len = 0;
        for (int resource_id:resouceces){
            String insertSql = "insert into resource_role (resource_id,role_id)values(?,?)";
            Object[] addParams =  {resource_id,roleId};

            len = baseDao.executeUpdate(insertSql,addParams);
        }
        return len;
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @Override
    public int deleteRole(Integer id) {
        String sql = "delete from role where id = ?";
        Object[] params = {id};
        return baseDao.executeUpdate(sql,params);
    }

	/**
	*更新角色
	*/
    @Override
    public int updateRole(Integer id, String roleName, String description, Integer status) {
        String sql = "update role set roleName=?,description=?,status=? where id=?";
        Object[] params = {roleName,description,status,id};

        return baseDao.executeUpdate(sql,params);
    }

	/**
	*根据role_id删除resource_role表里的对应的权限
	*/
    @Override
    public int deleteRole1(Integer id) {
        String sql = "delete from resource_role where role_id = ?";
        Object[] params = {id};
        return baseDao.executeUpdate(sql,params);
    }

	/**
	*根据role_id，resource_id，增加resource_role表里的对应的权限
	*/
    @Override
    public int addRole1(Integer id, Integer resource_id) {
        String sql = "insert into resource_role(role_id,resource_id)  values(?,?)";
        Object[] params = {id,resource_id};
        return baseDao.executeInsert(sql,params);
    }
}
