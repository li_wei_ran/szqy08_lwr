package com.aaa.dao;

import com.aaa.entity.User;

import java.util.List;
import java.util.Map;


public interface UserDao {
    /**
     * 分页查询所有会员信息
     *
     * @param pageNumber
     * @param pageSize
     * @param searchName
     * @return
     */
    List<User> getAllUserInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName);

    /**
     * 查询所有会员信息的条数
     *
     * @param searchId
     * @param searchName
     * @return
     */
    int getAllUserInfoCount(String searchId, String searchName);

    /**
     *
     *获取最后一个会员的会员Id
     * @param
     * @return
     */
    String getLastUserId();

    /**
     * 增加会员
     * @param user
     * @return
     */
    int addUser(User user);
    /**
     * 删除会员
     * @param user
     * @return
     */

    int deleteUserById(Integer userId);
    /**
     * 修改会员
     * @param user
     * @return
     */
    int updateUser(User user);
    /**
     * 根据
     * @param user
     * @return
     */
    List<Map<String,Object>> getCardInfoById(Integer cardId);
    /**
     * 根据会员卡编号查找user
     * @param cardId
     * @return
     */
    List<String> findUserByCardId(Integer cardId);
}
