package com.aaa.dao;

import com.aaa.entity.CardType;

import java.util.List;

public interface CardTypeDao {
    /**
     * 获取所有的卡类型
     *
     * @return
     */
    List<CardType> getAllCardTypeInfo();
}


