package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.CardDao;
import com.aaa.entity.Card;
import com.aaa.entity.RechargeRecord;
import com.aaa.entity.RechargeRule;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class CardDaoImpl implements CardDao {
    private BaseDao baseDao = new BaseDao();
    @Override
    public String getLastCardId() {
        String sql = "select cardId from user order by id desc limit 1";
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            String cardId = maps.get(0).get("cardId") + "";
            return cardId;
        }
        return null;
    }

    @Override
    public int addCard(Card card) {
        String sql = "insert into card(cardId,userId,amount,credit,status,staffId,levelId) values (?,?,?,?,?,?,?) ";
        Object[] objects = {card.getCardId(),card.getUserId(),card.getAmount(),card.getCredit(),card.getStatus(),card.getStaffId(),card.getLevelId()};
        int len = baseDao.executeUpdate(sql,objects);
        return len;
    }

    @Override
    public List<Card> getAllCard(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        String sql = "select u.userName userName,c.*,ct.name levelName from user u,card c,cardType ct where u.cardId = c.cardId and c.levelId = ct.`level`";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and c.cardId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and u.userName like '" + searchName + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<Card> cards = baseDao.query(sql, params,Card.class);
        return cards;
    }

    @Override
    public int getAllCardInfoCount(String searchId, String searchName) {
        String sql = "select count(1) len from card where 1=1";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and cardId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and u.userName like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql,null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }

    @Override
    public int BanOrAllowedCardByCardId(Integer cardId, Integer status) {
        status = status == 1 ? 0 : 1;
        String sql = "update card set status = ? where cardId = " + cardId;
        Object[] params = {status};
        int len = baseDao.executeUpdate(sql, params);
        return len;
    }

    @Override
    public int deleteCardByCardId(int cardId) {
        String sql = "delete from card where cardId = ?";
        Object [] arr = {cardId};
        return baseDao.executeUpdate(sql,arr) ;
    }

    @Override
    public List<RechargeRecord> getAllRechargeRecord(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        String sql = "select u.userName userName,re.* from rechargerecord re LEFT JOIN user u on re.cardId  = u.cardId where 1 = 1";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and re.cardId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and u.userName like '" + searchName + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<RechargeRecord> rechargeRecords = baseDao.query(sql, params,RechargeRecord.class);
        return rechargeRecords;
    }

    @Override
    public int getAllRechargeRecordCount(String searchId, String searchName) {
        String sql = "select count(1) len from rechargerecord re where 1=1";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and re.cardId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and u.userName like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql,null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }

    @Override
    public List<Card> getCardById(Integer cardId) {
        String sql = "select * from card  where cardId = ? and status = 1";
        Object[] parrms = {cardId};
        return baseDao.query(sql, parrms);
    }


    @Override
    public int rechargeCard(Integer cardId, double amount ,Integer rechargeRule) {
        String sql = "select * from card  where cardId = ?";
        Object[] params ={cardId};
        List<Card> cardList = baseDao.query(sql,params,Card.class);
        if (cardList.size() != 0){
            if(rechargeRule != 0){
                String sql2 ="select * from rechargerule where status = 1 ";
                sql2+=" and id = ?";
                Object[] objects = {rechargeRule};
                List<RechargeRule> rechargeRuleList= baseDao.query(sql2,objects, RechargeRule.class);
                double oldAmount= cardList.get(0).getAmount();
                double newAmount = rechargeRuleList.get(0).getCoefficient()*rechargeRuleList.get(0).getStartMoney() + oldAmount+amount;
                String sql1 = "update card set amount = ? where cardId = ? ";
                Object[] params1 = {newAmount,cardId};
                return baseDao.executeUpdate(sql1,params1);
            }else{
                double newAmount = cardList.get(0).getAmount() + amount;
                String sql1 = "update card set amount = ? where cardId = ? ";
                Object[] params1 = {newAmount,cardId};
                return baseDao.executeUpdate(sql1,params1);
            }
        }
        return 0;
    }
}
