package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.UserDao;
import com.aaa.entity.User;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class UserDaoImpl implements UserDao {
    private BaseDao baseDao = new BaseDao();


    @Override
    public List<User> getAllUserInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        /* String sql = "select u.*,c.amount amount,c.credit,c.staffId staffId,u.address address,ct.name from user u,card c,cardtype ct where u.cardId = c.cardId and c.levelId = ct.level "*/;
        String sql ="select user.*,card.amount,card.credit,card.staffId,user.address,cardtype.name levelName from user,card,cardtype where user.cardId= card.cardId and card.levelId = cardtype.level";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and user.userId = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and userName like '" + searchName + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<User> user = baseDao.query(sql, params, User.class);
        return user;
    }

    @Override
    public int getAllUserInfoCount(String searchId, String searchName) {
        String sql = "select count(1) len from user where 1=1";
        if (StringUtils.isNotBlank(searchId)) {
            sql += " and 'userId' = " + searchId;
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and 'userName' like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }
    @Override
    public String getLastUserId() {
        String sql = "select userId from user order by id desc limit 1";
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            String userId = maps.get(0).get("userId") + "";
            return userId;
        }
        return null;
    }

    @Override
    public int addUser(User user) {
        String sql = "insert into user (userId,cardId,userName,status,phone,idCard,sex,address,createdTime,momo,area,staffId)" +
                "values (?,?,?,?,?,?,?,?,?,?,?,?)";
        Object[] params = {user.getUserId(), user.getCardId(), user.getUserName(), user.getStatus(), user.getPhone(),
                user.getIdCard(), user.getSex(), user.getAddress(), user.getCreatedTime(),user.getMomo(),user.getArea(),user.getStaffId()};
        return baseDao.executeUpdate(sql, params);
    }
    @Override
    public int deleteUserById(Integer userId) {
        String sql = "delete from card where userId = ?";
        Object [] arr = {userId};
        return baseDao.executeUpdate(sql,arr) ;
    }
    @Override

    public int updateUser(User user) {
        String sql = "update user set userName = ?,phone= ?,status= ?,address= ?,idCard=?where userId = ?";
        Object[] params = {user.getUserName(), user.getPhone(), user.getStatus(), user.getAddress(), user.getIdCard(),user.getUserId()};
        return baseDao.executeUpdate(sql, params);
    }

    @Override
    public List<Map<String, Object>> getCardInfoById(Integer cardId) {
//        String sql = "SELECT u.userId,userName,c.amount,c.cardId,c.levelId,c.credit,ct.`name` from user u LEFT JOIN card c on user.userId = c.userId LEFT JOIN cardtype ct on ct.level = c.levelId where  c.cardId = ?";
        String sql1 = "select user.userId,user.userName,card.amount,card.cardId,card.levelId,card.credit,cardtype.name from user left join card on user.userId = card.userId left join cardtype on cardtype.level = card.levelId where card.cardId = ?";
        Object[] objects = {cardId};
        List<Map<String,Object>> list = baseDao.query(sql1,objects);
        if (list.size() > 0){
            return list;
        }else {
            return null;
        }
    }

    @Override
    public List<String> findUserByCardId(Integer cardId) {
        String sql = "SELECT `user`.id,`user`.userId,`user`.userName,`user`.cardId,card.credit,card.amount,cardtype.`name` FROM cardtype,`user`,card " +
                "WHERE `user`.userId = card.userId AND card.levelId = cardtype.`level` and `card`.cardId=?";
        System.out.println(sql);
        Object[] params = {cardId};
        List<String> list = baseDao.query(sql, params);
        System.out.println(list);
        return list;
    }
}
