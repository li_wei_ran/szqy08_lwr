var addGoodsUrl = "/AddGoodsServlet";
var CategoryUrl = "/GetAllGoodsCategoryServlet";
var unitUrl = "/GetAllGoodsUnitServlet";
var initGoodsIdUrl = "/initGoodsIdServlet";


$(function () {
    // 初始化商品类型下拉
    goodsCateGory.initList();
    // 初始化商品单位下拉
    goodsUnit.initList();
    // 初始化goodsId
    initGoodsId();
    // 初始化表单验证
    formValidator();
})
var goodsCateGory = {};
var goodsUnit = {};

/**
 * 添加商品信息
 */
function addGoods() {
    $.ajax({
        url: addGoodsUrl,// 自己完成后台
        type: 'post',
        data: {
            "goodsId":$("#goodsId").val(),
            "goodsName": $("#goodsName").val(),
            "goodsCode":$("#goodsCode").val(),
            "goodsType":$("#goodsType").val(),
            "goodsCategory": $("#goodsCategory").val(),
            "goodsUnit": $("#goodsUnit").val(),
            "goodsPrice": $("#goodsPrice").val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                toastr['success']("操作成功");
                    var a = parseInt($("#goodsId").val());
                    $("#goodsId").val(a+1);
                    $("#goodsName").val(null);
                    $("#goodsCode").val(null);
                    $("#goodsType").find("option:first").prop("selected",true);
                    $("#goodsCategory").find("option:first").prop("selected",true);
                    $("#goodsUnit").find("option:first").prop("selected",true);
                    $("#goodsPrice").val(null);
                //重置校验
                $("#addGoodsList").data('bootstrapValidator').destroy();
                $('#addGoodsList').data('bootstrapValidator', null);
                formValidator();
            } else {
                toastr['error']("操作失败");
            }
        }
    })
}
/**
 * 初始化商品单位
 */
goodsUnit.initList = function(){

    $.ajax({
        url: unitUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0 ) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].name + "</option>");
                    $("#goodsUnit").append(opt);
                }
            }
        }
    })
}
/**
 * 初始化商品类型
 */
goodsCateGory.initList = function(){

    $.ajax({
        url: CategoryUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0 ) {
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    var opt = $("<option value='" + res[i].id + "'>" + res[i].name + "</option>");
                    $("#goodsCategory").append(opt);
                }
            }
        }
    })
}

/**
 * 初始化商品编号
 */
function initGoodsId(){
    $.ajax({
        url: initGoodsIdUrl,
        type: 'get',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0 ) {
                var initGoodsId = result.data;
                $("#goodsId").val(initGoodsId[0].goodsId);
            }
        }
    })
}

/**
 * 重置新增输入框
 */
function resetAddGoods(){
    $("#goodsName").val(null);
    $("#goodsCode").val(null);
    $("#goodsPrice").val(null);

    $("#goodsType").find("option:first").prop("selected",true);
    $("#goodsCategory").find("option:first").prop("selected",true);
    $("#goodsUnit").find("option:first").prop("selected",true);

    //重置校验
    $("#addGoodsList").data('bootstrapValidator').destroy();
    $('#addGoodsList').data('bootstrapValidator', null);
    formValidator();
}



/**
 * 表单提交验证
 * */
function aaa (){
    var bootstrapValidator = $("#addGoodsList").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
    }else {
        alert("验证失败");
        return false;
    }
}
function formValidator(){
$("#addGoodsList").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        goodsName: {
            validators: {
                notEmpty: {
                    message: "商品名称不能为空"
                }
            }
        },
        goodsCode:{
            validators: {
                notEmpty: {
                    message: "商品数量不能为空"
                },
                callback:{
                    message:"商品数量不能为0",
                    callback: function (value) {
                        if (value == "0") {
                            return false;
                        } else {
                            return true
                        }
                    }
                }
            }
        },
        goodsUnit: {
            validators: {
                notEmpty: {
                    message:"必须选择商品单位"
                }
            }
        },
        goodsType: {
            validators: {
                notEmpty: {
                }
            }
        },
        goodsCategory:{
            validators: {
                notEmpty: {
                    message:"必须选择商品类别",
                }
            }
        },
        goodsPrice:{
            validators: {
                notEmpty: {
                    message: "商品价格不能为空"
                },
                callback:{
                    message:"商品价格不能为0",
                    callback: function (value) {
                        if (value == "0") {
                            return false;
                        } else {
                            return true
                        }
                    }
                }
            }
        }
    }
})
};