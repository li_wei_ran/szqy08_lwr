
var staffManage = {}

$(function () {

    //初始化员工信息
    staffManage.init();

});

//通过员工ID得到员工的个人信息
staffManage.init = function () {
    $.ajax({
        url: '/GetStaffInformationServlet',
        type: 'post',
        data: {
            //发给后台
            //浏览器缓存
            "staffId": window.localStorage.getItem("staffId"),//1.localStorage长期存在  window.localStorage.("staffId") 2.sessionStorage 临时 关闭就清空
        },
        dataType: 'json',
        success:function (result) {
            if (result.status > 0) {
                var r = result.data;
                console.log(r.createdTime)
                //赋值
                $("#staffId").val(r.staffId);
                $("#staffName").val(r.staffName);
                $("#phone").val(r.phone);
                $("#idCard").val(r.idCard);
                $("#address").val(r.address);
                $("#createdTime").val(r.createdTime);
            }
        }
    })
}

function change() {
    // 调用显示模态框
    $("#changePassword").modal('show');

}

function updatePassword() {

    $.ajax({
        url:'/',
        type:'post',
        data: {
            //将对应员工ID的密码通过ajax提交到后台
            "staffId":window.localStorage.getItem("staffId"),
            "newPassword":$("#newPassword").val()
        },
        dataType:'json',
        success:function (result) {
            if (result.status > 0) {
                toastr['success']("增加成功");
                $("#addModal").modal('hide');
            } else {
                toastr['error']("增加失败");
            }
        }

    });

}