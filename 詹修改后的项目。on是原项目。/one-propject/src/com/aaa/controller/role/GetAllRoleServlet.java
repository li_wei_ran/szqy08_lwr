package com.aaa.controller.role;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.aaa.util.IntegerUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 分页获取所有的角色信息
 */
@WebServlet("/GetAllRoleServlet")
public class GetAllRoleServlet extends HttpServlet {

    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        roleService = new RoleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();
        try {
            //Integer 有null，int没有null，为0
            Integer pageNumber = IntegerUtils.ToInteger(req.getParameter("pageNumber"));
            Integer pageSize = IntegerUtils.ToInteger(req.getParameter("pageSize"));
            String searchRoleName = req.getParameter("searchRoleName");
            responseDto.setData(roleService.getAllRoleInfo(pageNumber,pageSize,searchRoleName));
            responseDto.setMessage("请求成功");
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
        }catch (Exception e ){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
