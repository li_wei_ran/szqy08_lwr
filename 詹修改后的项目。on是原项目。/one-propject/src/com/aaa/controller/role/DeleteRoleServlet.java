package com.aaa.controller.role;

import com.aaa.entity.ResponseDto;
import com.aaa.service.RoleService;
import com.aaa.service.RoleServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/DeleteRoleServlet")
public class DeleteRoleServlet extends HttpServlet {

    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        roleService = new RoleServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResponseDto responseDto = new ResponseDto();

        try {
            //获取通过ajax得到的角色id
            String id = req.getParameter("id");

            //调用roleService的删除方法
            roleService.deleteRole(Integer.valueOf(id));
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("删除成功");
        }catch (Exception e ){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("删除失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
