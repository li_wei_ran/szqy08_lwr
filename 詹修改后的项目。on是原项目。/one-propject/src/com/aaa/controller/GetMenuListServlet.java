package com.aaa.controller;

import com.aaa.entity.Menu;
import com.aaa.entity.ResponseDto;
import com.aaa.service.MenuService;
import com.aaa.service.MenuServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Administrator
 */
@WebServlet("/GetMenuListServlet")
public class GetMenuListServlet extends HttpServlet {
   private MenuService menuService ;
    @Override
    public void init() throws ServletException {
        menuService = new MenuServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //返回参数
        ResponseDto responseDto = new ResponseDto();
        try {
            Integer roleId = Integer.parseInt(StringUtils.isBlank(request.getParameter("roleId")) ?
                    "0" : request.getParameter("roleId"));

            List<Menu> lists = menuService.getMenuList(roleId);

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("查询成功");
            responseDto.setData(lists);
            response.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("请求失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
