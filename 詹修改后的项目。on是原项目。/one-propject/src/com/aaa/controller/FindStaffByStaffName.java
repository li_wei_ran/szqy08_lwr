package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Staff;
import com.aaa.service.StaffService;
import com.aaa.service.impl.StaffServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/FindStaffByStaffName")
public class FindStaffByStaffName extends HttpServlet {
    StaffService staffService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String staffName = req.getParameter("staffName");
        List<Staff> staffs = staffService.findStaffByStaffName(staffName);

        ResponseDto responseDto = new ResponseDto();
        if (staffs.size() != 0){
            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setData(staffs);
            responseDto.setMessage("查询成功");
        }else {
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage("查询失败");
        }
        resp.getWriter().write(new Gson().toJson(responseDto));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        staffService = new StaffServiceImpl();
    }
}
