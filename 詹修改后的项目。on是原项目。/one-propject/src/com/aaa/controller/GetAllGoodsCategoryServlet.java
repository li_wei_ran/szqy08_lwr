package com.aaa.controller;

import com.aaa.entity.Category;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CategoryService;
import com.aaa.service.CategoryServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/GetAllGoodsCategoryServlet")
public class GetAllGoodsCategoryServlet extends HttpServlet {
    CategoryService categoryService = null;
    @Override
    public void init() throws ServletException {
        categoryService = new CategoryServiceImpl();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回参数
        ResponseDto responseDto = new ResponseDto();
        try {
            List<Category> categoryList = categoryService.GetCategoryList();

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("查询成功");
            responseDto.setData(categoryList);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("查询失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
