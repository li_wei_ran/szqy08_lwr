package com.aaa.controller;

import com.aaa.entity.Category;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CategoryService;
import com.aaa.service.CategoryServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddCategoryServlet")
public class AddCategoryServlet extends HttpServlet {
    CategoryService categoryService = null;

    @Override
    public void init() throws ServletException {
        categoryService = new CategoryServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String categoryName = req.getParameter("categoryName");
        String status = req.getParameter("status");
        String momo = req.getParameter("momo");

        Category category = new Category();
        category.setName(categoryName);
        category.setStatus(Integer.valueOf(status));
        category.setMomo(momo);
        ResponseDto responseDto = new ResponseDto();
        try {
            boolean changeStatus = categoryService.AddCategory(category);

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("修改商品分类成功");
            responseDto.setData(changeStatus);
            resp.getWriter().print(new Gson().toJson(responseDto));
        }catch (Exception e){
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("修改商品分类失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
