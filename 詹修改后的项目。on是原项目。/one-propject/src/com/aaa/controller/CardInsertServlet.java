package com.aaa.controller;

import com.aaa.entity.Card;
import com.aaa.entity.ResponseDto;
import com.aaa.service.CardService;
import com.aaa.service.impl.CardServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CardInsertServlet")
public class CardInsertServlet extends HttpServlet {
    CardService cardService;
    @Override
    public void init() throws ServletException {
        cardService = new CardServiceImpl();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("utf-8");
        String id = req.getParameter("id");
        String userId = req.getParameter("userId");
        String staffName = req.getParameter("staffName");
        String phone = req.getParameter("phone");
        String status = req.getParameter("status");
        String idCard = req.getParameter("idCard");
        String birthday = req.getParameter("birthday");
        String sex = req.getParameter("sex");
        String address = req.getParameter("address");
        String area = req.getParameter("area");
        String createTime = req.getParameter("createTime");
        Card card = new Card();
        ResponseDto responseDto = new ResponseDto();
        try{

            if (userId!=null){
                card.setId(Integer.valueOf(id));
                card.setUserId(Integer.valueOf(userId));
                card.setStaffName(staffName);
                card.setPhone(phone);
                card.setStatus(Integer.valueOf(status));
                card.setIdCard(idCard);
                card.setBirthday(birthday);
                card.setSex(Integer.valueOf(sex));
                card.setAddress(address);
                card.setArea(area);
                card.setCreatedTime(createTime);
//                try {
                System.out.println("---"+card);
                cardService.updateCardByCardId(card);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
                responseDto.setStatus(ResponseDto.SUCCESS_CODE);
                responseDto.setMessage("success!!");
            }else {
                responseDto.setStatus(ResponseDto.FAILURE_CODE);
                responseDto.setMessage("fail!!不存在该用户");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        resp.getWriter().print(new Gson().toJson(responseDto));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
