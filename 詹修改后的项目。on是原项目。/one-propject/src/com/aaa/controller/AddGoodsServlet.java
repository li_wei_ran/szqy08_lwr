package com.aaa.controller;

import com.aaa.entity.Goods;
import com.aaa.entity.ResponseDto;
import com.aaa.service.GoodsService;
import com.aaa.service.GoodsServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddGoodsServlet")
public class AddGoodsServlet extends HttpServlet {
    GoodsService goodsService = null;

    @Override
    public void init() throws ServletException {
        goodsService = new GoodsServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String goodsId = req.getParameter("goodsId");
        String goodsName = req.getParameter("goodsName");
        String goodsCode = req.getParameter("goodsCode");
        String goodsType = req.getParameter("goodsType");
        String goodsCategory = req.getParameter("goodsCategory");
        String goodsUnit = req.getParameter("goodsUnit");
        String goodsPrice = req.getParameter("goodsPrice");

        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setName(goodsName);
        goods.setCode(goodsCode);
        goods.setType(Integer.valueOf(goodsType));
        goods.setCategoryId(Integer.valueOf(goodsCategory));
        goods.setUnitId(Integer.valueOf(goodsUnit));
        goods.setPrice(Double.valueOf(goodsPrice));
        goods.setStatus(2);


        ResponseDto responseDto = new ResponseDto();
        try {
            boolean addGoods = goodsService.AddGoods(goods);;

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("添加商品成功");
            responseDto.setData(addGoods);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("添加商品失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
