package com.aaa.controller;

import com.aaa.entity.ResponseDto;
import com.aaa.entity.Unit;
import com.aaa.service.UnitService;
import com.aaa.service.UnitServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/GetAllGoodsUnitServlet")
public class GetAllGoodsUnitServlet extends HttpServlet {
    UnitService unitService = null;
    @Override
    public void init() throws ServletException {
        unitService = new UnitServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回参数
        ResponseDto responseDto = new ResponseDto();
        try {
            List<Unit> units = unitService.getUnitList();

            responseDto.setStatus(ResponseDto.SUCCESS_CODE);
            responseDto.setMessage("查询成功");
            responseDto.setData(units);
            resp.getWriter().print(new Gson().toJson(responseDto));
        } catch (Exception e) {
            e.printStackTrace();
            responseDto.setStatus(ResponseDto.FAILURE_CODE);
            responseDto.setMessage(e.getMessage());
            responseDto.setMessage("查询失败");
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
