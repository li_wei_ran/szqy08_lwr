package com.aaa.dao;

import com.aaa.entity.Goods;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.util.Map;

public class GoodsDaoImpl implements GoodsDao {

    private BaseDao baseDao = BaseDao.getInstance();

    @Override
    public int updateGoods(Goods goods) {
        String sql = "update goods set name=?,price=?,code=?,unitId=?,categoryId=? where goodsId=?";
        Object[] params = {goods.getName(),goods.getPrice(),goods.getCode(),goods.getUnitId(),goods.getCategoryId(),goods.getGoodsId()};
        return  baseDao.executeUpdate(sql,params);
    }

    @Override
    public int changeStatus(Goods goods) {
        String sql = "update goods set status=? where id=?";
        Object[] params = {goods.getStatus(),goods.getId()};
        return  baseDao.executeUpdate(sql,params);
    }

    @Override
    public List<Goods> getAllGoodsInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        String sql = "select g.*,u.name as unitName,c.name as categoryName from goods g LEFT JOIN category c ON g.categoryId=c.id LEFT JOIN unit u on g.unitId=u.id where 1=1";
        if (StringUtils.isNotBlank(searchId)) {
//            sql += " and g.goodsId   = " + searchId;
            searchId = "%" + searchId + "%";
            sql += " and goodsId like '" + searchId + "'";
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and g.name like '" + searchName + "'";
        }
        sql += " limit ?,?";
        Object[] params = {pageNumber, pageSize};
        List<Goods> goods = baseDao.query(sql,params,Goods.class);
        return goods;
    }

    @Override
    public int getAllGoodsInfoCount(String searchId, String searchName) {
        String sql = "select count(1) len from goods where 1=1";
        if (StringUtils.isNotBlank(searchId)) {
//            sql += " and goodsId = " + searchId;
            searchId = "%" + searchId + "%";
            sql += " and goodsId like '" + searchId + "'";
        }
        if (StringUtils.isNotBlank(searchName)) {
            searchName = "%" + searchName + "%";
            sql += " and name like '" + searchName + "'";
        }
        List<Map<String, Object>> maps = baseDao.query(sql, null);
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = maps.get(0);
            Integer res = Integer.parseInt(map.get("len") + "");
            return res;
        }
        return 0;
    }

    @Override
    public int addGoods(Goods goods) {
        String sql="insert into goods(goodsId,name,code,type,unitId,price,categoryId,status) values(?,?,?,?,?,?,?,?)";
        Object[] params = {goods.getGoodsId(),goods.getName(),goods.getCode(),goods.getType(),goods.getUnitId(),goods.getPrice(),goods.getCategoryId(),goods.getStatus()};
        return baseDao.executeUpdate(sql,params);
    }

    @Override
    public List<Goods> initGoodsId() {
        String sql = "select max(goodsId)+1 as goodsId from goods";
        return baseDao.query(sql,null,Goods.class);
    }
}
