package com.aaa.dao.impl;

import com.aaa.dao.BaseDao;
import com.aaa.dao.CardDao;
import com.aaa.entity.Card;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class CardDaoImpl implements CardDao {
    private BaseDao baseDao = BaseDao.getInstance();
    private PreparedStatement pstm;
    private Connection conn;
    @Override
    public int updateCardByCardId(Card card) {
        int i =0;
        String sql = "UPDATE staff s,card c set s.staffName=?,s.phone=?,status=?,s.idCard=?,c.cardId=?,c.credit=?,s.address=? WHERE c.id=?";
       i = baseDao.executeUpdate(sql,new Object[]{
               card.getStaffName(),card.getPhone(),card.getStatus(),card.getIdCard(),card.getCardId(),card.getCredit(),card.getAddress(),card.getId()
       });
        return i;
    }

    @Override
    public int addCardByCardId(Card card) {
       int i = 0;
       String sql="insert into card(cardId,staffName,phone,address,idCard,status,cardId,credit,createTime) values(?,?,?,?,?,?,?,?,?)";
        try {
            pstm = conn.prepareStatement(sql);
            pstm.setInt(1,card.getCardId());
            pstm.setString(2,card.getStaffName());
            pstm.setString(3,card.getPhone());
            pstm.setString(4,card.getAddress());
            pstm.setString(5,card.getIdCard());
            pstm.setInt(6,card.getStatus());
            pstm.setInt(7,card.getCardId());
            pstm.setInt(8,card.getCredit());
            pstm.setString(9,card.getCreatedTime());
            i = pstm.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public List<Card> getAllCardInfo(Integer pageNumber, Integer pageSize, String searchId, String searchName) {

        return null;
    }


    @Override
    public int getAllCardInfoCount(String searchId, String searchName) {
        return 0;
    }
}
