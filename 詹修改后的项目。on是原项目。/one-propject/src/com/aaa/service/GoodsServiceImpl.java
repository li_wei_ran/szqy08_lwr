package com.aaa.service;

import com.aaa.dao.GoodsDao;
import com.aaa.dao.GoodsDaoImpl;
import com.aaa.entity.Goods;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoodsServiceImpl implements GoodsService{
    private GoodsDao goodsDao = new GoodsDaoImpl();

    @Override
    public boolean UpdateGoods(Goods goods) {
        int num = goodsDao.updateGoods(goods);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean ChangeStatus(Goods goods) {
        int num = goodsDao.changeStatus(goods);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public Map<String, Object> getAllGoodsInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
        List<Goods> list = goodsDao.getAllGoodsInfo(pageNumber, pageSize, searchStuno, searchName);

        // 总页数
        int count = goodsDao.getAllGoodsInfoCount(searchStuno, searchName);

        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }

    @Override
    public boolean AddGoods(Goods goods) {
        int num = goodsDao.addGoods(goods);
        if (num>0){
            return true;
        }
        return false;
    }

    @Override
    public List<Goods> InitGoodsId() {
        return goodsDao.initGoodsId();
    }
}
