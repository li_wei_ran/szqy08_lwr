package com.aaa.service;

import com.aaa.entity.Category;
import com.aaa.entity.Goods;

import java.util.List;
import java.util.Map;

public interface CategoryService {
    /**
     * 商品列表对应商品类型的动态下拉框
     * @return
     */
    List<Category> GetCategoryList();

    /**
     * 分页查询所有商品分类类型的信息
     * @param pageNumber
     * @param pageSize
     * @return
     */
    Map<String,Object> getAllCategoryInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName)throws Exception;

    /**
     * 修改商品类别信息
     * @param category
     * @return
     */
    boolean UpdateCategory(Category category);

    /**
     * 改变商品类别状态
     * @param category
     * @return
     */
    boolean ChangeStatus(Category category);

    /**
     * 添加商品类别信息
     * @param category
     * @return
     */
    boolean AddCategory(Category category);
}
