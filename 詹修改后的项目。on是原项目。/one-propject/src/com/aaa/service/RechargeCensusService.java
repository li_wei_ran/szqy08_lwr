package com.aaa.service;

import com.aaa.entity.RechargeCensus;

import java.util.List;
import java.util.Map;

public interface RechargeCensusService {
    /**
     * 增加用
     * @return
     */
    boolean addrecharge(RechargeCensus rechargeCensus, double amount);

    List<Map<String,Object>> getDataByNearYear();
}
