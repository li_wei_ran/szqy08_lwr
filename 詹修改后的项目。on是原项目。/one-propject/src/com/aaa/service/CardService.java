package com.aaa.service;

import com.aaa.entity.Card;

import java.util.Map;

public interface CardService {
    int updateCardByCardId(Card card);

    int addCardByCardId(Card card);

    Map<String,Object> getAllCardInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName)throws Exception;
}
