package com.aaa.service.impl;

import com.aaa.dao.StaffDao;
import com.aaa.dao.impl.StaffDaoImpl;
import com.aaa.entity.Staff;
import com.aaa.service.StaffService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaffServiceImpl implements StaffService {
    private StaffDao staffDao = new StaffDaoImpl();

    @Override
    public Staff login(String staffId, String password) {
        return staffDao.login(staffId,password);
    }

    @Override
    public int updateStaffByStaffId(Staff staff) {
        return staffDao.updateStaffByStaffId(staff);
    }

    @Override
    public int addStaffByStaffId(Staff staff) {
        return staffDao.addStaffByStaffId(staff);
    }

    @Override
    public int deleteStaffByStaffId(int staffId) {
        return staffDao.deleteStaffByStaffId(staffId);
    }

    @Override
    public List<Staff> findAllStaff(Integer pageNumber, Integer pageSize, String searchId, String searchName) {
        return staffDao.getAllStaffInfo(pageNumber,pageSize,searchId,searchName);
    }

    @Override
    public List<Staff> findStaffByStaffId(int staffId) {
        return staffDao.findStaffByStaffId(staffId);
    }

    @Override
    public List<Staff> findStaffByStaffName(String staffName) {
        return staffDao.findStaffByStaffName(staffName);
    }

    @Override
    public List<Map<String, Object>> getRole() {
        return staffDao.getRole();
    }

    @Override
    public List<Staff> getMaxStaffId() {
        return staffDao.getMaxStaffId();
    }


    @Override
    public Map<String, Object> getAllStaffInfo(Integer pageNumber, Integer pageSize, String searchStuno, String searchName) throws Exception {
        if (pageNumber == null || pageNumber == 0) {
            throw new BusinessException("当前页数不能为空");
        }
        if (pageSize == null || pageSize == 0) {
            throw new BusinessException("每页条数不能为空");
        }
        pageNumber = (pageNumber - 1) * pageSize;
        // 当前页码数据
        List<Staff> list = staffDao.getAllStaffInfo(pageNumber, pageSize, searchStuno, searchName);

        // 总页数
        int count = staffDao.getAllStaffInfoCount(searchStuno, searchName);


        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);
        return map;
    }


    @Override
    public Staff getStaffByStaffId(int staffId) {
        //1。根据id 去数据库获取ID对应的那一条记录

        return staffDao.getStaffInformation(staffId);
    }

	/**
	*修改密码
	*/
    @Override
    public int updateStaffPassword(int staffId, String password) {
        return staffDao.updateStaffPassword(staffId,password);
    }

}
