package com.aaa.service.impl;

import com.aaa.dao.RechargeRuleDao;
import com.aaa.dao.impl.RechargeRuleDaoImpl;
import com.aaa.entity.RechargeRule;
import com.aaa.service.RechargeRuleService;
import com.aaa.util.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RechargeRuleServiceImpl implements RechargeRuleService {
    private RechargeRuleDao rechargeRuleDao = new RechargeRuleDaoImpl();
    @Override
    public List<RechargeRule> getAllRechargeRule() {
        return rechargeRuleDao.getAllRechargeRule();
    }

    @Override
    public Map<String, Object> getAllRechargeRuleInfo(int pageNumber, int pageSize, String name, String status) throws Exception {
        if (pageNumber == 0){
            throw new BusinessException("当前页数不能为0");
        }

        if (pageSize == 0){
            throw  new BusinessException("每页条数不能为0");
        }

        Map<String,Object> map = new HashMap<>();

        pageNumber = (pageNumber -1)*pageSize;

        List<RechargeRule> rechargeRuleList =  rechargeRuleDao.getAllRechargeRuleInfo(pageNumber,pageSize,name,status);
        int count = rechargeRuleDao.getAllRechargeRuleInfoCount(name, status);

        map.put("list",rechargeRuleList);
        map.put("count",count);

        return map;
    }


    @Override
    public int addRechargeRule(RechargeRule rechargeRule) {
        return rechargeRuleDao.addRechargeRule(rechargeRule);
    }

    @Override
    public int updateRechargeRule(RechargeRule rechargeRule) {
        return rechargeRuleDao.updateRechargeRule(rechargeRule);
    }

    @Override
    public int deleteRechargeRule(int id) {
        return rechargeRuleDao.deleteRechargeRule(id);
    }

    @Override
    public int updateStatus(RechargeRule rechargeRule) {
        return rechargeRuleDao.updateStatus(rechargeRule);
    }
}
