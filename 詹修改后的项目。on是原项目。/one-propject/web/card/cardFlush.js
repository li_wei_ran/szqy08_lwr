var cardListUrl = "newAgain_war_exploded/GetCardAllServlet";
var updateOrAddCardUrl="newAgain_war_exploded/GetCardUpdateServlet";
var deleteCardUrl="newAgain_war_exploded/DeleteServlet";

$(function () {
    // 初始化分页列表
    cardManage.initList();
})
var cardManage = {};


/**
 * 会员
 */
cardManage.initList = function () {
    $("#cardList").bootstrapTable({
        url: cardListUrl, //请求路径
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存
        pagination: true, //是否显示分页
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: cardManage.queryParams, //传递参数
        queryParamsType: '',
        sidePagination: 'server', // 分页方式
        pageNumber: 1, //初始化页数为第一页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id",
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        // onClickRow: function ( row,e, element) {  // 设置行点击事件
        //     alert('监控行点击事件：'+row.staffId)
        // },
        responseHandler: function (result) {
            console.log(result.data.count);
            if (result.status==1) {
                return {
                    'total': result.data.count, //总条数
                    'rows': result.data.list //所有的数据
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表
            field: 'id',
            title: "商品编号",
            visible: false
        }, {
            field: 'cardID',
            title: "员工编号"
        }, {
            field: 'cardName',
            title: "员工姓名"
        },
            {
            field: 'phone',
            title: "员工手机号"
        }, {
            field: 'address',
            title: "员工地址"
        },{
                field: 'idCard',
                title: "员工身份证"
            },  {
            field: 'status',
            title: "状态",
            formatter: function (value) {
                switch (value) {
                    case 1 :
                        return "启动";
                    case 2 :
                        return "禁用";

                }
            }
        }, {
            field: 'cardId',
            title: "会员卡id"
        }, {
            field: 'credit',
            title: "会员卡余额"
        }, {
            field: 'createdTime',
            title: "创建时间"
        }, {
            field: 'operation',
            events: buttonOperateEvent, // 设置每一行按钮的点击事件
            title: '操作',
            formatter: function (value, row, index) {  // 为当前行增加按钮
                return cardManage.buttonOption(value, row, index);
            }
        }
        ]
    });
}

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
cardManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "searchCardId": $("#searchCardId").val(),
        "searchName": $("#searchName").val()
    }
}
window.buttonOperateEvent = {
    // 更新
    'click .updateCard': function (e, value, row, index) {
        $("#sId").val(row.id);
        $("#cardId").val(row.cardId);
        $("#staffName").val(row.staffName);
        $("#phone").val(row.phone);
        $("#idCard").val(row.idCard);
        $("#credit").val(row.credit);
        $("#address").val(row.address);
        //
        //2.显示在模态框的确定按钮添加事件
        $("#myModal").modal('show');


    },
    // 删除员工
    'click .delCard': function (e, value, row, index) {
        cardManage.del(row);
    }
}
cardManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateCard">修改</button>');
    returnButton.push('<button class="btn btn-danger delCard">删除</button>');
    turnreturnButton.join('');
}
/**
 * 刷新列表
 */
cardManage.search = function () {
    //bootstrapTable 刷新
    $("#cardList").bootstrapTable('refresh');
}

cardManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days / 365);
}
/**
 * 删除员工
 */
cardManage.del = function (row) {

    Modal.confirm({
        msg: "确认当前操作"
    }).on(function (e) {
        if (e) {
            alert("使用ajax 执行 删除的动作 删除"+row.cardId);
            $.ajax({
                url: deleteCardUrl,
                type: 'post',
                data: {
                    "cardId": row.cardId,
                },
                dataType: 'json',
                success: function (result) {
                    if (result.status > 0) {
                        toastr['success']("操作成功");
                    } else {
                        toastr['error']("操作失败");
                    }
                }
            })

        }
    })
}
$(function () {
    // 初始模态框
    initRole();

})
function initRole() {
    var res = JSON.parse("[{\"id\":1,\"roleName\":\"管理员\",\"grade\":1,\"description\":\"管理员\",\"status\":1},{\"id\":2,\"roleName\":\"吧台\",\"grade\":2,\"description\":\"小卖部\",\"status\":1},{\"id\":3,\"roleName\":\"前台\",\"grade\":3,\"description\":\"前台\",\"status\":1},{\"id\":4,\"roleName\":\"收银员\",\"grade\":4,\"description\":\"收银员\",\"status\":1},{\"id\":5,\"roleName\":\"店长\",\"grade\":5,\"description\":\"店长\",\"status\":1},{\"id\":7,\"roleName\":\"新增职位\",\"description\":\"新增的职位\",\"status\":1},{\"id\":8,\"roleName\":\"保洁\",\"description\":\"保洁--负责用泳池消毒\",\"status\":1}]");

    for (var i = 0; i < res.length; i++) {
        var opt = $("<option value='" + res[i].id + "'>" + res[i].roleName + "</option>");
        $("#roleId").append(opt);
    }
}
/**
 * 更新员工信息
 */
function updateCard() {
    $.ajax({
        url: updateOrAddCardUrl,
        type: 'post',
        data: {
            "id":$("#sId").val(),
            "staffId": $("#cardId").val(),
            "staffName": $("#staffName").val(),
            "phone": $("#phone").val(),
            "status": $("#status").val(),
            "idCard": $("#idCard").val(),
            "cardId": $("#cardId").val(),
            "credit": $("#credit").val(),
            "address": $("#address").val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                toastr['success']("操作成功");
                // alert("操作成功");
                // toastr['error']("操作失败");
                $("#myModal").modal('hide');
                window.location.reload();
            } else {
                toastr['error']("操作失败");
            }
        }
    })
    $("#myModal").modal('hide');
}