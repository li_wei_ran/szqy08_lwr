var ruleListUrl = "/GetAllRuleServlet";

$(function () {
    // 初始化分页列表
    ruleManage.initList();
});

var ruleManage = {};

ruleManage.initList = function () {
    $("#ruleList2").bootstrapTable({
        url: ruleListUrl, //请求路径 **********************************
        method: 'post', //请求方式(*)
        contentType: 'application/x-www-form-urlencoded', //使用from表单方式提交(*)
        toolbar: '#toolbar', //工具按钮的容器
        striped: true, //是否启用隔行变色
        cache: false, //使用是否缓存 默认为true,所以一般情况下需要设置一下为false (*)
        pagination: true, //是否显示分页(*)
        sortable: false, //使用启用排序
        sortOrder: 'desc', //排序方式
        queryParams: ruleManage.queryParams, //传递参数(*)  *************************************************
        queryParamsType: '',
        sidePagination: 'server', // 分页方式有两种 1.client 客户端分页  2.server分页
        pageNumber: 1, //初始化页数为第一页  显示第几页
        pageSize: 5, //默认每页加载行数
        pageList: [10, 25, 50, 100], //每页可选择记录数
        strictSearch: true,
        showColumns: false, // 是否显示所有的列
        showRefresh: false, // 是否显示刷新按钮
        minimumCountColumns: 2, // 最少允许的列数
        clickToSelect: true, // 是否启用点击选中行
        uniqueId: "id", // 每一行的唯一标识，一般为主键列
        showToggle: false, // 是否显示详细视图和列表视图的切换按钮
        cardView: false, // 是否显示详细视图
        detailView: false, // 是否显示父子表
        smartDisplay: false,
        onClickRow: function (e, row, element) {  // 设置行点击事件***************************
            //alert('监控行点击事件：'+row.id)
        },
        responseHandler: function (result) {   // 通过网络请求得到   数据进行解析初始化*******************************
            console.log(result.data.count);
            if (result != null) {
                return {
                    'total': result.data.count, //总条数*************************************
                    'rows': result.data.list //所有的数据**********************************
                };
            }
            return {
                'total': 0, //总条数
                'rows': [] //所有的数据
            }
        },
        //列表显示
        columns: [{   // 配置显示的列表 ****************************
            field: 'id',
            title: "排序"
            //visible: false
        }, {
            field: 'name',
            title: "充值规则"
        }, {
            field: 'status',
            title: "规则状态",
            formatter: function (value) {//自定义显示
                switch (value) {
                    case 1 :
                        return "<span  class='label label-info'>启用</span>";
                    case 0 :
                        return "<span  class='label label-danger'>禁用</span>";
                }
            }
        },
            {
                field: 'coefficient',
                title: "充值系数"
            }, {
                field: 'startMoney',
                title: "起充金额"
            },{
                field: 'createdTime',
                title: "创建日期"
            },{
                field: 'endTime',
                title: "结束日期"
            },  {
                field: 'operation',
                events: buttonOperateEvent, // 设置每一行按钮的 点击事件*********************************
                title: '操作',
                formatter: function (value, row, index) {  // 为当前行增加 按钮*******************
                    return ruleManage.buttonOption(value, row, index);
                }
            }
        ]
    });
};

/**
 * 获取具体的参数
 * @param params
 * @returns {{pageNumber: *, searchName: *, pageSize: *, searchStaffId: *}}
 */
ruleManage.queryParams = function (params) {
    return {
        "pageNumber": params.pageNumber, //当前页数
        "pageSize": params.pageSize, //每页条数
        "name1": $("#name").val(),
        "status": $("#status").val()
    }
};

/**
 * 按钮的点击事件
 * @type {{"click .delStaff": Window.buttonOperateEvent.click .delStaff, "click .updateStaff": Window.buttonOperateEvent.click .updateStaff}}
 */
window.buttonOperateEvent = {
    // 更新 员工 对应的 更新按钮的updateStaff
    'click .updateRule': function (e, value, row, index) {
        //row 这一行的数据
        $("#myModal2").modal('show');
        $("#id2").val(row.id);
        $("#name2").val(row.name);
        $("#coefficient2").val(row.coefficient);
        $("#startMoney2").val(row.startMoney);
        $("#status2").val(row.status);
        $("#createdTime2").val(row.createdTime);
        $("#endTime2").val(row.endTime);
        // 初始化更新的模态框
    },
    // 删除员工 更新按钮的delStaff
    'click .ruleStatus': function (e, value, row, index) {
        ruleManage.del(row);
    }
}
/**
 * 为每一行添加按钮
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
ruleManage.buttonOption = function (value, row, index) {
    var returnButton = [];
    returnButton.push('<button class="btn btn-info updateRule">修改</button>');
    returnButton.push('<button class="btn btn-danger ruleStatus">启用/禁用</button>');
    return returnButton.join('');
};

/**
 * 刷新列表
 */
ruleManage.search = function () {
    //bootstrapTable 刷新
    $("#ruleList2").bootstrapTable('refresh');
};

ruleManage.getNow = function (startTime) {
    //结束时间
    var date2 = new Date();
    //时间差的毫秒数
    var date3 = date2.getTime() - new Date(startTime).getTime();
    //计算出相差天数
    var days = Math.floor(date3 / (24 * 3600 * 1000));
    return parseInt(days / 365);
}

ruleManage.del = function (row) {
    /**
     * 一般情况下删除要加confirm
     */
    Modal.confirm({
        msg: "确认更换状态么？"
    }).on(function (e) {
        if (e) {
            var id = row.id;
            var status = row.status;
            var createdTime = row.createdTime;
            $.ajax({
                url:"/ChangeRuleStatusServlet",
                type:"Get",
                data:{
                    "status":status,
                    "id":id,
                    "createdTime":createdTime
                },
                dataType:"json",
                success:function (data) {
                    toastr['success']('更新状态成功');
                    clear_toastr(500);
                }
            });
            ruleManage.search()
        }
    })
};


function fnW(str){
    var num;
    str>10?num=str:num="0"+str;
    return num;
}
ruleManage.addRule = function () {
    $("#myModal").modal('show');
    //获取当前时间,并显示在创建时间输入框内
    var date=new Date();
    var year=date.getFullYear();//当前年份
    var month=date.getMonth();//当前月份
    var day=date.getDate();//天
    var hours=date.getHours();//小时
    var minute=date.getMinutes();//分
    var second=date.getSeconds();//秒
    var time=year+"-"+fnW((month+1))+"-"+fnW(day)+"  "+fnW(hours)+":"+fnW(minute)+":"+fnW(second);
    $("#createdTime").val(time);
};

ruleManage.add = function () {

    var name = $("#name1").val();
    var coefficient =$("#coefficient").val();
    var startMoney = $("#startMoney").val();
    var status = $("#status1").val();
    var endTime = $("#endTime").val();
    var createdTime = $("#createdTime").val();

    var bootstrapValidator = $("#ruleForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        //alert("校验成功");
        $.ajax({
            url:"/AddRuleServlet",
            type:"Get",
            data:{//发送到服务器的数据
                "name":name,
                "coefficient":coefficient,
                "startMoney":startMoney,
                "status":status,
                "endTime":endTime,
                "createdTime":createdTime
            },
            dataType:"json",//返回服务器的数据形式
            success:function (result) {//如果成功从服务器返回数据
                if(result != null) {
                    //移除上次的校验配置并让模态框隐藏起来
                    $("#ruleForm")[0].reset();
                    $("#ruleForm").data('bootstrapValidator').resetForm();
                    $("#myModal").modal('hide');//让模态框隐藏起来
                    toastr['success']('添加成功')
                    clear_toastr(500);
                    //添加成功之后自动更新列表
                    ruleManage.search();
                }
            },
            error:function () {
                toastr['error']('添加失败')
            }
        });
    }else {
        toastr['error']('校验失败');
        clear_toastr(500);
        //staffManage.jia();
    }
};

ruleManage.update = function(){
    var name = $("#name2").val();
    var coefficient = $("#coefficient2").val();
    var startMoney = $("#startMoney2").val();
    var status = $("#status2").val();
    var createdTime = $("#createdTime2").val();
    var endTime = $("#endTime2").val();
    var id = $("#id2").val();
    //alert(createdTime)

    var bootstrapValidator = $("#ruleForm2").data('bootstrapValidator');
    bootstrapValidator.validate();
    if (bootstrapValidator.isValid()){
        //alert("校验成功");
        $.ajax({
            url:"/UpdateRuleServlet",
            type:"Get",
            data:{//发送到服务器的数据
                "name":name,
                "coefficient":coefficient,
                "startMoney":startMoney,
                "status":status,
                "createdTime":createdTime,
                "endTime":endTime,
                "id":id
            },
            dataType:"json",//返回服务器的数据形式
            success:function (result) {//如果成功从服务器返回数据
                if(result != null) {
                    //移除上次的校验配置并让模态框隐藏起来
                    $("#ruleForm2")[0].reset();
                    $("#ruleForm2").data('bootstrapValidator').resetForm();
                    $("#myModal2").modal('hide');//让模态框隐藏起来
                    toastr['success']('修改成功')
                    clear_toastr(500);
                    //添加成功之后自动更新列表
                    ruleManage.search();
                }
            }
        });
    }


}

$("#ruleForm").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
            validators: {
                notEmpty: {
                    message: "充值规则不能为空"
                }
            }
        },
        coefficient: {
            validators: {
                notEmpty: {
                    message: "充值系数不能为空"
                }
            }
        }
    }
});

$("#ruleForm2").bootstrapValidator({
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
            validators: {
                notEmpty: {
                    message: "充值规则不能为空"
                }
            }
        },
        coefficient: {
            validators: {
                notEmpty: {
                    message: "充值系数不能为空"
                }
            }
        }
    }
});

/*
*设置土司弹框的显示时间
* */
function clear_toastr(time) {

    setTimeout(
        function (){
            toastr.clear();
        },time)

}